#!/usr/bin/env bash

composer install
./vendor/bin/sail build --no-cache
./vendor/bin/sail down -v
./vendor/bin/sail up -d

./vendor/bin/sail artisan migrate
./vendor/bin/sail artisan migrate --seed

./vendor/bin/sail artisan route:clear
./vendor/bin/sail artisan cache:clear
./vendor/bin/sail artisan config:clear

./vendor/bin/sail artisan route:cache
./vendor/bin/sail artisan config:cache
