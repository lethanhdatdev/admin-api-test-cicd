# Development environment

### Requirements
- PHP: 8.1
- Composer: 2.4.2

### Docker build
- Copy .env.example to .env file
```bash
cp .env.example .env
```
- Install composer packages if necessary
```bash
composer install
```
- Build docker container
```bash
./vendor/bin/sail build
```


### Start environment
- Run laravel sail to up environment
```bash
./vendor/bin/sail up -d
```
- Generate application key (Skip for deploy, it has in .env.example file)
```bash
./vendor/bin/sail artisan generate:key
```


### Migrations
- Run migrations database structure
```bash
./vendor/bin/sail artisan migrate
```
- Run migrations database seed data
```bash
./vendor/bin/sail artisan migrate --seed
```
- Clear old stuff
```bash
./vendor/bin/sail artisan route:clear
./vendor/bin/sail artisan cache:clear
./vendor/bin/sail artisan config:clear
```
- Caching new stuff (no need for development)
```bash
./vendor/bin/sail artisan route:cache
./vendor/bin/sail artisan config:cache
```


### Informations
- Default admin account
```
admin@bittus.vn / bittus-admin@2022
```


### Link (default): [http://localhost:5001](`http://localhost:5001`)

# Production environment

```Still working```
docker buildx build --network host --platform linux/arm64 --push -t 022223880118.dkr.ecr.ap-southeast-1.amazonaws.com/bittus-admin-api:admin-api-1.0.6 .
