FROM composer:2.4.4 as build
COPY composer.json composer.lock /app/
COPY docker/php/conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
RUN composer install \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --prefer-dist\
    --optimize-autoloader\
    --ignore-platform-req=ext-gd
COPY . /app
# RUN composer dump-autoload

FROM php:8.1-apache-buster as production

RUN apt-get update
RUN apt-get install -y libpq-dev zlib1g-dev libpng-dev libzip-dev\
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql\
    && docker-php-ext-configure opcache --enable-opcache\
    && docker-php-ext-install pdo pdo_pgsql pgsql gd zip\
    && docker-php-ext-enable gd 

ENV APP_ENV=staging
ENV APP_DEBUG=false

ARG KEY_DECRYPT
ENV KEY_DECRYPT=${KEY_DECRYPT}

COPY docker/8.1/php.ini /usr/local/etc/php/conf.d/php.ini
COPY docker/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY --from=build /app /var/www/html

RUN php artisan env:decrypt --env=stag --key=${KEY_DECRYPT}
RUN mv .env.stag /var/www/html/.env

RUN php artisan key:generate && \
    php artisan config:cache && \
    php artisan route:cache && \
    chmod 777 -R /var/www/html/storage/ && \
    chown -R www-data:www-data /var/www/ && \
    a2enmod rewrite
