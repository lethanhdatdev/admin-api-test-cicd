#!/usr/bin/env bash
docker build -t bittus-admin-api .
aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin  022223880118.dkr.ecr.ap-southeast-1.amazonaws.com
docker tag bittus-admin-api:latest 022223880118.dkr.ecr.ap-southeast-1.amazonaws.com/bittus-admin-api:latest
IMAGES_TO_DELETE=$(aws ecr list-images --region ap-southeast-1 --repository-name bittus-admin-api --filter "tagStatus=UNTAGGED" --query 'imageIds[*]' --output json)
aws ecr batch-delete-image --region ap-southeast-1 --repository-name bittus-admin-api --image-ids "$IMAGES_TO_DELETE" || true
docker push 022223880118.dkr.ecr.ap-southeast-1.amazonaws.com/bittus-admin-api:latest
aws ecs update-service --cluster bittus-admin-api --service bittus-admin-api-service --region ap-southeast-1 --force-new-deployment
