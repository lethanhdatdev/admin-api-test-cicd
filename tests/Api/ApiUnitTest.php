<?php

namespace Tests\Api;

use App\Repository\UnitRepository;
use Tests\AbstractApiTestCase;

/**
 * @group Tests
 * @group Api
 * @group TestSchedule
 */
class ApiUnitTest extends AbstractApiTestCase
{
    private UnitRepository $unitRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->unitRepository = $this->app->make(UnitRepository::class);
    }

    public function testListTestSchedule(): void
    {
        $response = $this
            ->withToken($this->getToken())
            ->getJson('/api/unit?page=0&size=5');

        self::assertEquals(200, $response->getStatusCode());
    }

    protected function createUnitProvider(): array
    {
        return [
            [
                'data' => [
                    'name' => 'Test 2',
                ],
                'expected' => 201
            ],
            [
                'data' => [
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Test 2',
                ],
                'expected' => 201
            ]
        ];
    }

    /**
     * @dataProvider createUnitProvider
     * @param array $data
     * @param int $expectedCode
     * @return void
     */
    public function testCreateUnit(array $data, int $expectedCode): void
    {
        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->postJson('/api/unit', $data);

        self::assertEquals($expectedCode, $response->getStatusCode());
    }

    public function testEditUnit(): void
    {
        $unit = $this->unitRepository->create([
            'name' => 'Test 2'
        ]);

        $id = $unit->id;

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->putJson('/api/unit/' . $id, [
                'name' => 'New name'
            ]);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals($id, $response->json('data.id'));
        self::assertEquals('New name', $response->json('data.name'));
    }

    public function testGetSingleUnit(): void
    {
        $unit = $this->unitRepository->create([
            'name' => 'Test 2'
        ]);

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/unit/' . $unit->id);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('Test 2', $response->json('data.name'));
    }

    public function testDeleteTestSchedule(): void
    {
        $unit = $this->unitRepository->create([
            'name' => 'Test 2',
        ]);

        $id = $unit->id;

        $deleteResponse = $this->withToken($this->getToken())
            ->deleteJson('/api/unit/' . $id);

        self::assertEquals(200, $deleteResponse->getStatusCode());

        $unit = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/unit/' . $id);
        self::assertEquals(404, $unit->getStatusCode());
    }

    public function testOptionUnit(): void
    {
        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/options/unit/');

        self::assertEquals(200, $response->getStatusCode());
    }
}
