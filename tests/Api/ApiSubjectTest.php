<?php

namespace Tests\Api;

use App\Repository\SubjectRepository;
use Tests\AbstractApiTestCase;

/**
 * @group Tests
 * @group Api
 * @group TestSchedule
 */
class ApiSubjectTest extends AbstractApiTestCase
{
    private SubjectRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(SubjectRepository::class);
    }

    protected function createSubjectProvider(): array
    {
        return [
            [
                'data' => [
                    'name' => str_pad('', 256, 0),
                ],
                'expected' => 422
            ],
            [
                'data' => [
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Toán',
                ],
                'expected' => 201
            ]
        ];
    }

    /**
     * @dataProvider createSubjectProvider
     * @param array $data
     * @param int $expectedCode
     * @return void
     */
    public function testCreateSubject(array $data, int $expectedCode): void
    {
        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->postJson('/api/subject', $data);

        self::assertEquals($expectedCode, $response->getStatusCode());
    }
}
