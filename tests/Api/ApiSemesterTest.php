<?php

namespace Tests\Api;

use App\Repository\SemesterRepository;
use App\Repository\UnitRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\AbstractApiTestCase;

/**
 * @group Tests
 * @group Api
 * @group TestSchedule
 */
class ApiSemesterTest extends AbstractApiTestCase
{
    private UnitRepository $unitRepository;
    private SemesterRepository $semesterRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->unitRepository = $this->app->make(UnitRepository::class);
        $this->semesterRepository = $this->app->make(SemesterRepository::class);
    }

    protected function showListSemesterProvider(): array
    {
        return [
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                ],
                'expected' => 3
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'unit_id' => 1
                ],
                'expected' => 2
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'unit_id' => 2
                ],
                'expected' => 1
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'unit_id' => 3
                ],
                'expected' => 0
            ],
        ];
    }

    /**
     * @dataProvider showListSemesterProvider
     * @param array $data
     * @param int $expected
     * @return void
     */
    public function testShowListSemester(array $data, int $expected): void
    {
        $unit1 = $this->unitRepository->create(['name' => 'Lớp 7']);
        $unit2 = $this->unitRepository->create(['name' => 'Lớp 8']);
        $this->semesterRepository->create([
            'name' => 'Semester ' . rand(0, 999),
            'unit_id' => $unit1->id
        ]);
        $this->semesterRepository->create([
            'name' => 'Semester ' . rand(0, 999),
            'unit_id' => $unit1->id
        ]);
        $this->semesterRepository->create([
            'name' => 'Semester ' . rand(0, 999),
            'unit_id' => $unit2->id
        ]);

        $uri = '/api/semester?';
        foreach ($data as $key => $value) {
            $uri .= $key . '=' . $value . '&';
        }

        $response = $this
            ->withToken($this->getToken())
            ->getJson($uri);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals($expected, $response->json('meta.total'));
    }

    public function testShowSingleSemester(): void
    {
        $unit = $this->unitRepository->create(['name' => 'Lớp 7']);
        $entity = $this->semesterRepository->create([
            'name' => 'Semester #1',
            'unit_id' => $unit->id
        ]);
        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/semester/' . $entity->id);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('Semester #1', $response->json('data.name'));
        self::assertEquals($unit->id, $response->json('data.unit.id'));
    }

    protected function createSemesterProvider(): array
    {
        return [
            [
                'data' => [
                    'name' => str_pad('', 256, 0),
                ],
                'expected' => 422
            ],
            [
                'data' => [
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Hoc ky 1',
                    'unit_id' => 99
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Hoc ky 1',
                    'unit_id' => 1
                ],
                'expected' => 201
            ]
        ];
    }

    /**
     * @dataProvider createSemesterProvider
     * @param array $data
     * @param int $expectedCode
     * @return void
     */
    public function testCreateSemester(array $data, int $expectedCode): void
    {
        $this->unitRepository->create(['name' => 'Lớp 7']);
        $response = $this->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->postJson('/api/semester', $data);

        self::assertEquals($expectedCode, $response->getStatusCode());
    }

    public function testEditSemester(): void
    {
        $this->unitRepository->create(['name' => 'Lớp 7']);
        $unit2 = $this->unitRepository->create(['name' => 'Lớp 8']);
        $entity = $this->semesterRepository->create([
            'name' => 'Semester #1',
            'unit_id' => 1
        ]);
        $response = $this->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->putJson('/api/semester/' . $entity->id, [
                'name' => 'Semester #2',
                'unit_id' => $unit2->id
            ]);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('Semester #2', $response->json('data.name'));
        self::assertEquals($unit2->id, $response->json('data.unit.id'));
    }

    public function testDeleteSemester(): void
    {
        $this->unitRepository->create(['name' => 'Lớp 7']);
        $entity = $this->semesterRepository->create([
            'name' => 'Semester #1',
            'unit_id' => 1
        ]);
        $response = $this->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->deleteJson('/api/semester/' . $entity->id);
        self::assertEquals(200, $response->getStatusCode());

        self::expectException(ModelNotFoundException::class);
        $this->semesterRepository->find($entity->id);
    }

    public function testOptionSemester(): void
    {
        $this->unitRepository->create(['name' => 'Lớp 7']);
        $this->semesterRepository->create([
            'name' => 'Semester #1',
            'unit_id' => 1
        ]);
        $this->semesterRepository->create([
            'name' => 'Semester #2',
            'unit_id' => 1
        ]);
        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/options/semester/');

        self::assertEquals(200, $response->getStatusCode());
        self::assertCount(2, $response->json('data'));
    }
}
