<?php

namespace Tests\Api;

use App\Enums\Status;
use App\Enums\TestDifficulty;
use App\Enums\TestType;
use App\Repository\AnswerRepository;
use App\Repository\QuestionRepository;
use App\Repository\SemesterRepository;
use App\Repository\SubjectRepository;
use App\Repository\TestRepository;
use App\Repository\TestScheduleRepository;
use App\Repository\ThematicRepository;
use App\Repository\UnitRepository;
use App\Services\QuestionService;
use Tests\AbstractApiTestCase;

/**
 * @group Tests
 * @group Api
 */
class ApiTestScheduleTest extends AbstractApiTestCase
{
    private TestScheduleRepository $repository;
    private UnitRepository $unitRepository;
    private SubjectRepository $subjectRepository;
    private TestRepository $testRepository;
    private ThematicRepository $thematicRepository;
    private QuestionRepository $questionRepository;
    private AnswerRepository $answerRepository;
    private SemesterRepository $semesterRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(TestScheduleRepository::class);
        $this->unitRepository = $this->app->make(UnitRepository::class);
        $this->subjectRepository = $this->app->make(SubjectRepository::class);
        $this->testRepository = $this->app->make(TestRepository::class);
        $this->thematicRepository = $this->app->make(ThematicRepository::class);
        $this->questionRepository = $this->app->make(QuestionRepository::class);
        $this->answerRepository = $this->app->make(AnswerRepository::class);
        $this->semesterRepository = $this->app->make(SemesterRepository::class);
    }

    protected function listTestScheduleProvider(): array
    {
        return [
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                ],
                'expected' => 3
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'unit_id' => 1
                ],
                'expected' => 1
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'subject_id' => 2
                ],
                'expected' => 2
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'thematic_id' => 1
                ],
                'expected' => 1
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'thematic_id' => 2
                ],
                'expected' => 2
            ],
            [
                'data' => [
                    'page' => 0,
                    'size' => 5,
                    'year' => '2023-2024'
                ],
                'expected' => 1
            ]
        ];
    }

    /**
     * @dataProvider listTestScheduleProvider
     *
     * @param array $data
     * @param int $expected
     * @return void
     */
    public function testListTestSchedule(array $data, int $expected): void
    {
        $unit1 = $this->unitRepository->create(['name' => 'Unit 1']);
        $unit2 = $this->unitRepository->create(['name' => 'Unit 2']);
        $subject1 = $this->subjectRepository->create(['name' => 'Subject 1']);
        $subject2 = $this->subjectRepository->create(['name' => 'Subject 2']);
        $semester1 = $this->semesterRepository->create(['name' => 'Semester 1', 'unit_id' => $unit1->id]);
        $semester2 = $this->semesterRepository->create(['name' => 'Semester 2', 'unit_id' => $unit2->id]);
        $thematic1 = $this->thematicRepository->firstOrCreate([
            'name' => 'Chuyên đề 1',
            'description' => 'Chuyên đề 1',
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
        ]);
        $thematic2 = $this->thematicRepository->firstOrCreate([
            'name' => 'Chuyên đề 2',
            'description' => 'Chuyên đề 2',
            'subject_id' => $subject2->id,
            'semester_id' => $semester1->id,
        ]);

        $test = $this->testRepository->firstOrCreate([
            'code' => 'TEST_DEMO_1',
            'name' => 'Đề thi demo',
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
            'type' => TestType::OFFICIAL,
            'duration' => 1800,
            'difficulty' => TestDifficulty::NORMAL
        ]);
        $test->save();
        $test->questions()->sync($this->generateQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);
        $test->questions()->sync($this->genFillQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);
        $test->questions()->sync($this->genChoiceQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);

        $test2 = $this->testRepository->firstOrCreate([
            'code' => 'TEST_DEMO_2',
            'name' => 'Đề thi demo #2',
            'subject_id' => $subject2->id,
            'semester_id' => $semester2->id,
            'type' => TestType::OFFICIAL,
            'duration' => 3600,
            'difficulty' => TestDifficulty::ADVANCED
        ]);
        $test2->save();
        $test2->questions()->sync($this->generateQuestion($thematic1->id, 1, 'TEST_DEMO_1'), false);
        $test2->questions()->sync($this->genFillQuestion($thematic2->id, 1, 'TEST_DEMO_1'), false);
        $test2->questions()->sync($this->genChoiceQuestion($thematic2->id, 1, 'TEST_DEMO_1'), false);

        $this->repository->create([
            'name' => 'Test schedule ' . rand(0, 999),
            'description' => 'ABC',
            'unit_id' => $unit1->id,
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
            'test_id' => $test->id,
            'year' => '2021-2022',
            'start_time' => '2023-05-15 09:00:00'
        ]);
        $this->repository->create([
            'name' => 'Test schedule ' . rand(0, 999),
            'description' => 'ABC',
            'unit_id' => $unit2->id,
            'subject_id' => $subject2->id,
            'semester_id' => $semester2->id,
            'test_id' => $test2->id,
            'year' => '2022-2023',
            'start_time' => '2023-05-15 09:00:00'
        ]);
        $this->repository->create([
            'name' => 'Test schedule ' . rand(0, 999),
            'description' => 'ABC',
            'unit_id' => $unit2->id,
            'subject_id' => $subject2->id,
            'semester_id' => $semester2->id,
            'test_id' => $test2->id,
            'year' => '2023-2024',
            'start_time' => '2023-05-15 09:00:00'
        ]);

        $uri = '/api/test-schedule?';
        foreach ($data as $key => $value) {
            $uri .= $key . '=' . $value . '&';
        }

        $response = $this
            ->withToken($this->getToken())
            ->getJson($uri);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals($expected, $response->json('meta.total'));
    }

    protected function createTestScheduleProvider(): array
    {
        return [
            [
                'data' => [
                    'name' => 'Test 2',
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Test 2',
                    'description' => 'ABC',
                    'unit_id' => 1,
                    'subject_id' => 1,
                    'semester_id' => 1,
                    'test_id' => 99,
                    'year' => '2022-2023',
                    'start_time' => '2023-05-15 09:00:00'
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Test 2',
                    'description' => 'ABC',
                    'unit_id' => 1,
                    'subject_id' => 1,
                    'semester_id' => 1,
                    'test_id' => 99,
                    'year' => '2022-2023',
                    'start_time' => '2021-05-15 09:00:00'
                ],
                'expected' => 422
            ],
            [
                'data' => [
                    'name' => 'Test 2',
                    'description' => 'ABC',
                    'unit_id' => 1,
                    'subject_id' => 1,
                    'semester_id' => 1,
                    'test_id' => 1,
                    'year' => '2022-2023',
                    'start_time' => '2023-05-15 09:00:00'
                ],
                'expected' => 201
            ]
        ];
    }

    /**
     * @dataProvider createTestScheduleProvider
     * @param array $data
     * @param int $expectedCode
     * @return void
     */
    public function testCreateTestSchedule(array $data, int $expectedCode): void
    {
        $unit1 = $this->unitRepository->create(['name' => 'Unit 1']);
        $subject1 = $this->subjectRepository->create(['name' => 'Subject 1']);
        $semester1 = $this->semesterRepository->create(['name' => 'Semester 1', 'unit_id' => $unit1->id]);
        $thematic1 = $this->thematicRepository->firstOrCreate([
            'name' => 'Chuyên đề 1',
            'description' => 'Chuyên đề 1',
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
        ]);
        $test = $this->testRepository->firstOrCreate([
            'code' => 'TEST_DEMO_1',
            'name' => 'Đề thi demo',
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
            'type' => TestType::OFFICIAL,
            'duration' => 1800,
            'difficulty' => TestDifficulty::NORMAL
        ]);
        $test->save();
        $test->questions()->sync($this->generateQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);

        $response = $this->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->postJson('/api/test-schedule', $data);

        self::assertEquals($expectedCode, $response->getStatusCode());
    }

    public function testEditTestSchedule(): void
    {
        $this->generateRelatedData();

        $response = $this->repository->create([
            'name' => 'Test Schedule',
            'description' => 'ABC',
            'unit_id' => 1,
            'subject_id' => 1,
            'semester_id' => 1,
            'test_id' => 1,
            'year' => '2022-2023',
            'start_time' => '2023-05-15 09:00:00'
        ]);

        $testScheduleId = $response->id;

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->putJson('/api/test-schedule/' . $testScheduleId, [
                'name' => 'Test 2 Edit',
                'description' => 'ABC',
                'unit_id' => 1,
                'subject_id' => 1,
                'semester_id' => 1,
                'test_id' => 1,
                'year' => '2022-2023',
                'start_time' => '2023-05-15 09:00:00'
            ]);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals($testScheduleId, $response->json('data.id'));
        self::assertEquals('Test 2 Edit', $response->json('data.name'));
    }

    public function testGetSingleTestSchedule(): void
    {
        $this->generateRelatedData();

        $response = $this->repository->create([
            'name' => 'Test 2',
            'description' => 'ABC',
            'unit_id' => 1,
            'subject_id' => 1,
            'semester_id' => 1,
            'test_id' => 1,
            'year' => '2022-2023',
            'start_time' => '2023-05-15 09:00:00'
        ]);

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/test-schedule/' . $response->id);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('Test 2', $response->json('data.name'));
    }

    public function testDeleteTestSchedule(): void
    {
        $this->generateRelatedData();
        $this->repository->create([
            'name' => 'Test 2',
            'description' => 'ABC',
            'unit_id' => 1,
            'subject_id' => 1,
            'semester_id' => 1,
            'test_id' => 1,
            'year' => '2022-2023',
            'start_time' => '2023-05-15 09:00:00'
        ]);

        $deleteResponse = $this->withToken($this->getToken())
            ->deleteJson('/api/test-schedule/1');

        self::assertEquals(200, $deleteResponse->getStatusCode());

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/test-schedule/1');
        self::assertEquals(404, $response->getStatusCode());
    }

    public function testLockTestSchedule(): void
    {
        $this->generateRelatedData();

        $response = $this->withToken($this->getToken())
            ->putJson('/api/test-schedule/99/lock');
        self::assertEquals(404, $response->getStatusCode());

        $response = $this->repository->create([
            'name' => 'Test Schedule',
            'description' => 'ABC',
            'unit_id' => 1,
            'subject_id' => 1,
            'semester_id' => 1,
            'test_id' => 1,
            'year' => '2022-2023',
            'start_time' => '2023-05-15 09:00:00'
        ]);

        $id = $response->id;

        $response = $this->withToken($this->getToken())
            ->putJson('/api/test-schedule/' . $id . '/lock');
        self::assertEquals(200, $response->getStatusCode());

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/test-schedule/' . $id);
        self::assertEquals(200, $response->getStatusCode());

        self::assertEquals(Status::LOCK->value, $response->json('data.status'));
    }

    public function testActiveTestSchedule(): void
    {
        $this->generateRelatedData();

        $response = $this->withToken($this->getToken())
            ->putJson('/api/test-schedule/99/active');
        self::assertEquals(404, $response->getStatusCode());

        $response = $this->repository->create([
            'name' => 'Test Schedule',
            'description' => 'ABC',
            'unit_id' => 1,
            'subject_id' => 1,
            'semester_id' => 1,
            'test_id' => 1,
            'year' => '2022-2023',
            'start_time' => '2023-05-15 09:00:00'
        ]);

        $id = $response->id;

        $response = $this->withToken($this->getToken())
            ->putJson('/api/test-schedule/' . $id . '/lock');
        self::assertEquals(200, $response->getStatusCode());

        $response = $this->withToken($this->getToken())
            ->putJson('/api/test-schedule/' . $id . '/active');
        self::assertEquals(200, $response->getStatusCode());

        $response = $this
            ->withToken($this->getToken())
            ->withHeader('Content-Type', 'application/json')
            ->getJson('/api/test-schedule/' . $id);
        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals(Status::ACTIVE->value, $response->json('data.status'));
    }

    private function generateRelatedData(): void
    {
        $unit1 = $this->unitRepository->create(['name' => 'Unit 1']);
        $subject1 = $this->subjectRepository->create(['name' => 'Subject 1']);
        $semester1 = $this->semesterRepository->create(['name' => 'Semester 1', 'unit_id' => $unit1->id]);
        $thematic1 = $this->thematicRepository->firstOrCreate([
            'name' => 'Chuyên đề 1',
            'description' => 'Chuyên đề 1',
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
        ]);
        $test = $this->testRepository->firstOrCreate([
            'code' => 'TEST_DEMO_1',
            'name' => 'Đề thi demo',
            'subject_id' => $subject1->id,
            'semester_id' => $semester1->id,
            'type' => TestType::OFFICIAL,
            'duration' => 1800,
            'difficulty' => TestDifficulty::NORMAL
        ]);
        $test->save();
        $test->questions()->sync($this->generateQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);
    }

    private function generateQuestion(int $thematicId, int $count = 1, string $prefix = ''): array
    {
        $ids = [];

        for ($i = 1; $i <= $count; $i++) {
            $question = $this->questionRepository->firstOrCreate([
                'description' => 'Câu hỏi sắp xếp demo ' . $prefix . ' #' . $i,
                'type' => QuestionService::TYPE_SORT,
                'thematic_id' => $thematicId,
                'suggestion' => 'Suggestion if answer is incorrect',
                'sort_correct' => 'Số 1,Số 2,Số 3,Số 4',
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 1',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 2',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 3',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 4',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);

            $ids[] = $question->id;
        }

        return $ids;
    }

    private function genChoiceQuestion(int $thematicId, int $count = 1, string $prefix = ''): array
    {
        $ids = [];

        for ($i = 1; $i <= $count; $i++) {
            $question = $this->questionRepository->firstOrCreate([
                'description' => 'Câu hỏi trắc nghiệm ' . $prefix . ' #' . $i,
                'type' => QuestionService::TYPE_CHOICE,
                'thematic_id' => $thematicId,
                'suggestion' => 'Suggestion if answer is incorrect',
                'sort_correct' => '',
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 1',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => false,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 2',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => false,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 3',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 4',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => false,
                'question_id' => $question->id
            ]);

            $ids[] = $question->id;
        }

        return $ids;
    }

    private function genFillQuestion(int $thematicId, int $count = 1, string $prefix = ''): array
    {
        $ids = [];

        for ($i = 1; $i <= $count; $i++) {
            $question = $this->questionRepository->firstOrCreate([
                'description' => 'The quick ... fox jumps over the ... dog ' . $prefix . ' #' . $i,
                'type' => QuestionService::TYPE_FILL_IN_BLANK,
                'thematic_id' => $thematicId,
                'suggestion' => 'Suggestion if answer is incorrect',
                'sort_correct' => '',
            ]);

            $ids[] = $question->id;

            $this->answerRepository->firstOrCreate([
                'description' => 'brown',
                'type' => QuestionService::TYPE_FILL_IN_BLANK,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'lazy',
                'type' => QuestionService::TYPE_FILL_IN_BLANK,
                'correct' => true,
                'question_id' => $question->id
            ]);
        }

        return $ids;
    }
}
