<?php

namespace Tests;

use Database\Seeders\DataForTestingSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Testing\Concerns\TestDatabases;

class AbstractApiTestCase extends TestCase
{
    use TestDatabases;
    use DatabaseMigrations;

    protected static string|null $token = null;

    protected function setUp(): void
    {
        parent::setUp();

        $this->bootTestDatabase();
        $this->seed(DataForTestingSeeder::class);
        $this->loadToken();
    }

    /**
     * @return string
     */
    public function loadToken(): void
    {
        $login = $this
            ->withCredentials()
            ->postJson('/api/auth/login', [
                "email" => "admin@bittus.vn",
                "password" => "bittus-admin@2022"
            ], ['Content-Type' => 'application/json']);
        $content = json_decode($login->content());
        static::$token = $content->data->token;
    }

    public function getToken(): string
    {
        if (static::$token === null) {
            $this->loadToken();
        }

        return static::$token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }
}
