<?php

use App\Http\Controllers\AncestorController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\KnowledgeController;
use App\Http\Controllers\LearnController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PracticeCategoryController;
use App\Http\Controllers\PracticeController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SemesterController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\AmazonS3VideoController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TestScheduleController;
use App\Http\Controllers\ThematicController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerificationController;
use Illuminate\Support\Facades\Route;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController;
use Spatie\Health\Http\Controllers\HealthCheckJsonResultsController;

Route::group(['prefix' => config('sanctum.prefix', 'sanctum')], function () {
    Route::get('/csrf-cookie', [CsrfCookieController::class, 'show'])
        ->middleware('web')
        ->name('api.sanctum.csrf-cookie');
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:sanctum')->group(static function () {
    Route::post('/auth/logout', [AuthController::class, 'logout']);
    Route::post('/auth/refresh', [AuthController::class, 'refresh']);
    Route::get('/user/me', [UserController::class, 'me']);

    Route::get('/options/ancestor', [AncestorController::class, 'options']);
    Route::get('/options/unit', [UnitController::class, 'options']);
    Route::get('/options/subject', [SubjectController::class, 'options']);
    Route::get('/options/semester', [SemesterController::class, 'options']);
    Route::get('/options/thematic', [ThematicController::class, 'options']);
    Route::get('/options/practice-category', [PracticeCategoryController::class, 'options']);
    Route::get('/options/learn-type', [LearnController::class, 'learnTypeOptions']);
    Route::get('/options/student-type', [StudentController::class, 'studentTypeOptions']);

    Route::post('/import/question', [QuestionController::class, 'import']);

    Route::post('/practice/{id}/assign-questions', [PracticeController::class, 'assignQuestions']);
    Route::post('/test/{id}/assign-questions', [TestController::class, 'assignQuestions']);

    Route::get('/orders', [OrderController::class, 'index']);
    Route::get('/orders/{code}', [OrderController::class, 'show']);
    Route::post('/orders/{code}', [OrderController::class, 'update']);
});

Route::middleware('auth:sanctum')->apiResource('ancestor', AncestorController::class);
Route::middleware('auth:sanctum')->apiResource('student', StudentController::class);
Route::middleware('auth:sanctum')->apiResource('unit', UnitController::class);
Route::middleware('auth:sanctum')->apiResource('semester', SemesterController::class);
Route::middleware('auth:sanctum')->apiResource('subject', SubjectController::class);
Route::middleware('auth:sanctum')->apiResource('thematic', ThematicController::class);
Route::middleware('auth:sanctum')->apiResource('knowledge', KnowledgeController::class);
Route::middleware('auth:sanctum')->apiResource('question', QuestionController::class);
Route::middleware('auth:sanctum')->apiResource('answer', AnswerController::class);
Route::middleware('auth:sanctum')->apiResource('test', TestController::class);
Route::middleware('auth:sanctum')->apiResource('test-schedule', TestScheduleController::class);
Route::middleware('auth:sanctum')->put('test-schedule/{id}/lock', [TestScheduleController::class, 'lock']);
Route::middleware('auth:sanctum')->put('test-schedule/{id}/active', [TestScheduleController::class, 'active']);
Route::middleware('auth:sanctum')->apiResource('practice', PracticeController::class);
Route::middleware('auth:sanctum')->apiResource('learn', LearnController::class);
Route::middleware('auth:sanctum')->apiResource('practice-category', PracticeCategoryController::class);
Route::middleware('auth:sanctum')->apiResource('video', AmazonS3VideoController::class)->only(['index']);
Route::middleware('auth:sanctum')->resource('events', EventsController::class);
Route::middleware('auth:sanctum')->get('tests', [TestController::class, 'getTestData'])->name('getTestData');
Route::middleware('auth:sanctum')->post('media', [MediaController::class, 'store'])->name('storeMedia');


Route::post('/auth/login', [AuthController::class, 'authenticate'])->middleware('guest');
Route::post('/verify/start', [VerificationController::class, 'start'])->middleware('guest');
Route::post('/verify/check', [VerificationController::class, 'check'])->middleware('guest');
Route::get('/health', HealthCheckJsonResultsController::class)->middleware(['throttle:6']);

Route::post('/sync-video', [AmazonS3VideoController::class, 'sync'])->middleware(['amazon.s3.sync']);
