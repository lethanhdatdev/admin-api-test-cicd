<?php

namespace App\Checks;

use Exception;
use Illuminate\Support\Facades\Storage;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class AmazonS3ServiceChecks extends Check
{
    public function run(): Result
    {
        try {
            Storage::disk('s3')->getClient()->listBuckets();
            return Result::make();
        } catch (Exception $exception) {
            return Result::make()->failed($exception->getMessage());
        }
    }
}
