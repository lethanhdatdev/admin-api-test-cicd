<?php

namespace App\Repository;

use App\Models\Thematic;

class ThematicRepository extends AbstractBaseRepository
{
    protected array $filterable = ['subject_id', 'semester_id'];
    protected array $searchable = ['name', 'description'];
    protected array $sortable = ['id', 'name', 'description', 'created_at'];

    protected function getModel(): string
    {
        return Thematic::class;
    }
}
