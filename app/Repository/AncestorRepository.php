<?php

namespace App\Repository;

use App\Models\Ancestor;

class AncestorRepository extends AbstractBaseRepository
{
    protected array $filterable = [];
    protected array $searchable = ['address', 'phone', 'affiliate_code', 'user' => ['name', 'email']];
    protected array $sortable = ['id', 'birthday', 'address', 'phone', 'created_at'];

    protected function getModel(): string
    {
        return Ancestor::class;
    }
}
