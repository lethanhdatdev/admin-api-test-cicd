<?php

namespace App\Repository;

use App\Models\SyncVideo;

class AmazonS3VideoRepository extends AbstractBaseRepository
{
    protected array $filterable = [];
    protected array $searchable = ['name'];
    protected array $sortable = ['name', 'created_at'];

    protected function getModel(): string
    {
        return SyncVideo::class;
    }
}
