<?php

namespace App\Repository;

use App\Models\Question;
use App\Models\Test;
use App\Models\Thematic;

class TestRepository extends AbstractBaseRepository
{
    protected array $filterable = [
        'type',
        'subject_id',
        'semester_id',
        'difficulty',
        'unit_id' => ['semester' => 'unit_id'],
        'thematic_id' => ['questions' => 'thematic_id']
    ];
    protected array $searchable = ['name', 'code'];
    protected array $sortable = [
        'id',
        'created_at',
        'name',
        'code',
        'duration',
        'difficulty',
        'type',
        'subject_id',
        'semester_id'
    ];

    protected function getModel(): string
    {
        return Test::class;
    }

    public function create($attributes = []): Test
    {
        /** @var Test $testEntity */
        $testEntity = parent::create($attributes);

        $this->updateQuestions($attributes['questions'] ?? [], $testEntity);

        return $testEntity;
    }

    public function update($id, $attributes = []): Test
    {
        /** @var Test $testEntity */
        $testEntity = parent::update($id, $attributes);

        $this->updateQuestions($attributes['questions'] ?? [], $testEntity);

        return $testEntity;
    }

    private function updateQuestions(array $questions, Test $test): void
    {
        $questionIds = [];
        foreach ($questions as $question) {
            $questionId = (int)($question['id'] ?? null);
            array_push($questionIds, $questionId);
        }

        $test->questions()->sync($questionIds);
    }

    public function assignQuestions(Test $test, array $questionIds): array
    {
        $assignedQuestionIds = [];

        foreach ($questionIds as $questionId) {
            $question = Question::find($questionId);

            if (null === $question) {
                continue;
            }

            /** @var Question $question */
            if (!Thematic::where('id', $question->thematic_id)->where('subject_id', $test->subject_id)->where(
                'semester_id',
                $test->semester_id
            )->exists()) {
                continue;
            }

            $assignedQuestionIds[] = $question->id;
        }

        if (empty($assignedQuestionIds)) {
            return [];
        }

        $test->questions()->syncWithoutDetaching($assignedQuestionIds);

        return $assignedQuestionIds;
    }
}
