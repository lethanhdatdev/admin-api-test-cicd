<?php

namespace App\Repository;

use App\Http\Requests\IOptionsRequest;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property array $filterable
 */
interface IOptionsRepository
{
    public function options(IOptionsRequest $request, string $labelColumn = 'name', string $valueColumn = 'id', string $sortByColumn = 'name'): Collection;
}
