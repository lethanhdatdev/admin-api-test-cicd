<?php

namespace App\Repository;

use App\Models\Knowledge;
use App\Models\Question;
use App\Models\Topic;
use Exception;
use Illuminate\Database\Eloquent\Model;

class KnowledgeRepository extends AbstractBaseRepository
{
    protected array $filterable = [
        'type',
        'thematic_id',
        'unit_id' => ['thematic' => ['semester' => 'unit_id']],
        'subject_id' => ['thematic' => 'subject_id']
    ];
    protected array $searchable = ['name', 'intro', 'description'];
    protected array $sortable = ['id', 'created_at', 'name', 'type', 'thematic_id'];

    public function __construct(
        private readonly QuestionRepository $questionRepository,
        private readonly TopicRepository $topicRepository
    ) {
        parent::__construct();
    }

    protected function getModel(): string
    {
        return Knowledge::class;
    }

    public function create($attributes = []): Model
    {
        /** @var Knowledge $knowledge */
        $knowledge = parent::create($attributes);

        $topicIds = $this->updateExtraData($knowledge, $attributes);

        $knowledge->topics()->sync($topicIds);

        return $knowledge;
    }

    public function update($id, $attributes = []): Knowledge
    {
        /** @var Knowledge $knowledge */
        $knowledge = parent::update($id, $attributes);

        $topicIds = $this->updateExtraData($knowledge, $attributes, false);

        $knowledge->topics()->sync($topicIds);

        return $knowledge;
    }

    private function updateExtraData(Knowledge $knowledge, array $attributes, bool $forceCreate = true): array
    {
        $existingTopicIds = $forceCreate ? [] : $knowledge->topics()->pluck('id')->toArray();
        $existingQuestionIds = $forceCreate ? [] : $knowledge->topics()
            ->withPivot(['*'])
            ->pluck('question_id')
            ->toArray();
        $topics = $attributes['topics'] ?? [];
        $topicIds = [];

        foreach ($topics as $topic) {
            $topicId = (int)($topic['id'] ?? null);
            $questionEntity = null;
            $questionId = (int)($topic['question']['id'] ?? null);

            if ($topicId) {
                if ($forceCreate) {
                    unset($topic['id']);

                    /** @var Topic $topicEntity */
                    $topicEntity = $this->topicRepository->create($topic);
                } else {
                    if (!empty($topicId) && !in_array($topicId, $existingTopicIds, true)) {
                        throw new Exception('Topic is not belong to this knowledge');
                    }

                    $topicEntity = $this->topicRepository->update($topicId, $topic);
                }
            } else {
                $topicEntity = $this->topicRepository->create($topic);
            }

            $question = $topic['question'] ?? [];
            if (!empty($question)) {
                if ($question['thematic_id'] !== $knowledge->thematic_id) {
                    throw new Exception('Thematic is mismatch between Knowledge and Question');
                }

                if ($questionId) {
                    if ($forceCreate) {
                        unset($question['id']);
                        $questionEntity = $this->questionRepository->create($question);
                    } else {
                        if (!empty($questionId) && !in_array($questionId, $existingQuestionIds, true)) {
                            throw new Exception('Question is not belong to this knowledge');
                        }

                        $questionEntity = $this->questionRepository->update($questionId, $question);
                    }
                } else {
                    $questionEntity = $this->questionRepository->create($question);
                }
            }

            $knowledge->topics()->syncWithPivotValues(
                [$topicEntity->id],
                [
                    'topic_start' => $topic['topic_start'] ?? 0,
                    'question_id' => $questionEntity->id ?? null,
                    'question_start' => $topic['question_start'] ?? 0,
                    'question_end' => $topic['question_end'] ?? 0
                ],
                false
            );

            $topicIds[] = $topicEntity->id;
        }

        $diff = array_diff($existingTopicIds, $topicIds);

        if (!empty($diff)) {
            $this->topicRepository->deleteQuitely($diff);
        }

        return $topicIds;
    }

    public function assignQuestions(Knowledge $knowledge, array $questionIds): array
    {
        $assignedQuestionIds = [];

        foreach ($questionIds as $questionId) {
            /** @var null|Question $question */
            $question = Question::find($questionId);

            if (null === $question) {
                continue;
            }

            if ($question->thematic_id !== $knowledge->thematic_id) {
                continue;
            }

            $assignedQuestionIds[] = $question->id;
        }

        if (empty($assignedQuestionIds)) {
            return [];
        }

        $knowledge->questions()->syncWithoutDetaching($assignedQuestionIds);

        return $assignedQuestionIds;
    }
}
