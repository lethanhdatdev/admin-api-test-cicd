<?php

namespace App\Repository;

use App\Models\Unit;

class UnitRepository extends AbstractBaseRepository
{
    protected array $filterable = [];
    protected array $searchable = ['name'];
    protected array $sortable = ['id', 'name', 'created_at'];

    protected function getModel(): string
    {
        return Unit::class;
    }
}
