<?php

namespace App\Repository;

use App\Http\Requests\IListRequest;
use App\Models\Learn;
use App\Models\Semester;
use App\Models\Thematic;
use Exception;
use Illuminate\Database\Eloquent\Builder;

class LearnRepository extends AbstractBaseRepository
{
    protected array $filterable = ['type', 'year', 'status', 'subject_id', 'unit_id'];
    protected array $searchable = ['name', 'code', 'description'];
    protected array $sortable = ['name', 'code', 'created_at', 'price', 'description'];

    protected function getModel(): string
    {
        return Learn::class;
    }

    public function create($attributes = []): Learn
    {
        /** @var Learn $entity */
        $entity = parent::create($attributes);

        $this->updateAssociate($entity, $attributes);

        return $entity;
    }

    public function update($id, $attributes = []): Learn|bool
    {
        $entity = parent::update($id, $attributes);

        $this->updateAssociate($entity, $attributes);

        return $entity;
    }

    private function updateAssociate(Learn $learn, array $attributes): void
    {
        if ($learn->isThematic()) {
            if (empty($attributes['thematic_id'])) {
                throw new Exception('Không tìm thấy chuyên đề');
            }

            $learn->entity()->associate(Thematic::find($attributes['thematic_id']));
            $learn->save();
        }

        if ($learn->isSemester()) {
            if (empty($attributes['semester_id'])) {
                throw new Exception('Không tìm thấy học kỳ');
            }

            $learn->entity()->associate(Semester::find($attributes['semester_id']));
            $learn->save();
        }
    }
}
