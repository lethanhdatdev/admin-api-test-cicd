<?php

namespace App\Repository;

use App\Http\Requests\IListRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

/**
 * @property array $filterable
 * @property array $searchable
 */
interface IBaseRepository extends IOptionsRepository
{
    /**
     * Get all
     * @return mixed
     */
    public function getAll();

    public function list(IListRequest $request): LengthAwarePaginator;

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id): mixed;

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create($attributes = []);

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, $attributes = []);

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id);

    public function firstOrCreate(array $attributes = [], array $values = []): Model;

    public function firstOrNew(array $attributes = [], array $values = []): Model;

    public function deleteQuitely(array|int $id): void;
}
