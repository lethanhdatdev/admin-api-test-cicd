<?php

namespace App\Repository;

use App\Models\Subject;

class SubjectRepository extends AbstractBaseRepository
{
    protected array $filterable = [];
    protected array $searchable = ['name'];
    protected array $sortable = ['id', 'name', 'created_at'];

    protected function getModel(): string
    {
        return Subject::class;
    }
}
