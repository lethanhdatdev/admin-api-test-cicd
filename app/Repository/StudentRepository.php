<?php

namespace App\Repository;

use App\Models\Student;

class StudentRepository extends AbstractBaseRepository
{
    protected array $filterable = ['ancestor_id', 'unit_id', 'lock'];
    protected array $searchable = ['address', 'phone', 'code', 'user' => ['name', 'email']];
    protected array $sortable = ['id', 'code', 'birthday', 'address', 'phone', 'created_at', 'point'];

    protected function getModel(): string
    {
        return Student::class;
    }
}
