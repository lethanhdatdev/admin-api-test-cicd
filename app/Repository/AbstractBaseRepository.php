<?php

namespace App\Repository;

use App\Http\Requests\IListRequest;
use App\Http\Requests\IOptionsRequest;
use App\Http\Resources\OptionResource;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;

abstract class AbstractBaseRepository implements IBaseRepository
{
    protected Model $model;

    protected array $filterable = [];
    protected array $searchable = [];

    public const SEARCH_PARAM = 'search';
    public const SORT_PARAM = 'sort';
    public const SORT_DIRECTION_PARAM = 'direction';
    protected array $sortable = [];
    protected array $directionable = ['asc', 'desc'];
    protected string $defaultSortColumn = 'id';
    protected string $defaultSortDirection = 'desc';

    public function __construct()
    {
        $this->setModel();
    }

    abstract protected function getModel(): string;

    private function setModel(): void
    {
        $this->model = app()->make($this->getModel());
    }

    public function getAll(): Collection
    {
        return $this->model->all();
    }

    public function list(IListRequest $request): LengthAwarePaginator
    {
        /** @var Builder $queryBuilder */
        $queryBuilder = $this->prepareListQuery($request);

        // @TODO: Legacy code. Remove after Admin-HTML already implemented new way
        if ($request->filters || $request->globalFilter) {
            if ($request->filters && !empty($this->filterable)) {
                $filters = json_decode($request->filters, false, 512, JSON_THROW_ON_ERROR);
                $queryBuilder->where(static function (Builder $query) use ($filters) {
                    foreach ($filters as $filter) {
                        $query->where($filter->id, 'LIKE', '%' . $filter->value . '%', 'and');
                    }
                });
            }

            if ($request->globalFilter && !empty($this->searchable)) {
                $searchText = $request->globalFilter;
                $searchable = $this->searchable;
                $queryBuilder->where(static function (Builder $query) use ($searchText, $searchable) {
                    foreach ($searchable as $searchableField) {
                        $query->where($searchableField, 'LIKE', '%' . $searchText . '%', 'or');
                    }
                });
            }
        }

        return $queryBuilder->paginate($request->size, ['*'], 'page', $request->page + 1);
    }

    public function options(
        IOptionsRequest $request,
        string $labelColumn = 'name',
        string $valueColumn = 'id',
        string $sortByColumn = 'name'
    ): Collection {
        /** @var Builder $queryBuilder */
        $queryBuilder = $this->model::select([
                                                 $labelColumn . ' as ' . OptionResource::LABEL_KEY,
                                                 $valueColumn . ' as ' . OptionResource::VALUE_KEY
                                             ]);

        if ($request->filters && !empty($this->filterable)) {
            $filters = json_decode($request->filters, false, 512, JSON_THROW_ON_ERROR);
            $queryBuilder->where(static function (Builder $query) use ($filters) {
                foreach ($filters as $filter) {
                    $query->where($filter->id, '=', $filter->value, 'and');
                }
            });
        }

        return $queryBuilder->limit(50)->orderBy($sortByColumn)->get();
    }

    public function find($id): mixed
    {
        return $this->model->findOrFail($id);
    }

    public function create($attributes = []): Model
    {
        return $this->model->create($attributes);
    }

    public function update($id, $attributes = []): bool|Model
    {
        $result = $this->find($id);

        $result->updateOrFail($attributes);

        return $result;
    }

    public function deleteQuitely(array|int $id): void
    {
        /** @var Collection $collection */
        $collection = $this->model->find($id);
        $collection->each(static function (Model $item) {
            $item->deleteQuietly();
        });
    }

    public function delete($id): bool
    {
        $result = $this->find($id);

        if ($result) {
            $result->delete();

            return true;
        }

        return false;
    }

    public function firstOrCreate(array $attributes = [], array $values = []): Model
    {
        return $this->model->firstOrCreate($attributes, $values);
    }

    public function firstOrNew(array $attributes = [], array $values = []): Model
    {
        return $this->model->firstOrNew($attributes, $values);
    }

    protected function prepareListQuery(IListRequest $request): Builder
    {
        /** @var Builder $queryBuilder */
        $queryBuilder = $this->model::query();

        $this->prepareFilterQuery($request, $queryBuilder);
        $this->prepareFilterRelationQuery($request, $queryBuilder);

        $searchText = $request->get(self::SEARCH_PARAM);
        if (!empty($searchText) && !empty($this->searchable)) {
            $this->prepareSearchQuery($searchText, $queryBuilder);
            $this->prepareRelationSearchQuery($searchText, $queryBuilder);
        }

        $this->prepareSorting($request, $queryBuilder);

        return $queryBuilder;
    }

    protected function prepareSorting(IListRequest $request, Builder $query): void
    {
        if ($request->exists(self::SORT_PARAM)) {
            $sort = $request->get(self::SORT_PARAM, $this->defaultSortColumn);
            $sort = in_array($sort, $this->sortable, true) ? $sort : $this->defaultSortColumn;
            $direction = $request->get(self::SORT_DIRECTION_PARAM, $this->defaultSortDirection);

            $query->orderBy($sort, $direction);
        } elseif ($request->exists('sorting')) {
            // @TODO: There params below will be remove in future.
            $sorting = json_decode($request->get('sorting'), false, 512, JSON_THROW_ON_ERROR);
            foreach ($sorting as $sort) {
                if (!in_array($sort->id, $this->sortable, true)) {
                    continue;
                }

                $query->orderBy($sort->id, $sort->desc ? 'desc' : 'asc');
            }
        } else {
            $query->orderBy($this->defaultSortColumn, $this->defaultSortDirection);
        }
    }

    public function getFilterable(): array
    {
        return $this->filterable;
    }

    public function getSearchable(): array
    {
        return $this->searchable;
    }

    public function getSortable(): array
    {
        return $this->sortable;
    }

    public function getDirectionable(): array
    {
        return $this->directionable;
    }

    protected function prepareSearchQuery(string $searchText, Builder $builder): void
    {
        $searchableColumns = array_filter($this->searchable, static function ($item) {
            return is_string($item);
        });

        $builder->where(static function (Builder $query) use ($searchText, $searchableColumns) {
            foreach ($searchableColumns as $searchColumn) {
                if (is_string($searchColumn)) {
                    $query->orWhere($searchColumn, 'ILIKE', '%' . $searchText . '%');
                }
            }
        });
    }

    protected function prepareRelationSearchQuery(string $searchText, Builder $builder): void
    {
        $searchableColumns = array_filter($this->searchable, static function ($item) {
            return is_array($item);
        });

        foreach ($searchableColumns as $relation => $columns) {
            $builder->orWhereHas($relation, static function (Builder $subQuery) use ($searchText, $columns) {
                $subQuery->whereNested(static function (QueryBuilder $_builder) use ($searchText, $columns) {
                    foreach ($columns as $column) {
                        $_builder->orWhere($column, 'ILIKE', '%' . $searchText . '%');
                    }
                });
            });
        }
    }

    protected function prepareFilterQuery(IListRequest $request, Builder $builder): void
    {
        $filterColumns = array_filter($this->filterable, static function ($item) {
            return is_string($item);
        });

        foreach ($filterColumns as $filterColumn) {
            if (!$request->exists($filterColumn)) {
                continue;
            }

            $filterValue = $request->get($filterColumn);
            $builder->where(static function (Builder $query) use ($filterColumn, $filterValue) {
                $query->where($filterColumn, '=', $filterValue, 'and');
            });
        }
    }

    protected function prepareFilterRelationQuery(IListRequest $request, Builder $builder): void
    {
        $filterRelationColumns = array_filter($this->filterable, static function ($item) {
            return is_array($item);
        });

        foreach ($filterRelationColumns as $filterColumn => $filterRelations) {
            if (!$request->exists($filterColumn)) {
                continue;
            }

            $value = $request->get($filterColumn);

            foreach ($filterRelations as $relation => $column) {
                if (is_array($column)) {
                    $builder->whereHas($relation, static function (Builder $subQuery) use ($value, $column) {
                        foreach ($column as $subRelation => $relationColumn) {
                            $subQuery->whereHas(
                                $subRelation,
                                static function (Builder $subQuery) use ($value, $relationColumn) {
                                    $subQuery->whereNested(
                                        static function (QueryBuilder $relationBuilder) use ($value, $relationColumn) {
                                            $relationBuilder->orWhere($relationColumn, '=', $value);
                                        }
                                    );
                                }
                            );
                        }
                    });
                } else {
                    $builder->whereHas($relation, static function (Builder $subQuery) use ($value, $column) {
                        $subQuery->whereNested(
                            static function (QueryBuilder $relationBuilder) use ($value, $column) {
                                $relationBuilder->orWhere($column, '=', $value);
                            }
                        );
                    });
                }
            }
        }
    }
}
