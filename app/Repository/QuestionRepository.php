<?php

namespace App\Repository;

use App\Models\Question;
use App\Services\QuestionService;
use Exception;

class QuestionRepository extends AbstractBaseRepository
{
    protected array $filterable = ['type', 'thematic_id'];
    protected array $searchable = ['description', 'suggestion'];
    protected array $sortable = ['id', 'description', 'type', 'suggestion', 'created_at'];

    public function __construct(private AnswerRepository $answerRepository)
    {
        parent::__construct();
    }

    protected function getModel(): string
    {
        return Question::class;
    }

    public function create($attributes = []): Question
    {
        /** @var Question $question */
        $question = parent::create($attributes);

        $answers = $attributes['answers'] ?? [];

        $this->updateOrCreateAnswers($question, $answers);

        if ($question->type === QuestionService::TYPE_SORT) {
            $sortCorrect = array_map(static function ($item) {
                return $item['description'];
            }, $answers);
            $question->updateQuietly(['sort_correct' => implode(',', $sortCorrect)]);
        }

        return $question;
    }

    public function update($id, $attributes = []): Question
    {
        /** @var Question $question */
        $question = parent::update($id, $attributes);

        $answers = $attributes['answers'] ?? [];

        $this->updateOrCreateAnswers($question, $answers, false);

        if ($question->type === QuestionService::TYPE_SORT) {
            $sortCorrect = array_map(static function ($item) {
                return $item['description'];
            }, $answers);
            $question->updateQuietly(['sort_correct' => implode(',', $sortCorrect)]);
        }

        return $question;
    }

    private function updateOrCreateAnswers(Question $question, array $answers, bool $forceCreate = true): void
    {
        $existingAnswerIds = $question->answers()->pluck('id')->toArray();

        foreach ($answers as $answer) {
            $answerId = (int)($answer['id'] ?? null);
            $answer['type'] = $question->type;
            $answer['question_id'] = $question->id;

            if ($answerId) {
                if ($forceCreate) {
                    unset($answer['id']);
                    $this->answerRepository->create($answer);
                } else {
                    if (!empty($answerId) && !in_array($answerId, $existingAnswerIds, true)) {
                        throw new Exception('Answer is not belong to this question');
                    }

                    $this->answerRepository->update($answerId, $answer);
                }
            } else {
                $this->answerRepository->create($answer);
            }
        }
    }
}
