<?php

namespace App\Repository;

use App\Models\PracticeCategory;

class PracticeCategoryRepository extends AbstractBaseRepository
{
    protected array $filterable = [];
    protected array $searchable = ['name', 'description'];
    protected array $sortable = ['id', 'name', 'description', 'created_at'];

    protected function getModel(): string
    {
        return PracticeCategory::class;
    }
}
