<?php

namespace App\Repository;

use App\Models\Practice;
use App\Models\Question;
use App\Models\Thematic;

class PracticeRepository extends AbstractBaseRepository
{
    protected array $filterable = [
        'semester_id',
        'subject_id',
        'difficulty',
        'practice_category_id',
        'unit_id' => ['semester' => 'unit_id'],
        'thematic_id' => ['questions' => 'thematic_id']
    ];
    protected array $searchable = ['name', 'code'];
    protected array $sortable = [
        'id',
        'created_at',
        'name',
        'code',
        'semester_id',
        'subject_id',
        'difficulty',
        'practice_category_id'
    ];

    protected function getModel(): string
    {
        return Practice::class;
    }

    public function create($attributes = []): Practice
    {
        /** @var Practice $practiceEntity */
        $practiceEntity = parent::create($attributes);

        $this->updateQuestions($attributes['questions'] ?? [], $practiceEntity);

        return $practiceEntity;
    }

    public function update($id, $attributes = []): Practice
    {
        /** @var Practice $practice */
        $practice = parent::update($id, $attributes);

        $this->updateQuestions($attributes['questions'] ?? [], $practice, false);

        return $practice;
    }

    private function updateQuestions(array $questions, Practice $practice, bool $isCreating = true): void
    {
        $questionIds = [];
        $existingQuestionIds = $isCreating ? [] : $practice->questions()->pluck('id')->toArray();
        foreach ($questions as $question) {
            $questionId = (int)($question['id'] ?? null);
            array_push($questionIds, $questionId);
        }

        $practice->questions()->sync($questionIds);
    }

    public function assignQuestions(Practice $practice, array $questionIds): array
    {
        $assignedQuestionIds = [];

        foreach ($questionIds as $questionId) {
            $question = Question::find($questionId);

            if (null === $question) {
                continue;
            }

            /** @var Question $question */
            if (!Thematic::where('id', $question->thematic_id)->where('subject_id', $practice->subject_id)->where(
                'semester_id',
                $practice->semester_id
            )->exists()) {
                continue;
            }

            $assignedQuestionIds[] = $question->id;
        }

        if (empty($assignedQuestionIds)) {
            return [];
        }

        $practice->questions()->syncWithoutDetaching($assignedQuestionIds);

        return $assignedQuestionIds;
    }
}
