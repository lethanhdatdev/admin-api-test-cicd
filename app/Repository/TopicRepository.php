<?php

namespace App\Repository;

use App\Models\Topic;

class TopicRepository extends AbstractBaseRepository
{
    protected array $filterable = [];
    protected array $searchable = ['name', 'intro', 'description'];

    protected function getModel(): string
    {
        return Topic::class;
    }
}
