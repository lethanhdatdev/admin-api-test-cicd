<?php

namespace App\Repository;

use App\Models\Semester;

class SemesterRepository extends AbstractBaseRepository
{
    protected array $filterable = ['unit_id'];
    protected array $searchable = ['name'];
    protected array $sortable = ['id', 'name', 'created_at'];

    protected function getModel(): string
    {
        return Semester::class;
    }
}
