<?php

namespace App\Repository;

use App\Http\Requests\IListRequest;
use App\Models\Question;
use App\Models\QuestionTest;
use App\Models\Test;
use App\Models\TestSchedule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class TestScheduleRepository extends AbstractBaseRepository
{
    protected array $filterable = [
        'unit_id',
        'subject_id',
        'semester_id',
        'year'
    ];
    protected array $searchable = ['name', 'description'];
    protected array $sortable = [
        'unit_id',
        'subject_id',
        'semester_id',
        'year',
        'thematic_id',
        'name',
        'test_id',
        'start_time',
        'duration'
    ];

    protected function getModel(): string
    {
        return TestSchedule::class;
    }

    protected function prepareListQuery(IListRequest $request): Builder
    {
        $query = parent::prepareListQuery($request);
        $testScheduleTable = $this->model->getTable();
        $query->with('test', 'unit', 'subject', 'semester');
        $query->select([$testScheduleTable . '.*']);

        $subQueryForThematic = $this->createSubQueryThematic();

        $query->selectSub($subQueryForThematic, 'thematic_id');

        $thematicId = $request->get('thematic_id');
        if ($thematicId) {
            $query->where($subQueryForThematic, '=', $thematicId);
        }

        return $query;
    }

    protected function prepareSorting(IListRequest $request, Builder $query): void
    {
        $value = $request->get(self::SORT_PARAM);
        $direction = $request->get(self::SORT_DIRECTION_PARAM, $this->defaultSortDirection);

        // Custom sorting for duration
        if ($value === 'duration') {
            $testTable = App::make(Test::class)->getTable();
            $testScheduleTable = $this->model->getTable();
            $query->leftJoin($testTable, $testScheduleTable . '.test_id', '=', $testTable . '.id');
            $query->reorder($testTable . '.duration', $direction);
        } else {
            parent::prepareSorting($request, $query);
        }
    }

    private function createSubQueryThematic(): \Illuminate\Database\Query\Builder
    {
        $testScheduleTable = $this->model->getTable();
        $questionTable = App::make(Question::class)->getTable();
        $questionTestsTable = App::make(QuestionTest::class)->getTable();

        return DB::query()->select($questionTable . '.thematic_id')
            ->from($questionTable)
            ->leftJoin($questionTestsTable, $questionTestsTable . '.question_id', '=', $questionTable . '.id')
            ->where(
                $questionTestsTable . '.test_id',
                '=',
                DB::raw($testScheduleTable . '.test_id')
            )
            ->orderByRaw('COUNT(' . $questionTable . '.thematic_id) DESC, ' . $questionTable . '.thematic_id DESC')
            ->groupBy($questionTable . '.thematic_id')
            ->limit(1);
    }

    /**
     * @throws \Exception
     */
    public function create($attributes = []): TestSchedule|Model
    {
        /** @var Test $test */
        $test = Test::find($attributes['test_id']);

        if (!$test->isOfficial()) {
            throw new \Exception("Bài thi này không khả dụng");
        }

        return parent::create($attributes);
    }
}
