<?php

namespace App\Repository;

use App\Models\Answer;

class AnswerRepository extends AbstractBaseRepository
{
    protected array $filterable = ['correct', 'type', 'question_id'];
    protected array $searchable = ['description'];
    protected array $sortable = ['id', 'description', 'type', 'created_at', 'correct'];

    protected function getModel(): string
    {
        return Answer::class;
    }
}
