<?php

namespace App\Imports;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Thematic;
use App\Services\QuestionService;
use Exception;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Row;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\RichText\Run;
use PhpOffice\PhpSpreadsheet\Style\Font;

class QuestionsImport implements OnEachRow, WithStartRow, WithHeadingRow
{
    use Importable;

    public const COUNT = 'import.questions';

    public function onRow(Row $row)
    {
        $rowData = $row->toArray();
        $rowCellsIndex = array_keys($rowData);
        $answers = [];
        $correctAnswer = '';

        $index = 0;
        foreach ($row->getCellIterator() as $cell) {
            $cellValue = $cell->getValue();

            if ($cellValue instanceof RichText) {
                $cellValue = $this->processRichText($cellValue);
            }

            $rowKey = $rowCellsIndex[$index];

            if (str_starts_with($rowKey, 'cau_tra_loi')) {
                if ($rowKey === 'cau_tra_loi_dung') {
                    $correctAnswer = trim($cellValue);
                } else {
                    $answers[] = ['description' => $cellValue, 'correct' => false];
                }
            }

            $rowData[$rowCellsIndex[$index]] = $cellValue;
            $index++;
        }

        foreach ($answers as $key => $answer) {
            if (empty($answer['description'])) {
                unset($answers[$key]);
                continue;
            }
            $answers[$key]['correct'] = $answer['description'] === $correctAnswer;
        }

        $this->store($rowData, $answers);
    }

    private function processRichText(RichText $richText): string
    {
        $results = [];

        foreach ($richText->getRichTextElements() as $element) {
            if ($element instanceof Run) {
                /** @var Font $eleFont */
                $eleFont = $element->getFont();
                $eleText = trim($element->getText());

                if ($eleFont->getSuperscript()) {
                    $eleText = '<sup>' . $eleText . '</sup>';
                } elseif ($eleFont->getSubscript()) {
                    $eleText = '<sub>' . $eleText . '</sub>';
                } elseif ($eleFont->getStrikethrough()) {
                    $eleText = '<del>' . $eleText . '</del>';
                }
            } else {
                $eleText = $element->getText();
            }

            $results[] = $eleText;
        }

        return implode('', $results);
    }

    private function store(array $row, array $answers): void
    {
        $chuyenDe = trim($row['chuyen_de']);
        $thematic = Thematic::where('name', $chuyenDe)->first();

        if (!$thematic) {
            report('Chuyên đề ' . $chuyenDe . ' không tồn tại');
            return;
        }

        $cauHoi = trim($row['cau_hoi']);

        if (empty($cauHoi)) {
            report('Nội dung câu hỏi rỗng. Bỏ qua');
            return;
        }

        $loaiCauHoi = mb_strtolower(trim($row['loai_cau_hoi']));

        $questionType = match ($loaiCauHoi) {
            'TracNghiem', 'trắc nghiệm' => QuestionService::TYPE_CHOICE,
            'DienKhuyet', 'điền khuyết' => QuestionService::TYPE_FILL_IN_BLANK,
            'SapXep', 'sắp xếp' => QuestionService::TYPE_SORT,
            default => throw new Exception('Unknow question type: ' . $loaiCauHoi . ' is not found'),
        };

        $cauTraLoiDung = trim($row['cau_tra_loi_dung']);
        $suggestionText = trim($row['comment_khi_tra_loi_sai']);
        $sortCorrect = $questionType === QuestionService::TYPE_SORT ? trim($row['cau_tra_loi_dung']) : null;

        /** @var Question $question */
        $question = Question::create([
            'description' => $cauHoi,
            'type' => $questionType,
            'thematic_id' => $thematic->id,
            'correct_answer' => $cauTraLoiDung,
            'suggestion' => $suggestionText,
            'sort_correct' => $sortCorrect,
        ]);

        foreach ($answers as $answer) {
            $answer['question_id'] = $question->id;
            $answer['type'] = $questionType;
            Answer::create($answer);
        }

        $count = (int) Session::get(self::COUNT);
        $count++;

        Session::put(self::COUNT, $count);
    }

    public function startRow(): int
    {
        return 2;
    }
}
