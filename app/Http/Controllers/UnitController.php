<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionsDefaultRequest;
use App\Http\Requests\UnitCreateRequest;
use App\Http\Requests\UnitListRequest;
use App\Http\Resources\OptionsResource;
use App\Http\Resources\UnitResource;
use App\Http\Resources\UnitsResource;
use App\Models\Unit;
use App\Models\Practice;
use App\Models\Test;
use App\Models\Student;
use App\Models\Thematic;
use App\Models\Semester;
use App\Repository\UnitRepository;
use Illuminate\Support\Facades\DB;

class UnitController extends Controller
{
    public function __construct(private UnitRepository $repository)
    {
    }

    public function index(UnitListRequest $request): UnitsResource
    {
        return new UnitsResource($this->repository->list($request));
    }

    public function store(UnitCreateRequest $request): UnitResource
    {
        $result = null;

        DB::beginTransaction();
        try {
            $result = Unit::create($request->all());
            $semesters = [
                [
                    'name' => 'Học kỳ 1',
                    'unit_id' => $result->id,
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Học kỳ 2',
                    'unit_id' => $result->id,
                    'created_at' => date('Y-m-d H:i:s')
                ],
            ];
            Semester::insert($semesters);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();
            report($e);

            return false;
        }

        return new UnitResource($result);
    }

    public function show(Unit $unit): UnitResource
    {
        return new UnitResource($unit);
    }

    public function update(UnitCreateRequest $request, Unit $unit): UnitResource
    {
        $unit = $this->repository->update($unit->id, $request->all());

        return new UnitResource($unit);
    }

    public function destroy(Unit $unit): void
    {
        $unit->deleteOrFail();
    }

    public function options(OptionsDefaultRequest $request): OptionsResource
    {
        $data = $this->repository->options($request);

        return new OptionsResource($data->toArray());
    }
}
