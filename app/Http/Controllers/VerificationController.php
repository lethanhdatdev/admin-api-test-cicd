<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmsVerifyCheckRequest;
use App\Http\Requests\SmsVerifyStartRequest;
use App\Services\OTPService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class VerificationController extends Controller
{
    public function start(SmsVerifyStartRequest $request): JsonResponse
    {
        try {
            if (!OTPService::startOTP($request->phone)) {
                return response()->json(['message' => 'Error on sending OTP SMS'], 500);
            }

            return response()->json();
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }

    public function check(SmsVerifyCheckRequest $request): JsonResponse
    {
        try {
            if (!OTPService::otpCheck($request->phone, $request->code)) {
                return response()->json(['message' => 'Invalid code'], 400);
            }

            return response()->json(['message' => 'OTP verify successful!']);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }
}
