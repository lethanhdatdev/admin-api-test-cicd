<?php

namespace App\Http\Controllers\Traits;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait AmazonS3FileHandler
{
    public function amazonS3UploadFile(UploadedFile|null $uploadedFile, string $key): array|null
    {
        if (null === $uploadedFile) {
            return null;
        }

        $uploadedFilePath = Storage::put($key, $uploadedFile);
        $imageType = explode('/', $uploadedFile->getMimeType());
        $imageType = $imageType[0];

        return [
            'name' => $uploadedFile->getClientOriginalName(),
            'mime' => $uploadedFile->getMimeType(),
            'type' => $imageType,
            'path' => $uploadedFilePath
        ];
    }

    public function associateFile(Model $model, array|null $data, File $fileEntity = null): void
    {
        if (null === $data) {
            return;
        }

        if (null !== $fileEntity) {
//            $this->deleteFile($fileEntity->path);
            $fileEntity->update($data);
        } else {
            $fileEntity = File::create($data);
            $fileEntity->entity()->associate($model);
            $fileEntity->save();
        }
    }

    public function deleteFile(string $path = null): void
    {
        if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }
}
