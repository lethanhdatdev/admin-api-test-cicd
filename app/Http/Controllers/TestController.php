<?php

namespace App\Http\Controllers;

use App\Enums\TestType;
use App\Http\Controllers\Traits\AmazonS3FileHandler;
use App\Http\Requests\Test\GetTestDataWhenCreateEventRequest;
use App\Http\Requests\TestAssignQuestionsRequest;
use App\Http\Requests\TestCreateRequest;
use App\Http\Requests\TestListRequest;
use App\Http\Requests\TestUpdateRequest;
use App\Http\Resources\SimpleTestResource;
use App\Http\Resources\TestDetailResource;
use App\Http\Resources\TestsResource;
use App\Models\Test;
use App\Repository\TestRepository;
use Exception;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    use AmazonS3FileHandler;

    public function __construct(private TestRepository $repository)
    {
    }

    public function index(TestListRequest $request): TestsResource
    {
        return new TestsResource($this->repository->list($request));
    }

    public function store(TestCreateRequest $request): TestDetailResource
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'test');

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            $test = $this->repository->create($request->only($keys));
            $this->associateFile($test, $imageFile);

            DB::commit();

            $test->refresh();

            return new TestDetailResource($test);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function show(Test $test): TestDetailResource
    {
        return new TestDetailResource($test);
    }

    public function update(TestUpdateRequest $request, Test $test): TestDetailResource
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'test');

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            /** @var Test $test */
            $test = $this->repository->update($test->id, $request->only($keys));
            $this->associateFile($test, $imageFile, $test->image);

            DB::commit();

            $test->refresh();

            return new TestDetailResource($test);

        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function destroy(Test $test): void
    {
        try {
            DB::beginTransaction();

            $test->questions()->detach();

            if ($test->image) {
                $this->deleteFile($test->image->path);

                $test->image->delete();
            }

            $test->deleteOrFail();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function assignQuestions(TestAssignQuestionsRequest $request, int $id): JsonResponse
    {
        try {
            DB::beginTransaction();

            $test = Test::findOrFail($id);
            $questionIds = $request->get('ids');
            $results = $this->repository->assignQuestions($test, $questionIds);

            DB::commit();

            return response()->json(
                [
                    'success' => $results,
                    'fail' => array_values(array_diff($questionIds, $results))
                ]
            );
        } catch (Exception $exception) {
            DB::rollBack();
            throw new Exception();
        }
    }

    public function getTestData(GetTestDataWhenCreateEventRequest $request)
    {
        $tests = Test::where('subject_id', $request->subject_id)->whereHas('semester', function (Builder $query) use ($request) {
            $query->where('unit_id', $request->unit_id);
        })->where('type', '=', TestType::EVENT)->select('id', 'name')->get();
        return SimpleTestResource::collection($tests);
    }
}
