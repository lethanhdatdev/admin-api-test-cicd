<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionsDefaultRequest;
use App\Http\Requests\PracticeCategoryCreateRequest;
use App\Http\Requests\PracticeCategoryListRequest;
use App\Http\Requests\PracticeCategoryUpdateRequest;
use App\Http\Resources\OptionsResource;
use App\Http\Resources\PracticeCategoriesResource;
use App\Http\Resources\PracticeCategoryResource;
use App\Models\PracticeCategory;
use App\Repository\PracticeCategoryRepository;

class PracticeCategoryController extends Controller
{
    public function __construct(private PracticeCategoryRepository $repository)
    {
    }

    public function index(PracticeCategoryListRequest $request): PracticeCategoriesResource
    {
        return new PracticeCategoriesResource($this->repository->list($request));
    }

    public function store(PracticeCategoryCreateRequest $request): PracticeCategoryResource
    {
        return new PracticeCategoryResource($this->repository->create($request->all()));
    }

    public function show(PracticeCategory $practiceCategory): PracticeCategoryResource
    {
        return new PracticeCategoryResource($practiceCategory);
    }

    public function update(PracticeCategoryUpdateRequest $request, PracticeCategory $practiceCategory): PracticeCategoryResource
    {
        return new PracticeCategoryResource($this->repository->update($practiceCategory->id, $request->all()));
    }

    public function destroy(PracticeCategory $practiceCategory): void
    {
        $practiceCategory->deleteOrFail();
    }

    public function options(OptionsDefaultRequest $request): OptionsResource
    {
        $data = $this->repository->options($request);

        return new OptionsResource($data->toArray());
    }
}
