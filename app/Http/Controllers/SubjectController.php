<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionsDefaultRequest;
use App\Http\Requests\SubjectCreateRequest;
use App\Http\Requests\SubjectListRequest;
use App\Http\Requests\SubjectUpdateRequest;
use App\Http\Resources\OptionsResource;
use App\Http\Resources\SubjectResource;
use App\Http\Resources\SubjectsResource;
use App\Models\Subject;
use App\Repository\SubjectRepository;

class SubjectController extends Controller
{
    public function __construct(private SubjectRepository $repository)
    {
    }

    public function index(SubjectListRequest $request): SubjectsResource
    {
        return new SubjectsResource($this->repository->list($request));
    }

    public function store(SubjectCreateRequest $request): SubjectResource
    {
        return new SubjectResource(Subject::create($request->all()));
    }

    public function show(Subject $subject): SubjectResource
    {
        return new SubjectResource($subject);
    }

    public function update(SubjectUpdateRequest $request, Subject $subject): SubjectResource
    {
        $this->repository->update($subject->id, $request->all());

        return new SubjectResource($subject);
    }

    public function destroy(Subject $subject): void
    {
        $subject->deleteOrFail();
    }

    public function options(OptionsDefaultRequest $request): OptionsResource
    {
        $data = $this->repository->options($request);

        return new OptionsResource($data->toArray());
    }
}
