<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\AmazonS3FileHandler;
use App\Http\Requests\KnowledgeCreateRequest;
use App\Http\Requests\KnowledgeListRequest;
use App\Http\Requests\KnowledgeUpdateRequest;
use App\Http\Resources\KnowledgeDetailResource;
use App\Http\Resources\KnowledgeListResource;
use App\Models\Knowledge;
use App\Models\SyncVideo;
use App\Repository\KnowledgeRepository;
use Exception;
use Illuminate\Support\Facades\DB;

class KnowledgeController extends Controller
{
    use AmazonS3FileHandler;

    public function __construct(private KnowledgeRepository $repository)
    {
    }

    public function index(KnowledgeListRequest $request): KnowledgeListResource
    {
        return new KnowledgeListResource($this->repository->list($request));
    }

    public function store(KnowledgeCreateRequest $request): KnowledgeDetailResource
    {
        list($imageFile, $videoFile, $duration) = $this->getMediaFiles($request);

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });
            $data = $request->only($keys);
            $data['duration'] = $duration;

            /** @var Knowledge $knowledge */
            $knowledge = $this->repository->create($data);
            $this->associateFile($knowledge, $imageFile);
            $this->associateFile($knowledge, $videoFile);

            DB::commit();

            return new KnowledgeDetailResource($knowledge);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            if ($videoFile) {
                $this->deleteFile($videoFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function show(Knowledge $knowledge): KnowledgeDetailResource
    {
        return new KnowledgeDetailResource($knowledge);
    }

    public function update(KnowledgeUpdateRequest $request, Knowledge $knowledge): KnowledgeDetailResource
    {
        list($imageFile, $videoFile, $duration) = $this->getMediaFiles($request);

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            /** @var Knowledge $knowledge */
            $data = $request->only($keys);
            $data['duration'] = $duration;
            $knowledge = $this->repository->update($knowledge->id, $data);
            $image = $knowledge->files->filter(static function ($item) {
                return $item->type === 'image';
            })->first();

            $video = $knowledge->files->filter(static function ($item) {
                return $item->type === 'video';
            })->first();

            $this->associateFile($knowledge, $imageFile, $image);
            $this->associateFile($knowledge, $videoFile, $video);

            DB::commit();

            return new KnowledgeDetailResource($knowledge);
        } catch (Exception $exception) {
            dd($exception->getFile(), $exception->getLine());
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            if ($videoFile) {
                $this->deleteFile($videoFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function destroy(Knowledge $knowledge): void
    {
        try {
            DB::beginTransaction();

            $knowledge->topics()->detach();

            foreach ($knowledge->files as $file) {
                $this->deleteFile($file->path);
                $file->delete();
            }

            $knowledge->deleteOrFail();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function getMediaFiles($request): array
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'knowledge');
        $videoFile = null;
        $duration = $request->duration ?? '0.0';
        if (!$request->sync_video_id && $request->file('video')) {
            $videoFile = $this->amazonS3UploadFile($request->file('video'), 'knowledge');
        } elseif ($request->sync_video_id) {
            $video = SyncVideo::where('id', $request->sync_video_id)->first();
            if ($video) {
                $duration = $video->duration;
                $videoFile = [
                    'name' => $video->name,
                    'mime' => 'video/mp4',
                    'type' => 'video',
                    'path' => config('services.amazon-s3.prefix')."/$video->file"
                ];
            }
        }
        return array($imageFile, $videoFile, $duration);
    }
}
