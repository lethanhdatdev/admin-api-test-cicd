<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderListRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    /**
     * @param OrderListRequest $request
     * @param OrderService $orderService
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function index(OrderListRequest $request, OrderService $orderService)
    {
        try {
            $data = $orderService->list($request->validated());
            return OrderResource::collection($data);
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['message' => 'INTERNAL_SERVER_ERROR'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $code
     * @return OrderResource|JsonResponse
     */
    public function show($code)
    {
        try {
            $order = Order::where('code', $code)->with('customer', 'appliedUser', 'learn.entity')->first();
            if (!$order) {
                return response()->json(['error' => 'ERROR.ORDER_NOT_FOUND'], Response::HTTP_NOT_FOUND);
            }
            return new OrderResource($order);
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['message' => 'INTERNAL_SERVER_ERROR'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $code
     * @param UpdateOrderRequest $request
     * @param OrderService $orderService
     * @return OrderResource|JsonResponse
     */
    public function update($code, UpdateOrderRequest $request, OrderService $orderService)
    {
        try {
            $order = Order::where('code', $code)->firstOrFail();
            if (in_array($order->status->value, ['SUCCESS', 'CANCELLED'])) {
                return response()->json(['message' => 'INVALID_ORDER'], Response::HTTP_BAD_REQUEST);
            }
            $order = $orderService->updateOrder($order, $request->validated());
            return new OrderResource($order);
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['message' => 'INTERNAL_SERVER_ERROR'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
