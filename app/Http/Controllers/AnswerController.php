<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnswerCreateRequest;
use App\Http\Requests\AnswerListRequest;
use App\Http\Requests\AnswerUpdateRequest;
use App\Http\Resources\AnswerListResource;
use App\Http\Resources\AnswerResource;
use App\Models\Answer;
use App\Repository\AnswerRepository;

class AnswerController extends Controller
{
    public function __construct(private AnswerRepository $repository)
    {
    }

    public function index(AnswerListRequest $request): AnswerListResource
    {
        return new AnswerListResource($this->repository->list($request));
    }

    public function store(AnswerCreateRequest $request): AnswerResource
    {
        return new AnswerResource($this->repository->create($request->all()));
    }

    public function show(Answer $answer): AnswerResource
    {
        return new AnswerResource($answer);
    }

    public function update(AnswerUpdateRequest $request, Answer $answer): AnswerResource
    {
        $this->repository->update($answer->id, $request->all());

        return new AnswerResource($answer);
    }

    public function destroy(Answer $answer): void
    {
        $answer->deleteOrFail();
    }
}
