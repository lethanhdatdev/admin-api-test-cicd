<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\AmazonS3FileHandler;
use App\Http\Requests\LearnCreateRequest;
use App\Http\Requests\LearnListRequest;
use App\Http\Requests\LearnUpdateRequest;
use App\Http\Resources\LearnDetailResource;
use App\Http\Resources\LearnResource;
use App\Http\Resources\LearnsResource;
use App\Models\Learn;
use App\Repository\LearnRepository;
use App\Services\LearnService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Enums\LearnType;

class LearnController extends Controller
{
    use AmazonS3FileHandler;

    public function __construct(private readonly LearnRepository $repository)
    {
    }

    public function index(LearnListRequest $request, LearnService $learnService): LearnsResource
    {
        $data = $learnService->list($request->validated());
        return new LearnsResource($data);
    }

    /**
     * @param LearnCreateRequest $request
     * @return LearnResource|JsonResponse
     */
    public function store(LearnCreateRequest $request): JsonResponse|LearnResource
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'learn');

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            $entity = $this->repository->create($request->only($keys));
            $this->associateFile($entity, $imageFile);
            $knowledge = $entity->isSemester() ? $entity->entity->thematics->pluck('knowledge')->flatten() : $entity->entity->knowledge;
            $entity->knowledgeList()->sync($knowledge->pluck('id')->toArray());
            DB::commit();

            $entity->refresh();

            return new LearnResource($entity);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            DB::rollBack();
            return response()->json(['error' => $exception->getMessage().' - '. $exception->getLine().' - '. $exception->getFile()], 500);
        }
    }

    public function show(Learn $learn): LearnDetailResource
    {
        return new LearnDetailResource($learn);
    }

    public function update(LearnUpdateRequest $request, Learn $learn): LearnDetailResource
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'learn');

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            /** @var Learn $learn */
            $learn = $this->repository->update($learn->id, $request->only($keys));
            $this->associateFile($learn, $imageFile, $learn->image);
            $knowledge = $learn->isSemester() ? $learn->entity->thematics->pluck('knowledge')->flatten() : $learn->entity->knowledge;
            $learn->knowledgeList()->sync($knowledge->pluck('id')->toArray());

            DB::commit();

            $learn->refresh();

            return new LearnDetailResource($learn);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function destroy(Learn $learn): void
    {
        try {
            DB::beginTransaction();

            if ($learn->image) {
                $this->deleteFile($learn->image->path);
                $learn->image->delete();
            }

            $learn->deleteOrFail();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function learnTypeOptions(): array
    {
        return [
            'data' => [
                [
                    'label' => 'Gói học kì',
                    'value' => LearnType::SEMESTER,
                ],
                [
                    'label' => 'Gói chuyên đề',
                    'value' => LearnType::THEMATIC,
                ]
            ]
        ];
    }
}
