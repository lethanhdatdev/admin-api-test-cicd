<?php

namespace App\Http\Controllers;

use App\Http\Requests\AmazonS3VideoListRequest;
use App\Http\Requests\AmazonS3VideoSyncRequest;
use App\Http\Resources\AmazonS3VideoListResource;
use App\Models\SyncVideo;
use App\Services\AmazonS3VideoService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class AmazonS3VideoController extends Controller
{

    /**
     * @param AmazonS3VideoListRequest $request
     * @param AmazonS3VideoService $s3VideoService
     * @return AmazonS3VideoListResource|JsonResponse
     */
    public function index(AmazonS3VideoListRequest $request, AmazonS3VideoService $s3VideoService)
    {
        try {
            return new AmazonS3VideoListResource($s3VideoService->list($request->validated()));
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['error' => 'INTERNAL_SERVER_ERROR'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function sync(AmazonS3VideoSyncRequest $request): JsonResponse
    {
        try {
            SyncVideo::create($request->all());
            return response()->json(['message' => 'SUCCESS.VIDEO_CREATED']);
        } catch (Exception $exception) {
            Log::error($exception);
            return response()->json(['error' => 'INTERNAL_SERVER_ERROR'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
