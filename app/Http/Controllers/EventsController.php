<?php

namespace App\Http\Controllers;

use App\Enums\EventStatus;
use App\Http\Controllers\Traits\AmazonS3FileHandler;
use App\Http\Requests\Event\EventListRequest;
use App\Http\Requests\Event\StoreEventRequest;
use App\Http\Requests\Event\UpdateEventRequest;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Services\EventService;
use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class EventsController extends Controller
{
    use AmazonS3FileHandler;

    public function index(EventListRequest $request, EventService $eventService)
    {
        try {
            $data = $eventService->list($request->validated());
            return EventResource::collection($data);
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function store(StoreEventRequest $request)
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'event');

        try {
            DB::beginTransaction();
            $data = $request->validated();
            $data['status'] = EventStatus::GOING_TO_HAPPEN;
            $event = Event::create($data);
            $this->associateFile($event, $imageFile);
            DB::commit();
            $event->refresh();

            return new EventResource($event);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }
            DB::rollBack();
            return response()->json(['error' => $exception->getMessage().' - '. $exception->getLine().' - '. $exception->getFile()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show($id)
    {
        $event = Event::find($id);
        if (!$event) {
            return response()->json(['message' => 'ERROR.EVENT_NOT_FOUND'], Response::HTTP_NOT_FOUND);
        }
        return new EventResource($event);
    }

    public function update(UpdateEventRequest $request, Event $event)
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'event');

        try {
            DB::beginTransaction();
            $data = $request->validated();
            $event->update($data);
            $this->associateFile($event, $imageFile, $event->file);
            DB::commit();
            $event->refresh();

            return new EventResource($event);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }
            DB::rollBack();
            return response()->json(['error' => $exception->getMessage().' - '. $exception->getLine().' - '. $exception->getFile()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(Event $event)
    {
        try {
            $event->delete();
            return response()->json(['message' => 'SUCCESS.EVENT_DELETED']);
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
