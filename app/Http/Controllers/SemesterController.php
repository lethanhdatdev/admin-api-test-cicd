<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionsDefaultRequest;
use App\Http\Requests\SemesterCreateRequest;
use App\Http\Requests\SemesterListRequest;
use App\Http\Requests\SemesterUpdateRequest;
use App\Http\Resources\OptionsResource;
use App\Http\Resources\SemesterResource;
use App\Http\Resources\SemestersResource;
use App\Models\Semester;
use App\Repository\SemesterRepository;

class SemesterController extends Controller
{
    public function __construct(private SemesterRepository $repository)
    {
    }

    public function index(SemesterListRequest $request): SemestersResource
    {
        return new SemestersResource($this->repository->list($request));
    }

    public function store(SemesterCreateRequest $request): SemesterResource
    {
        return new SemesterResource(Semester::create($request->all()));
    }

    public function show(Semester $semester): SemesterResource
    {
        return new SemesterResource($semester);
    }

    public function update(SemesterUpdateRequest $request, Semester $semester): SemesterResource
    {
        $semester = $this->repository->update($semester->id, $request->all());

        return new SemesterResource($semester);
    }

    public function destroy(Semester $semester): void
    {
        $semester->deleteOrFail();
    }

    public function options(OptionsDefaultRequest $request): OptionsResource
    {
        $data = $this->repository->options($request);

        return new OptionsResource($data->toArray());
    }
}
