<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\AmazonS3FileHandler;
use App\Http\Requests\Media\StoreMediaRequest;
use Exception;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class MediaController extends Controller
{
    use AmazonS3FileHandler;

    public function store(StoreMediaRequest $request)
    {
        try {
            $imageFile = $this->amazonS3UploadFile($request->file('media'), 'media');
            return response()->json(['path' => Storage::disk('s3')->url($imageFile['path'])]);
        } catch (Exception $exception) {
            report($exception);
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
