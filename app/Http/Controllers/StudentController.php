<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentCreateRequest;
use App\Http\Requests\StudentListRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Http\Resources\StudentResource;
use App\Http\Resources\StudentsResource;
use App\Models\Ancestor;
use App\Models\Student;
use App\Models\User;
use App\Repository\StudentRepository;
use App\Services\StudentService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Enums\StudentType;

class StudentController extends Controller
{
    public function __construct(private StudentRepository $repository)
    {
    }

    public function index(StudentListRequest $request): StudentsResource
    {
        $data = $this->repository->list($request);

        return new StudentsResource($data);
    }

    public function store(StudentCreateRequest $request): StudentResource
    {
        try {
            DB::beginTransaction();

            $info = $request->all();
            $info['code'] = StudentService::generateCode($request);

            /** @var Ancestor $student */
            $student = Student::create($info);

            $userData = $request->only('name', 'email');
            $userData['password'] = Hash::make($request->get('password'));

            /** @var User $user */
            $user = User::create($userData);
            $user->userable()->associate($student);
            $user->save();

            DB::commit();

            $student->refresh();

            return new StudentResource($student);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function show(Student $student): StudentResource
    {
        return new StudentResource($student);
    }

    public function update(StudentUpdateRequest $request, Student $student): StudentResource
    {
        try {
            DB::beginTransaction();

            $info = $request->all();
            $this->repository->update($student->id, $info);

            /** @var User $user */
            $user = $student->user();
            $user->update($request->only('email', 'name'));

            $student->refresh();

            DB::commit();

            return new StudentResource($student);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function destroy(Student $student): void
    {
        try {
            DB::beginTransaction();
            $user = $student->user;
            $student->deleteOrFail();
            $user->delete();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function studentTypeOptions()
    {
        return [
            'data' => [
                [
                    'label' => 'Thường',
                    'value' => StudentType::NORMAL,
                ],
                [
                    'label' => 'VIP',
                    'value' => StudentType::VIP,
                ]
            ]
        ];

    }
}
