<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticateRequest;
use App\Http\Resources\AccessTokenResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @throws ValidationException|UnauthorizedException
     */
    public function authenticate(AuthenticateRequest $request): AccessTokenResource
    {
        /** @var User $user */
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        if (!$user->isSuperAdmin()) {
            throw new UnauthorizedException('You do not have permission.');
        }

        return new AccessTokenResource($user->createToken($request->email));
    }

    public function logout(Request $request): Response
    {
        $request->user()->currentAccessToken()->delete();

        return \response('Logout success');
    }

    public function refresh(Request $request): AccessTokenResource
    {
        $user = $request->user();
        $user->tokens()->delete();

        return new AccessTokenResource($user->createToken($user->email));
    }
}
