<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionCreateRequest;
use App\Http\Requests\QuestionImportRequest;
use App\Http\Requests\QuestionListRequest;
use App\Http\Requests\QuestionUpdateRequest;
use App\Http\Resources\QuestionDetailResource;
use App\Http\Resources\QuestionListResource;
use App\Imports\QuestionsImport;
use App\Models\Question;
use App\Repository\QuestionRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Excel;

class QuestionController extends Controller
{
    public function __construct(private QuestionRepository $repository)
    {
    }

    public function index(QuestionListRequest $request): QuestionListResource
    {
        return new QuestionListResource($this->repository->list($request));
    }

    public function store(QuestionCreateRequest $request): QuestionDetailResource
    {
        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            $question = $this->repository->create($request->only($keys));

            DB::commit();

            return new QuestionDetailResource($question);
        } catch (Exception $exception) {
            DB::rollBack();
            throw new $exception;
        }
    }

    public function show(Question $question): QuestionDetailResource
    {
        return new QuestionDetailResource($question);
    }

    public function update(QuestionUpdateRequest $request, Question $question): QuestionDetailResource
    {
        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            $question = $this->repository->update($question->id, $request->only($keys));

            DB::commit();

            return new QuestionDetailResource($question);
        } catch (Exception $exception) {
            DB::rollBack();
            throw new $exception;
        }
    }

    public function destroy(Question $question): void
    {
        try {
            DB::beginTransaction();

            $question->deleteOrFail();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function import(QuestionImportRequest $request): JsonResponse
    {
        $file = $request->file('file');

        (new QuestionsImport())->import($file, null, Excel::XLSX);

        $count = (int) Session::get(QuestionsImport::COUNT);
        Session::remove(QuestionsImport::COUNT);

        return response()->json(['message' => 'Nhập thành công ' . $count . ' câu hỏi']);
    }
}
