<?php

namespace App\Http\Controllers;

use App\Http\Requests\AncestorCreateRequest;
use App\Http\Requests\AncestorListRequest;
use App\Http\Requests\AncestorUpdateRequest;
use App\Http\Resources\AncestorResource;
use App\Http\Resources\AncestorsResource;
use App\Http\Resources\OptionResource;
use App\Http\Resources\OptionsResource;
use App\Models\Ancestor;
use App\Models\User;
use App\Repository\AncestorRepository;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AncestorController extends Controller
{
    public function __construct(private AncestorRepository $repository)
    {
    }

    public function index(AncestorListRequest $request): AncestorsResource
    {
        $data = $this->repository->list($request);

        return new AncestorsResource($data);
    }

    public function store(AncestorCreateRequest $request): AncestorResource
    {
        /** @var Ancestor $info */
        $info = Ancestor::create($request->only('phone', 'birthday', 'address', 'affiliate_code'));

        $userData = $request->only('name', 'email');
        $userData['password'] = Hash::make($request->get('password'));

        /** @var User $user */
        $user = User::create($userData);
        $user->userable()->associate($info);
        $user->save();

        return new AncestorResource($info);
    }

    public function show(Ancestor $ancestor): AncestorResource
    {
        return new AncestorResource($ancestor);
    }

    public function update(AncestorUpdateRequest $request, Ancestor $ancestor): AncestorResource
    {
        $info = $request->only('phone', 'birthday', 'address', 'affiliate_code');
        $this->repository->update($ancestor->id, $info);

        /** @var User $user */
        $user = $ancestor->user();
        $user->update($request->only('email', 'name'));

        $ancestor->refresh();

        return new AncestorResource($ancestor);
    }

    public function destroy(Ancestor $ancestor): void
    {
        try {
            DB::beginTransaction();
            $user = $ancestor->user;
            $ancestor->deleteOrFail();
            $user->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function options(): OptionsResource
    {
        /** @var Builder $queryBuilder */
        $data = DB::table('ancestors')->join('users', function (JoinClause $join) {
            $join->on('users.userable_id', '=', 'ancestors.id')
                ->where('users.userable_type', '=', Ancestor::class);
        })
            ->orderBy('users.name')
            ->select(['users.name as ' . OptionResource::LABEL_KEY, 'ancestors.id as ' . OptionResource::VALUE_KEY])
            ->orderBy('users.name')
            ->limit(50)
            ->get();

        return new OptionsResource($data->toArray());
    }
}
