<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\AmazonS3FileHandler;
use App\Http\Requests\PracticeAssignQuestionsRequest;
use App\Http\Requests\PracticeCreateRequest;
use App\Http\Requests\PracticeListRequest;
use App\Http\Requests\PracticeUpdateRequest;
use App\Http\Resources\PracticeDetailResource;
use App\Http\Resources\PracticesResource;
use App\Models\Practice;
use App\Repository\PracticeRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PracticeController extends Controller
{
    use AmazonS3FileHandler;

    public function __construct(private PracticeRepository $repository)
    {
    }

    public function index(PracticeListRequest $request): PracticesResource
    {
        return new PracticesResource($this->repository->list($request));
    }

    public function store(PracticeCreateRequest $request): PracticeDetailResource
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'practice');

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            $practice = $this->repository->create($request->only($keys));
            $this->associateFile($practice, $imageFile);

            DB::commit();

            $practice->refresh();

            return new PracticeDetailResource($practice);
        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function show(Practice $practice): PracticeDetailResource
    {
        return new PracticeDetailResource($practice);
    }

    public function update(PracticeUpdateRequest $request, Practice $practice): PracticeDetailResource
    {
        $imageFile = $this->amazonS3UploadFile($request->file('image'), 'practice');

        try {
            DB::beginTransaction();

            $keys = array_keys($request->rules());
            $keys = array_filter($keys, static function ($item) {
                return !str_contains($item, '*');
            });

            /** @var Practice $practice */
            $practice = $this->repository->update($practice->id, $request->only($keys));
            $this->associateFile($practice, $imageFile, $practice->image);

            DB::commit();

            $practice->refresh();

            return new PracticeDetailResource($practice);

        } catch (Exception $exception) {
            if ($imageFile) {
                $this->deleteFile($imageFile['path']);
            }

            DB::rollBack();

            throw $exception;
        }
    }

    public function destroy(Practice $practice): void
    {
        try {
            DB::beginTransaction();

            $practice->questions()->detach();

            if ($practice->image) {
                $this->deleteFile($practice->image->path);
                $practice->image->delete();
            }

            $practice->deleteOrFail();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function assignQuestions(PracticeAssignQuestionsRequest $request, int $id): JsonResponse
    {
        try {
            DB::beginTransaction();

            $practice = Practice::findOrFail($id);
            $questionIds = $request->get('ids');
            $results = $this->repository->assignQuestions($practice, $questionIds);

            DB::commit();

            return response()->json(
                [
                    'success' => $results,
                    'fail' => array_values(array_diff($questionIds, $results))
                ]
            );
        } catch (Exception $exception) {
            DB::rollBack();
            throw new Exception();
        }
    }
}
