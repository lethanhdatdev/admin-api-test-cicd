<?php

namespace App\Http\Controllers;

use App\Enums\Status;
use App\Http\Requests\TestScheduleCreateRequest;
use App\Http\Requests\TestScheduleListRequest;
use App\Http\Requests\TestScheduleUpdateRequest;
use App\Http\Resources\TestScheduleResource;
use App\Http\Resources\TestSchedulesResource;
use App\Models\TestSchedule;
use App\Repository\TestScheduleRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use JsonException;

class TestScheduleController extends Controller
{
    public function __construct(private readonly TestScheduleRepository $repository)
    {
    }

    /**
     * @throws JsonException
     */
    public function index(TestScheduleListRequest $request): TestSchedulesResource
    {
        return new TestSchedulesResource($this->repository->list($request));
    }

    public function show(TestSchedule $testSchedule): TestScheduleResource
    {
        return new TestScheduleResource($testSchedule);
    }

    public function store(TestScheduleCreateRequest $request): TestScheduleResource
    {
        return new TestScheduleResource($this->repository->create($request->all()));
    }

    public function update(TestScheduleUpdateRequest $request, TestSchedule $testSchedule): TestScheduleResource
    {
        try {
            DB::beginTransaction();
            $keys = array_keys($request->rules());
            $testSchedule = $this->repository->update($testSchedule->id, $request->only($keys));
            DB::commit();

            return new TestScheduleResource($testSchedule);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @throws Exception
     */
    public function lock(int $id): JsonResponse
    {
        try {
            DB::beginTransaction();
            /** @var TestSchedule|null $model */
            $model = $this->repository->find($id);
            if (null === $model) {
                throw new Exception('Không tìm thấy lịch thi');
            }
            $model->status = Status::LOCK->value;
            $model->save();
            DB::commit();

            return response()->json([]);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @throws Exception
     */
    public function active(int $id): JsonResponse
    {
        try {
            DB::beginTransaction();
            /** @var TestSchedule|null $model */
            $model = $this->repository->find($id);
            if (null === $model) {
                throw new Exception('Không tìm thấy lịch thi');
            }
            $model->status = Status::ACTIVE->value;
            $model->save();
            DB::commit();

            return response()->json([]);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function destroy(TestSchedule $testSchedule): void
    {
        try {
            DB::beginTransaction();
            $testSchedule->deleteOrFail();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
