<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionsDefaultRequest;
use App\Http\Requests\ThematicCreateRequest;
use App\Http\Requests\ThematicListRequest;
use App\Http\Requests\ThematicUpdateRequest;
use App\Http\Resources\OptionsResource;
use App\Http\Resources\ThematicResource;
use App\Http\Resources\ThematicsResource;
use App\Models\Thematic;
use App\Repository\ThematicRepository;

class ThematicController extends Controller
{
    public function __construct(private ThematicRepository $repository)
    {
    }

    public function index(ThematicListRequest $request): ThematicsResource
    {
        return new ThematicsResource($this->repository->list($request));
    }

    public function store(ThematicCreateRequest $request): ThematicResource
    {
        return new ThematicResource($this->repository->create($request->all()));
    }

    public function show(Thematic $thematic): ThematicResource
    {
        return new ThematicResource($thematic);
    }

    public function update(ThematicUpdateRequest $request, Thematic $thematic): ThematicResource
    {
        $this->repository->update($thematic->id, $request->all());

        return new ThematicResource($thematic);
    }

    public function destroy(Thematic $thematic): void
    {
        $thematic->deleteOrFail();
    }

    public function options(OptionsDefaultRequest $request): OptionsResource
    {
        $data = $this->repository->options($request);

        return new OptionsResource($data->toArray());
    }
}
