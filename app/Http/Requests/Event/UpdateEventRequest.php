<?php

namespace App\Http\Requests\Event;

use App\Enums\EventStatus;
use App\Http\Requests\AbstractApiRequest;

/**
 * @property mixed $direction
 * @property mixed $sort
 */
class UpdateEventRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'unit_id' => ['required', 'int'],
            'subject_id' => ['required', 'int'],
            'test_id' => ['required', 'int'],
            'name' => ['required', 'string'],
            'event_time' => ['required'],
            'short_description' => ['nullable', 'string'],
            'content' => ['nullable', 'string'],
            'image' => ['nullable', 'mimes:jpg,jpeg,png'],
            'status' => ['nullable', 'in:' . implode(',', EventStatus::values())],
        ];
    }
}
