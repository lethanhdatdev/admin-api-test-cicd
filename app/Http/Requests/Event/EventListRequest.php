<?php

namespace App\Http\Requests\Event;

use App\Enums\EventStatus;
use App\Http\Requests\AbstractApiRequest;

/**
 * @property mixed $direction
 * @property mixed $sort
 */
class EventListRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'page' => ['required', 'int'],
            'size' => ['required', 'int'],
            'sort' => ['nullable', 'string'],
            'direction' => ['nullable', 'string', 'in:asc,desc'],
            'subject_id' => ['nullable', 'int'],
            'unit_id' => ['nullable', 'int'],
            'status' => ['nullable', 'string', 'in:' . implode(',', EventStatus::values())],
            'search' => ['nullable', 'string', 'min:3', 'max:50'],
        ];
    }

    protected function prepareForValidation()
    {
        $sort = !$this->sort ? 'id' : $this->sort;
        $direction = !$this->direction ? 'asc' : $this->direction;
        $this->merge([
            'sort' => $sort,
            'direction' => $direction
        ]);
    }
}
