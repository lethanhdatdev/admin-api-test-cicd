<?php

namespace App\Http\Requests\Event;

use App\Http\Requests\AbstractApiRequest;

/**
 * @property mixed $direction
 * @property mixed $sort
 */
class StoreEventRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'unit_id' => ['required', 'int'],
            'subject_id' => ['required', 'int'],
            'test_id' => ['required', 'int'],
            'name' => ['required', 'string'],
            'event_time' => ['required'],
            'short_description' => ['nullable', 'string'],
            'content' => ['nullable', 'string'],
            'image' => ['nullable', 'mimes:jpg,jpeg,png'],
        ];
    }
}
