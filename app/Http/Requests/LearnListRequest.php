<?php

namespace App\Http\Requests;

use App\Enums\Status;
use App\Services\LearnService;

/**
 * @property mixed $direction
 * @property mixed $sort
 * @property mixed $created_to
 * @property mixed $created_from
 */
class LearnListRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'page' => ['required', 'int'],
            'size' => ['required', 'int'],
            'sort' => ['nullable', 'string'],
            'direction' => ['nullable', 'string', 'in:asc,desc'],
            'subject_id' => ['nullable', 'int'],
            'unit_id' => ['nullable', 'int'],
            'sorting' => ['string', 'nullable'],
            'year' => ['nullable', 'string', 'min:9', 'max:9'],
            'type' => ['nullable', 'string', 'in:' . LearnService::TYPE_SEMESTER . ',' . LearnService::TYPE_THEMATIC],
            'status' => ['nullable', 'string', 'in:' . implode(',', Status::values())],
            'search' => ['nullable', 'string', 'min:3', 'max:50'],
            'created_from' => ['nullable', 'date', function ($attribute, $value, $fails) {
                if ($value && $this->created_to && $value > $this->created_to) {
                    $fails('THE_CREATED_FROM_MUST_BE_A_DATE_BEFORE_OR_EQUAL_CREATED_TO');
                }
            }],
            'created_to' => ['nullable', 'date', function ($attribute, $value, $fails) {
                if ($value && $this->created_from && $value < $this->created_from) {
                    $fails('THE_CREATED_TO_MUST_BE_A_DATE_AFTER_OR_EQUAL_CREATED_FROM');
                }
            }],
        ];
    }

    protected function prepareForValidation()
    {
        $sort = !$this->sort ? 'created_at' : $this->sort;
        $direction = !$this->direction ? 'desc' : $this->direction;
        $this->merge([
            'sort' => $sort,
            'direction' => $direction
        ]);
    }
}
