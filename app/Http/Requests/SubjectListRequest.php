<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\SubjectRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class SubjectListRequest extends DefaultListRequest
{
    protected function getRepository(): SubjectRepository
    {
        return App::make(SubjectRepository::class);
    }
}
