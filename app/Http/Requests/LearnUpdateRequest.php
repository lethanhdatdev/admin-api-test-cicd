<?php

namespace App\Http\Requests;

use App\Enums\Status;
use App\Models\Learn;
use App\Services\LearnService;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property Learn $learn
 */
class LearnUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:10', 'max:255'],
            'code' => ['required', 'string', 'min:5', 'max:50', 'unique:learns,code,' . $this->learn->id . ',id'],
            'description' => ['string', 'nullable'],
            'type' => ['required', 'string', 'in:' . LearnService::TYPE_SEMESTER . ',' . LearnService::TYPE_THEMATIC],
            'image' => ['nullable', 'image'],
            'price' => ['required', 'int', 'min:1'],
            'year' => ['nullable', 'string', 'min:9', 'max:9'],
            'status' => ['nullable', 'string', 'in:' . implode(',', Status::values())],
            'thematic_id' => ['int', 'exists:thematics,id'],
            'semester_id' => ['int', 'exists:semesters,id']
        ];
    }
}
