<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\QuestionCreateTrait;
use Illuminate\Foundation\Http\FormRequest;

class QuestionCreateRequest extends FormRequest
{
    use QuestionCreateTrait;

    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules()
    {
        return $this->questionRules(false, true);
    }
}
