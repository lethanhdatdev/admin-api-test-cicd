<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\TopicCreateTrait;
use App\Services\KnowledgeService;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $sync_video_id
 */
class KnowledgeCreateRequest extends FormRequest
{
    use TopicCreateTrait;

    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules()
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'intro' => ['string', 'nullable'],
            'description' => ['string', 'nullable'],
            'image' => ['nullable', 'image'],
            'video' => ['nullable', 'file'],
            'sync_video_id' => ['nullable', 'numeric'],
            'duration' => ['nullable', 'numeric'],
            'year' => ['nullable', 'string', 'min:9', 'max:9'],
            'type' => ['required', 'string', 'in:' . KnowledgeService::TYPE_NORMAL . ',' . KnowledgeService::TYPE_ADVANCED],
            'thematic_id' => ['required', 'int', 'exists:thematics,id'],
            'topics' => ['nullable', 'array'],
            'learn_type' => ['string', 'in:SEMESTER,THEMATIC'],
        ];

        $rules = array_merge(
            $rules,
            $this->topicRules(true, true, 'topics.*.question.'),
            [
                'topics.*.topic_start' => ['nullable', 'int'],
                'topics.*.question_start' => ['nullable', 'int'],
                'topics.*.question_end' => ['nullable', 'int']
            ]
        );

        return $rules;
    }
}
