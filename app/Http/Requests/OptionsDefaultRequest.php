<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OptionsDefaultRequest extends FormRequest implements IOptionsRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'filters' => ['string', 'nullable']
        ];
    }
}
