<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\QuestionCreateTrait;
use App\Services\PracticeService;
use Illuminate\Foundation\Http\FormRequest;

class PracticeCreateRequest extends FormRequest
{
    use QuestionCreateTrait;

    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules()
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'code' => ['required', 'string', 'max:100', 'unique:practices'],
            'image' => ['nullable', 'image'],
            'subject_id' => ['required', 'int', 'exists:subjects,id'],
            'semester_id' => ['required', 'int', 'exists:semesters,id'],
            'year' => ['nullable', 'string', 'min:9', 'max:9'],
            'practice_category_id' => ['required', 'int', 'exists:practice_categories,id'],
            'difficulty' => ['required', 'string', 'in:' . PracticeService::TYPE_DIFFICULT_NORMAL . ',' . PracticeService::TYPE_DIFFICULT_ADVANCED],
            'questions' => ['nullable', 'array'],
            'student_type' => ['in:NORMAL,VIP'],
        ];

        return array_merge($rules, $this->questionRules(true, true, 'questions.*.'));
    }
}
