<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $phone
 * @property string $code
 */
class SmsVerifyCheckRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->guest();
    }

    public function rules(): array
    {
        return [
            'phone' => ['required', 'string', 'min:10'],
            'code' => ['required', 'string', 'min:6', 'max:6']
        ];
    }
}
