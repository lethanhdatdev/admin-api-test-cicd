<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidatesWhenResolved;

/**
 * @property int $page
 * @property int $size
 * @property null|string $filters
 * @property null|string $globalFilter
 * @property null|string $sorting
 */
interface IListRequest extends ValidatesWhenResolved
{
}
