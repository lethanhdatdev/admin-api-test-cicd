<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\TestScheduleRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class TestScheduleListRequest extends DefaultListRequest
{
    protected function getRepository(): TestScheduleRepository
    {
        return App::make(TestScheduleRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'unit_id' => ['nullable', 'int'],
            'subject_id' => ['nullable', 'int'],
            'semester_id' => ['nullable', 'int'],
            'thematic_id' => ['nullable', 'int'],
            'year' => ['nullable', 'string', 'min:9', 'max:9']
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages(): array
    {
        $messages = [
            'unit_id' => 'unit_id phải là số.',
            'subject_id' => 'subject_id phải là số.',
            'semester_id' => 'semester_id phải là số.',
            'thematic_id' => 'thematic_id phải là số.',
            'year' => 'Định dạng year sai',
        ];

        return array_merge(parent::messages(), $messages);
    }
}
