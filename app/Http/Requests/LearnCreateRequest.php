<?php

namespace App\Http\Requests;

use App\Enums\Status;
use App\Services\LearnService;

class LearnCreateRequest extends AbstractApiRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:10', 'max:255'],
            'code' => ['required', 'string', 'min:5', 'max:50', 'unique:learns'],
            'description' => ['string', 'nullable'],
            'type' => ['required', 'string', 'in:' . LearnService::TYPE_SEMESTER . ',' . LearnService::TYPE_THEMATIC],
            'price' => ['required', 'int', 'min:1'],
            'image' => ['nullable', 'image'],
            'year' => ['nullable', 'string', 'min:9', 'max:9'],
            'status' => ['nullable', 'string', 'in:' . implode(',', Status::values())],
            'thematic_id' => ['int', 'exists:thematics,id'],
            'semester_id' => ['int', 'exists:semesters,id'],
            'unit_id' => ['int', 'exists:units,id'],
            'subject_id' => ['int', 'exists:subjects,id'],
        ];
    }
}
