<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\AnswerCreateTrait;
use Illuminate\Foundation\Http\FormRequest;

class AnswerUpdateRequest extends FormRequest
{
    use AnswerCreateTrait;

    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules()
    {
        return $this->answerRules(false);
    }
}
