<?php

namespace App\Http\Requests;

use App\Models\Ancestor;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property Ancestor $ancestor
 */
class AncestorUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $this->ancestor->user->id . ',id'],
            'phone' => ['required', 'string', 'min:10', 'max:11', 'unique:ancestors,phone,' . $this->ancestor->id . ',id', 'unique:students'],
            'birthday' => ['required', 'string'],
            'affiliate_code' => ['string', 'nullable'],
            'address' => ['string', 'nullable'],
        ];
    }
}
