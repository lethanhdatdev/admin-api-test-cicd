<?php

namespace App\Http\Requests;

use App\Enums\OrderLearnPackageType;
use App\Enums\OrderStatus;
use Illuminate\Validation\Rules\Enum;

/**
 * @property mixed $sort
 * @property mixed $direction
 * @property mixed $created_from
 * @property mixed $created_to
 * @property mixed $page
 */
class OrderListRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'page' => ['required', 'int'],
            'size' => ['required', 'int'],
            'sort' => ['nullable', 'string'],
            'direction' => ['nullable', 'string', 'in:asc,desc'],
            'search' => ['nullable', 'string', 'min:3'],
            'created_from' => ['nullable', 'date', function ($attribute, $value, $fails) {
                if ($value && $this->created_to && $value > $this->created_to) {
                    $fails('THE_CREATED_FROM_MUST_BE_A_DATE_BEFORE_OR_EQUAL_CREATED_TO');
                }
            }],
            'created_to' => ['nullable', 'date', function ($attribute, $value, $fails) {
                if ($value && $this->created_from && $value < $this->created_from) {
                    $fails('THE_CREATED_TO_MUST_BE_A_DATE_AFTER_OR_EQUAL_CREATED_FROM');
                }
            }],
            'type' => ['nullable', new Enum(OrderLearnPackageType::class)],
            'status' => ['nullable', new Enum(OrderStatus::class)]
        ];
    }

    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $sort = !$this->sort ? 'created_at' : $this->sort;
        $direction = !$this->direction ? 'desc' : $this->direction;
        $this->merge([
            'sort' => $sort,
            'direction' => $direction,
            'page' => $this->page + 1,
        ]);
    }
}
