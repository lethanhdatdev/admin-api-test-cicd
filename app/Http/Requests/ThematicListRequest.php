<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\ThematicRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class ThematicListRequest extends DefaultListRequest
{
    protected function getRepository(): ThematicRepository
    {
        return App::make(ThematicRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'subject_id' => ['nullable', 'int'],
            'semester_id' => ['nullable', 'int']
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'subject_id' => 'subject_id phải là số.',
            'semester_id' => 'semester_id phải là số.'
        ];

        return array_merge(parent::messages(), $messages);
    }
}
