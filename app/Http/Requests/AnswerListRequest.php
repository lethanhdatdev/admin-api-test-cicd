<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\AnswerRepository;
use App\Services\QuestionService;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class AnswerListRequest extends DefaultListRequest
{
    protected function getRepository(): AnswerRepository
    {
        return App::make(AnswerRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'question_id' => ['nullable', 'int'],
            'correct' => ['nullable', 'boolean'],
            'type' => ['nullable', 'string', 'in:' . QuestionService::TYPE_SORT . ',' . QuestionService::TYPE_CHOICE . ',' . QuestionService::TYPE_FILL_IN_BLANK]
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'question_id' => 'question_id phải là số.',
            'correct' => 'correct phải là 1 hoặc 0',
            'type' => 'type phải là một trong các dạng sau: ' . QuestionService::TYPE_SORT . ',' . QuestionService::TYPE_CHOICE . ',' . QuestionService::TYPE_FILL_IN_BLANK,
        ];

        return array_merge(parent::messages(), $messages);
    }
}
