<?php

namespace App\Http\Requests;

use App\Models\Student;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property Student $student
 */
class StudentUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $this->student->user->id . ',id'],
            'phone' => ['required', 'string', 'min:10', 'max:11', 'unique:students,phone,' . $this->student->id . ',id', 'unique:ancestors'],
            'birthday' => ['required', 'string'],
            'address' => ['string', 'nullable'],
            'unit_id' => ['required', 'int', 'exists:units,id'],
            'lock' => ['boolean', 'nullable'],
            'point' => ['int', 'nullable']
        ];
    }
}
