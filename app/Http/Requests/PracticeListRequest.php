<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\PracticeRepository;
use App\Services\PracticeService;
use App\Services\QuestionService;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class PracticeListRequest extends DefaultListRequest
{
    protected function getRepository(): PracticeRepository
    {
        return App::make(PracticeRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'practice_category_id' => ['nullable', 'int'],
            'subject_id' => ['nullable', 'int'],
            'semester_id' => ['nullable', 'int'],
            'difficulty' => ['nullable', 'string', 'in:' . PracticeService::TYPE_DIFFICULT_NORMAL . ',' . PracticeService::TYPE_DIFFICULT_ADVANCED],
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'practice_category_id' => 'practice_category_id phải là số.',
            'subject_id' => 'subject_id phải là số.',
            'semester_id' => 'semester_id phải là số.',
            'difficulty' => 'difficulty phải là một trong các dạng sau: ' . PracticeService::TYPE_DIFFICULT_NORMAL . ',' . PracticeService::TYPE_DIFFICULT_ADVANCED,
        ];

        return array_merge(parent::messages(), $messages);
    }
}
