<?php

namespace App\Http\Requests;

use App\Enums\TestDifficulty;
use App\Enums\TestType;
use App\Repository\AbstractBaseRepository;
use App\Repository\TestRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class TestListRequest extends DefaultListRequest
{
    protected function getRepository(): TestRepository
    {
        return App::make(TestRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'subject_id' => ['nullable', 'int'],
            'semester_id' => ['nullable', 'int'],
            'type' => ['nullable', 'string', 'in:' . implode(',', TestType::values())],
            'difficulty' => ['nullable', 'string', 'in:' . implode(',', TestDifficulty::values())],
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'subject_id' => 'subject_id phải là số.',
            'semester_id' => 'semester_id phải là số.',
            'type' => 'type phải là một trong các dạng sau: ' . implode(',', TestType::values()),
            'difficulty' => 'difficulty phải là một trong các dạng sau: ' . implode(',', TestDifficulty::values()),
        ];

        return array_merge(parent::messages(), $messages);
    }
}
