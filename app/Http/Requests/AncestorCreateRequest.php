<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AncestorCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'min:10', 'max:11', 'unique:ancestors', 'unique:students'],
            'birthday' => ['required', 'string'],
            'affiliate_code' => ['string', 'nullable'],
            'address' => ['string', 'nullable'],
        ];
    }
}
