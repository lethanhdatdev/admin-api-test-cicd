<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\SemesterRepository;
use App\Repository\ThematicRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class SemesterListRequest extends DefaultListRequest
{
    protected function getRepository(): SemesterRepository
    {
        return App::make(SemesterRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'unit_id' => ['nullable', 'int'],
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'unit_id' => 'unit_id phải là số.',
        ];

        return array_merge(parent::messages(), $messages);
    }
}
