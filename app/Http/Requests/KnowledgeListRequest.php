<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\KnowledgeRepository;
use App\Services\KnowledgeService;
use App\Services\QuestionService;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class KnowledgeListRequest extends DefaultListRequest
{
    protected function getRepository(): KnowledgeRepository
    {
        return App::make(KnowledgeRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'thematic_id' => ['nullable', 'int'],
            'type' => ['nullable', 'string', 'in:' . KnowledgeService::TYPE_ADVANCED . ',' . KnowledgeService::TYPE_NORMAL],
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'thematic_id' => 'thematic_id phải là số.',
            'type' => 'type phải là một trong các dạng sau: ' . KnowledgeService::TYPE_NORMAL . ',' . KnowledgeService::TYPE_ADVANCED,
        ];

        return array_merge(parent::messages(), $messages);
    }
}
