<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ThematicCreateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['string', 'nullable'],
            'subject_id' => ['required', 'int', 'exists:subjects,id'],
            'semester_id' => ['required', 'int', 'exists:semesters,id']
        ];
    }
}
