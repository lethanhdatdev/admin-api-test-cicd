<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property AbstractBaseRepository $repository
 */
abstract class DefaultListRequest extends FormRequest implements IListRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        $repository = $this->getRepository();

        return [
            'page' => ['required', 'int', 'max:255'],
            'size' => ['required', 'int'],
            AbstractBaseRepository::SORT_PARAM => ['nullable', 'string', 'in:' . implode(',', $repository->getSortable())],
            AbstractBaseRepository::SORT_DIRECTION_PARAM => ['nullable', 'string', 'in:' . implode(',', $repository->getDirectionable())],
            AbstractBaseRepository::SEARCH_PARAM => ['nullable', 'string', 'min:3']
        ];
    }

    abstract protected function getRepository(): AbstractBaseRepository;
}
