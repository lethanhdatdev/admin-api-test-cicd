<?php

namespace App\Http\Requests;

use App\Repository\PracticeCategoryRepository;
use Illuminate\Support\Facades\App;

class PracticeCategoryListRequest extends DefaultListRequest
{
    protected function getRepository(): PracticeCategoryRepository
    {
        return App::make(PracticeCategoryRepository::class);
    }

    public function rules(): array
    {
        return [
            'page' => ['required', 'int', 'max:255'],
            'size' => ['required', 'int'],
            'search' => ['string', 'nullable']
        ];
    }
}
