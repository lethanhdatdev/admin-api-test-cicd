<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;

class AbstractApiRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     *
     * @param Validator $validator
     *
     */
    protected function failedValidation(Validator $validator)
    {
        $messages = Arr::flatten($validator->errors()->messages());
        $errors = array_map(function ($item) {
            $item = str_replace(' ', '_', $item);
            $item = str_replace('.', '', $item);
            return 'ERROR.VALIDATE.' . strtoupper(str_replace(' ', '_', $item));
        }, $messages);
        throw new HttpResponseException(
            response()->json(['error' => $errors], Response::HTTP_BAD_REQUEST)
        );
    }

    protected function prepareForValidation()
    {
        $this->merge($this->route()->parameters());
    }

    protected function failedAuthorization()
    {
        throw new HttpResponseException(response()->json(
            [
                'error' => 'ERROR.UNAUTHORIZED'
            ],
            Response::HTTP_UNAUTHORIZED
        ));
    }
}
