<?php

namespace App\Http\Requests;

use App\Enums\TestDifficulty;
use App\Enums\TestType;
use App\Http\Requests\Traits\QuestionCreateTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestCreateRequest extends FormRequest
{
    use QuestionCreateTrait;

    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'code' => ['required', 'string', 'max:100', 'unique:tests'],
            'subject_id' => ['required', 'int', 'exists:subjects,id'],
            'semester_id' => ['required', 'int', 'exists:semesters,id'],
            'image' => ['nullable', 'image'],
            'year' => ['nullable', 'string', 'min:9', 'max:9'],
            'type' => ['required', 'string', 'in:' . implode(',', TestType::values())],
            'duration' => ['required', 'int'],
            'difficulty' => ['required', 'string', 'in:' . implode(',', TestDifficulty::values())],
            'questions' => ['nullable', 'array'],
            'student_type' => ['in:NORMAL,VIP'],
        ];

        return array_merge($rules, $this->questionRules(true, true, 'questions.*.'));
    }
}
