<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $sort
 * @property mixed $direction
 */
class AmazonS3VideoListRequest extends FormRequest implements IListRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'page' => ['required', 'int', 'max:255'],
            'size' => ['required', 'int'],
            'search' => ['nullable', 'string', 'min:3'],
            'sort' => ['nullable', 'string'],
            'direction' => ['nullable', 'string', 'in:asc,desc'],
        ];
    }

    protected function prepareForValidation()
    {
        $sort = !$this->sort ? 'created_at' : $this->sort;
        $direction = !$this->direction ? 'desc' : $this->direction;
        $this->merge([
            'sort' => $sort,
            'direction' => $direction
        ]);
    }
}
