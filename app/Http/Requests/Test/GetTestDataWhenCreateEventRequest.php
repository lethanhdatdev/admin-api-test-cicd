<?php

namespace App\Http\Requests\Test;

use App\Http\Requests\AbstractApiRequest;

/**
 * @property mixed $unit_id
 * @property mixed $subject_id
 */
class GetTestDataWhenCreateEventRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'subject_id' => ['required', 'int'],
            'unit_id' => ['required', 'int'],
        ];
    }
}
