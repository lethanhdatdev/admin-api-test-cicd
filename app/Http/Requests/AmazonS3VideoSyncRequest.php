<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmazonS3VideoSyncRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'file' => ['required', 'string', 'max:255'],
            'thumb' => ['required', 'string', 'max:255'],
            'extra' => ['required', 'json'],
            'duration' => ['numeric', 'nullable']
        ];
    }
}
