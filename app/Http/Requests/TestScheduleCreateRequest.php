<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\QuestionCreateTrait;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class TestScheduleCreateRequest extends FormRequest
{
    use QuestionCreateTrait;

    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255', 'min:4'],
            'description' => ['nullable', 'string'],
            'unit_id' => ['required', 'int', 'exists:units,id'],
            'subject_id' => ['required', 'int', 'exists:subjects,id'],
            'semester_id' => ['required', 'int', 'exists:semesters,id'],
            'test_id' => ['required', 'int', 'exists:tests,id'],
            'year' => ['required', 'string', 'min:9', 'max:9'],
            'start_time' => ['required', 'date', function ($attribute, $value, $fails) {
                if ($value <= Carbon::now()) {
                    $fails('THE_START_DATE_MUST_BE_A_DATE_IN_FUTURE');
                }
            }],
        ];
    }
}
