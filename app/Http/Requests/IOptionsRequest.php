<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidatesWhenResolved;

/**
 * @property null|string $filters
 */
interface IOptionsRequest extends ValidatesWhenResolved
{
}
