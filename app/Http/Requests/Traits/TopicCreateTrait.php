<?php

namespace App\Http\Requests\Traits;

use App\Services\QuestionService;

trait TopicCreateTrait
{
    use QuestionCreateTrait;

    public function topicRules(bool $isSub = false, bool $includeQuestion = false, string|null $prefix = ''): array
    {
        $rules = [
            ($isSub ? 'topics.*.' : '') . 'name' => ['required', 'string', 'max:255'],
            ($isSub ? 'topics.*.' : '') . 'description' => ['required', 'string']
        ];

        if ($includeQuestion) {
            $key = ($isSub ? 'topics.*.' : '') . 'question';
            $rules[$key] = ['nullable', 'array'];
            $rules = array_merge($rules, $this->questionRules(false, true, $prefix));
        }

        return $rules;
    }
}
