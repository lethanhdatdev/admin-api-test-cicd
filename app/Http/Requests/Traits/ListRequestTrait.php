<?php

namespace App\Http\Requests\Traits;

trait ListRequestTrait
{
    public function rules(): array
    {
        return [
            'page' => ['required', 'int', 'max:255'],
            'size' => ['required', 'int'],
            'filters' => ['string', 'nullable'],
            'globalFilter' => ['string', 'nullable'],
            'sorting' => ['string', 'nullable']
        ];
    }
}
