<?php

namespace App\Http\Requests\Traits;

use App\Services\QuestionService;

trait QuestionCreateTrait
{
    use AnswerCreateTrait;

    public function questionRules(bool $isSub = false, bool $includeAnswers = false, string|null $prefix = ''): array
    {
        $rules = [
            ($isSub ? 'questions.*.' : '') . ($prefix ?: '') . 'description' => ['required', 'string', 'max:255'],
            ($isSub ? 'questions.*.' : '') . ($prefix ?: '') . 'type' => ['required', 'string', 'in:' . QuestionService::TYPE_SORT . ',' . QuestionService::TYPE_CHOICE . ',' . QuestionService::TYPE_FILL_IN_BLANK],
            ($isSub ? 'questions.*.' : '') . ($prefix ?: '') . 'thematic_id' => ['required', 'int', 'exists:thematics,id'],
            ($isSub ? 'questions.*.' : '') . ($prefix ?: '') . 'suggestion' => ['required', 'string'],
            ($isSub ? 'questions.*.' : '') . ($prefix ?: '') . 'sort_correct' => ['nullable', 'string']
        ];

        if ($includeAnswers) {
            $key = ($isSub ? 'questions.*.' : '') . 'answers';
            $rules[$key] = ['nullable', 'array'];
            $rules = array_merge($rules, $this->answerRules(false, true, $prefix));
        }

        return $rules;
    }
}
