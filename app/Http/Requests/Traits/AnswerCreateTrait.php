<?php

namespace App\Http\Requests\Traits;

use App\Services\QuestionService;

trait AnswerCreateTrait
{
    public function answerRules(bool $hasQuestionId = true, bool $isSub = false, string|null $prefix = null): array
    {
        $rules = [
            ($prefix ?: '') . ($isSub ? 'answers.*.' : '') . 'description' =>
                ['required', 'string', 'max:255'],
            ($prefix ?: '') . ($isSub ? 'answers.*.' : '') . 'type' =>
                ['required', 'string', 'in:' . QuestionService::TYPE_SORT . ',' . QuestionService::TYPE_CHOICE . ',' . QuestionService::TYPE_FILL_IN_BLANK],
            ($prefix ?: '') . ($isSub ? 'answers.*.' : '') . 'correct' =>
                ['required', 'bool']
        ];

        if ($hasQuestionId) {
            $key = ($prefix ?: '') . ($isSub ? 'answers.*.' : '') . 'question_id';
            $rules[$key] = ['required', 'int', 'exists:questions,id'];
        }

        return $rules;
    }
}
