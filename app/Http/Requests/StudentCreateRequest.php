<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'min:10', 'max:11', 'unique:ancestors', 'unique:students'],
            'birthday' => ['required', 'string'],
            'address' => ['string', 'nullable'],
            'ancestor_id' => ['required', 'int', 'exists:ancestors,id'],
            'unit_id' => ['required', 'int', 'exists:units,id'],
            'lock' => ['boolean', 'nullable'],
            'point' => ['int', 'nullable']
        ];
    }
}
