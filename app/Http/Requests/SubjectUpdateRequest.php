<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubjectUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->isSuperAdmin();
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255']
        ];
    }
}
