<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\UnitRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class UnitListRequest extends DefaultListRequest
{
    protected function getRepository(): UnitRepository
    {
        return App::make(UnitRepository::class);
    }
}
