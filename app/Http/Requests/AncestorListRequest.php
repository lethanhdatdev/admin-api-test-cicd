<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\AncestorRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class AncestorListRequest extends DefaultListRequest
{
    protected function getRepository(): AncestorRepository
    {
        return App::make(AncestorRepository::class);
    }
}
