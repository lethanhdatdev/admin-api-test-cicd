<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\QuestionRepository;
use App\Services\QuestionService;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class QuestionListRequest extends DefaultListRequest
{
    protected function getRepository(): QuestionRepository
    {
        return App::make(QuestionRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'thematic_id' => ['nullable', 'int'],
            'type' => ['nullable', 'string', 'in:' . QuestionService::TYPE_SORT . ',' . QuestionService::TYPE_CHOICE . ',' . QuestionService::TYPE_FILL_IN_BLANK]
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'thematic_id' => 'thematic_id phải là số.',
            'type' => 'type phải là một trong các dạng sau: ' . QuestionService::TYPE_SORT . ',' . QuestionService::TYPE_CHOICE . ',' . QuestionService::TYPE_FILL_IN_BLANK,
        ];

        return array_merge(parent::messages(), $messages);
    }
}
