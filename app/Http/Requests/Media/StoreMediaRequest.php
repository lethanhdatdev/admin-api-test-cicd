<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\AbstractApiRequest;

class StoreMediaRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'media' => 'required|mimes:jpg,jpeg,png,mp4'
        ];
    }
}
