<?php

namespace App\Http\Requests;

use App\Repository\AbstractBaseRepository;
use App\Repository\StudentRepository;
use Illuminate\Support\Facades\App;

/**
 * @property AbstractBaseRepository $repository
 */
class StudentListRequest extends DefaultListRequest
{
    protected function getRepository(): StudentRepository
    {
        return App::make(StudentRepository::class);
    }

    public function rules(): array
    {
        $rules = [
            'ancestor_id' => ['nullable', 'int'],
            'unit_id' => ['nullable', 'int'],
            'lock' => ['nullable', 'boolean'],
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function messages()
    {
        $messages = [
            'ancestor_id' => 'ancestor_id phải là số.',
            'unit_id' => 'unit_id phải là số.',
            'lock' => "Lock phải là 1 hoặc 0"
        ];

        return array_merge(parent::messages(), $messages);
    }
}
