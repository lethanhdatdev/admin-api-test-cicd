<?php

namespace App\Http\Requests;

class UpdateOrderRequest extends AbstractApiRequest
{
    public function rules(): array
    {
        return [
            'status' => 'required|in:SUCCESS,CANCELLED',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
