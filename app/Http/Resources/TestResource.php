<?php

namespace App\Http\Resources;

use App\Models\File;
use App\Models\Question;
use App\Models\Semester;
use App\Models\Subject;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $subject_id
 * @property int $semester_id
 * @property string $type
 * @property int $duration
 * @property string $difficulty
 * @property string $year
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection<Question> $questions
 * @property File $image
 *
 * @method BelongsTo subject()
 * @method BelongsTo semester()
 * @method BelongsToMany questions()
 *
 * @property Test $resource
 */
class TestResource extends JsonResource
{
    public function toArray($request): array
    {
        $thematics = new Collection();
        foreach ($this->questions as $question) {
            $thematics->add($question->thematic);
        }
        $thematics = $thematics->groupBy('id')
            ->sortBy(static function ($item) {
                return $item->count();
            }, SORT_REGULAR, true)
            ->map(static function ($item) {
                return ['id' => $item->first()->id, 'name' => $item->first()->name];
            });

        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'type' => $this->type,
            'difficulty' => $this->difficulty,
            'duration' => $this->duration,
            'image' => $this->image ? Storage::url($this->image->path) : null,
            'subject' => $this->subject()->get(['id', 'name'])->first(),
            'semester' => $this->semester()->get(['id', 'name'])->first(),
            'unit' => $this->semester->unit->only(['id', 'name']),
            'year' => $this->year,
            'status' => $this->resource->status,
            'questions_count' => $this->questions()->count(),
            'thematic' => $thematics->first(),
            'student_type' => $this->student_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
