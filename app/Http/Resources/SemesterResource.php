<?php

namespace App\Http\Resources;

use App\Models\Unit;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class SemesterResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'unit' => $this->getUnit(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    protected function getUnit(): null|Unit
    {
        try {
            return $this->unit()->get(['id', 'name'])->first();
        } catch (\Exception $e) {
            return null;
        }
    }
}
