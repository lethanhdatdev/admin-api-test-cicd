<?php

namespace App\Http\Resources;

use App\Models\File;
use App\Models\Practice;
use App\Models\PracticeCategory;
use App\Models\Semester;
use App\Models\Subject;
use App\Services\PracticeService;
use App\Services\QuestionService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $difficulty
 * @property string $practice_category_id
 * @property int $subject_id
 * @property int $semester_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection $questions
 * @property File $image
 * @property PracticeCategory $category
 *
 * @method BelongsTo category()
 * @method BelongsTo subject()
 * @method BelongsTo semester()
 * @method BelongsToMany questions()
 *
 * @property Practice $resource
 */
class PracticeDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        $questions = [];
        foreach ($this->questions as $question) {
            $answers = [];
            foreach ($question->answers as $answer) {
                $answers[] = [
                    'id' => $answer->id,
                    'type' => $answer->type,
                    'description' => $answer->description,
                    'correct' => $answer->correct
                ];
            }

            $questions[] = [
                'id' => $question->id,
                'type' => $question->type,
                'description' => QuestionService::replaceLaTex($question->description),
                'thematic' => $question->thematic()->get(['id', 'name'])->first(),
                'suggestion' => $question->suggestion,
                'sort_correct' => $question->sort_correct,
                'answers' => $answers
            ];
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'difficulty' => $this->difficulty,
            'category' => $this->category()->get(['id', 'name'])->first(),
            'image' => $this->image ? Storage::url($this->image->path) : null,
            'maximum_point' => PracticeService::calculatePoint($this->difficulty, $this->questions()->count()),
            'subject' => $this->subject()->get(['id', 'name'])->first(),
            'unit' => $this->resource->semester->unit->only(['id', 'name']),
            'semester' => $this->semester()->get(['id', 'name'])->first(),
            'year' => $this->resource->year,
            'student_type' => $this->student_type,
            'questions' => $questions,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
