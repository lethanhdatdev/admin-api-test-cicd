<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class StudentResource extends JsonResource
{
    public function toArray($request): array
    {
        $birthday = $this->birthday ? new Carbon($this->birthday) : null;

        return [
            'id' => $this->id,
            'code' => $this->code,
            'address' => $this->address,
            'birthday' => $birthday?->format('d/m/Y'),
            'phone' => $this->phone,
            'point' => $this->points(),
            'lock' => $this->lock,
            'unit' => $this->unit()->get(['id', 'name'])->first(),
            'user' => new UserResource($this->user),
            'ancestor' => [
                'id' => $this->ancestor->id,
                'name' => $this->ancestor->user->name,
                'email' => $this->ancestor->user->email,
                'birthday' => $this->ancestor->birthday,
                'address' => $this->ancestor->address,
                'phone' => $this->ancestor->phone
            ],
            'created_at' => (new Carbon($this->created_at))?->format('d/m/Y H:i:s'),
            'updated_at' => $this->updated_at,
        ];
    }
}
