<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class AncestorResource extends JsonResource
{
    public function toArray($request): array
    {
        $birthday = $this->birthday ? new Carbon($this->birthday) : null;

        return [
            'id' => $this->id,
            'affiliate_code' => $this->affiliate_code,
            'birthday' => $birthday?->format('d/m/Y'),
            'address' => $this->address,
            'phone' => $this->phone,
            'user' => new UserResource($this->user),
            'student_count' => $this->students()->count(),
            'created_at' => (new Carbon($this->created_at))?->format('d/m/Y H:i:s'),
            'updated_at' => $this->updated_at,
        ];
    }
}
