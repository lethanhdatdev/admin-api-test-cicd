<?php

namespace App\Http\Resources;

use App\Models\File;
use App\Models\Practice;
use App\Models\PracticeCategory;
use App\Models\Question;
use App\Models\Semester;
use App\Models\Subject;
use App\Services\PracticeService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $difficulty
 * @property string $practice_category_id
 * @property int $subject_id
 * @property int $semester_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection<Question> $questions
 * @property File $image
 * @property PracticeCategory $category
 *
 * @method BelongsTo subject()
 * @method BelongsTo semester()
 * @method BelongsTo category()
 * @method BelongsToMany questions()
 *
 * @property Practice $resource
 */
class PracticeResource extends JsonResource
{
    public function toArray($request): array
    {
        $thematics = new Collection();
        foreach ($this->questions as $question) {
            $thematics->add($question->thematic);
        }
        $thematics = $thematics->groupBy('id')
            ->sortBy(static function ($item) {
                return $item->count();
            }, SORT_REGULAR, true)
            ->map(static function ($item) {
                return ['id' => $item->first()->id, 'name' => $item->first()->name];
            });

        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'difficulty' => $this->difficulty,
            'subject' => $this->subject()->get(['id', 'name'])->first(),
            'semester' => $this->semester()->get(['id', 'name'])->first(),
            'unit' => $this->resource->semester->unit->only(['id', 'name']),
            'category' => $this->category()->get(['id', 'name'])->first(),
            'year' => $this->resource->year,
            'image' => $this->image ? Storage::url($this->image->path) : null,
            'maximum_point' => PracticeService::calculatePoint($this->difficulty, $this->questions()->count()),
            'questions_count' => $this->questions()->count(),
            'thematic' => $thematics->first(),
            'student_type' => $this->student_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
