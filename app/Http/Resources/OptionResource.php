<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OptionResource extends JsonResource
{
    public const LABEL_KEY = 'label';
    public const VALUE_KEY = 'value';

    public function toArray($request): array
    {
        return [
            self::LABEL_KEY => is_object($this->resource) ? $this->resource->{self::LABEL_KEY} : $this->resource[self::LABEL_KEY],
            self::VALUE_KEY => is_object($this->resource) ? $this->resource->{self::VALUE_KEY} : $this->resource[self::VALUE_KEY],
        ];
    }
}
