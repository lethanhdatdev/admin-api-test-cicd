<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TestSchedulesResource extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => TestScheduleResource::collection($this->collection),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
