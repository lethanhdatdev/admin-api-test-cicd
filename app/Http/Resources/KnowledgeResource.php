<?php

namespace App\Http\Resources;

use App\Models\File;
use App\Models\Knowledge;
use App\Models\Thematic;
use App\Models\Topic;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property int $thematic_id
 * @property string $name
 * @property string $intro
 * @property string $description
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Thematic $thematic
 * @property Collection<File> $files
 * @property Collection<Topic> $topics
 *
 * @method BelongsToMany topics()
 * @method BelongsTo thematic()
 *
 * @property Knowledge $resource
 */
class KnowledgeResource extends JsonResource
{
    public function toArray($request): array
    {
        $files = [];
        foreach ($this->files as $file) {
            $files[$file->type] = Storage::url($file->path);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'intro' => $this->intro,
            'description' => $this->description,
            'files' => $files,
            'type' => $this->type,
            'year' => $this->resource->year,
            'thematic' => $this->thematic()->get(['id', 'name'])->first(),
            'unit' => $this->resource->thematic->semester->unit->only(['id', 'name']),
            'subject' => $this->resource->thematic->subject->only(['id', 'name']),
            'semester' => $this->resource->thematic->semester->only(['id', 'name']),
            'learn_type' => $this->learn_type,
            'topics_count' => $this->topics()->count(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
