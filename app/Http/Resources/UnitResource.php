<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UnitResource extends JsonResource
{
    public function toArray($request): array
    {
        $semesters = [];
        try {
            $semesters = $this->semesters()->get(['id', 'name']);
        } catch (\Exception $e) {
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'semesters' => $semesters,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
