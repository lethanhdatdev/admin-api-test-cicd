<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TestsResource extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => TestResource::collection($this->collection),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
