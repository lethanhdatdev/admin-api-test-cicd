<?php

namespace App\Http\Resources;

use App\Models\Semester;
use App\Models\Subject;
use App\Models\Test;
use App\Models\TestSchedule;
use App\Models\Thematic;
use App\Models\Unit;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property TestSchedule $resource
 *
 * @property int|null $thematic_id
 * @property Unit|null $unit
 * @property Subject|null $subject
 * @property Semester|null $semester
 * @property Test|null $test
 */
class TestScheduleResource extends JsonResource
{
    public function toArray($request): array
    {
        $thematic = ($this->thematic_id) ?
            // Already getting by list-api
            Thematic::find($this->thematic_id)->only(['id', 'name'])
            // In case for detail
            : $this->resource->thematic()?->only(['id', 'name']);

        return [
            'id' => $this->resource->id,
            'unit' => $this->unit->only(['id', 'name']),
            'subject' => $this->subject->only(['id', 'name']),
            'semester' => $this->semester->only(['id', 'name']),
            'year' => $this->resource->year,
            'thematic' => $thematic,
            'name' => $this->resource->name,
            'test' => $this->test->only(['id', 'name']),
            'status' => $this->resource->status,
            'start_time' => $this->resource->start_time,
            'duration' => $this->test->duration,
            'created_at' => $this->resource->created_at
        ];
    }
}
