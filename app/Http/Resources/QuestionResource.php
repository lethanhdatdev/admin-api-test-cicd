<?php

namespace App\Http\Resources;

use App\Models\Answer;
use App\Models\Test;
use App\Models\Thematic;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $type
 * @property string $description
 * @property string $suggestion
 * @property string $sort_correct
 * @property int $thematic_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Collection<Answer> $answers
 * @property Collection<Test> $tests
 * @property Thematic $thematic
 *
 * @method BelongsTo thematic()
 * @method HasMany answers()
 */
class QuestionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'description' => $this->description,
            'thematic' => $this->thematic()->get(['id', 'name'])->first(),
            'suggestion' => $this->suggestion,
            'sort_correct' => $this->sort_correct,
            'answers_count' => $this->answers()->count(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
