<?php

namespace App\Http\Resources;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

/**
 * @property Event $resource
 */
class EventResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'image' => $this->resource->file ? Storage::disk('s3')->url($this->resource->file->path) : null,
            'unit' => $this->resource->unit->only(['id', 'name']),
            'subject' => $this->resource->subject->only(['id', 'name']),
            'test' => $this->resource->test->only(['id', 'name']),
            'name' => $this->resource->name,
            'event_time' => $this->resource->event_time,
            'status' => $this->resource->status,
            'short_description' => $this->resource->short_description,
            'content' => $this->resource->content,
        ];
    }
}
