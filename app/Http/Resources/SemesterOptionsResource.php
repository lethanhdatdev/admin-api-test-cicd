<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JsonSerializable;

class SemesterOptionsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        foreach ($this->collection as $key => $item) {
            $this->collection->offsetSet($key, new OptionResource(
                [
                    'label' => $item->name,
                    'value' => $item->id
                ]
            ));
        }

        return [
            'data' => $this->collection,
        ];
    }
}
