<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $learn
 * @property mixed $appliedUser
 * @property mixed $customer
 * @property mixed $status
 * @property mixed $price
 * @property mixed $code
 * @property mixed $created_at
 * @method transactionType()
 */
class OrderResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'code' => $this->code,
            'price' => $this->price,
            'status' => $this->status,
            'customer' => new OrderCustomerResource($this->customer),
            'applied_user' => new OrderAppliedUserResource($this->appliedUser),
            'learn' => new OrderLearnResource($this->learn),
            'transaction_type' => $this->transactionType(),
            'created_at' => $this->created_at,
        ];
    }
}
