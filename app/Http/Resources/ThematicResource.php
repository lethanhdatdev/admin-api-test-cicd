<?php

namespace App\Http\Resources;

use App\Models\Semester;
use App\Models\Subject;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $subject_id
 * @property int $semester_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection $knowledge
 *
 * @method HasMany knowledge()
 */
class ThematicResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'subject' => $this->subject,
            'semester' => $this->semester,
            'unit' => $this->semester->unit,
            'knowledge_count' => $this->knowledge()->count(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
