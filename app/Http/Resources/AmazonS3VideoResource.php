<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

/**
 * @property \App\Models\SyncVideo $resource
 */
class AmazonS3VideoResource extends JsonResource
{
    /**
     * @param $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'thumb' => Storage::disk('s3')->url(config('services.amazon-s3.prefix') . '/' . $this->resource->thumb),
            'file' => Storage::disk('s3')->url(config('services.amazon-s3.prefix') . '/' . $this->resource->file),
            'extra' => $this->resource->extra,
            'duration' => $this->resource->duration,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }
}
