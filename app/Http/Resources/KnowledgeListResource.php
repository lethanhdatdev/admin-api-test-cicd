<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JsonSerializable;

class KnowledgeListResource extends ResourceCollection
{
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return [
            'data' => KnowledgeResource::collection($this->collection),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
