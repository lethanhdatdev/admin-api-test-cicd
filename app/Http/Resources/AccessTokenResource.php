<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * @property string $plainTextToken
 * @property array $accessToken
 */
class AccessTokenResource extends JsonResource
{
    public function toArray($request): array
    {
        $expiration = config('sanctum.expiration');

        if (null !== $expiration) {
            $created = new Carbon($this->accessToken->created_at);
            $created->addMinutes($expiration);

            $expiration = $created->unix();
        }

        return [
            'token' => $this->plainTextToken,
            'expired' => $expiration
        ];
    }
}
