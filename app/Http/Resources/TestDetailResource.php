<?php

namespace App\Http\Resources;

use App\Models\File;
use App\Models\Semester;
use App\Models\Subject;
use App\Models\Test;
use App\Services\QuestionService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $subject_id
 * @property int $semester_id
 * @property string $type
 * @property int $duration
 * @property string $difficulty
 * @property string $year
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Test $resource
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection $questions
 * @property File $image
 *
 * @method BelongsTo subject()
 * @method BelongsTo semester()
 */
class TestDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        $questions = [];
        foreach ($this->questions as $question) {
            $answers = [];
            foreach ($question->answers as $answer) {
                $answers[] = [
                    'id' => $answer->id,
                    'type' => $answer->type,
                    'description' => $answer->description,
                    'correct' => $answer->correct
                ];
            }

            $questions[] = [
                'id' => $question->id,
                'type' => $question->type,
                'description' => QuestionService::replaceLaTex($question->description),
                'thematic' => $question->thematic()->get(['id', 'name'])->first(),
                'suggestion' => $question->suggestion,
                'sort_correct' => $question->sort_correct,
                'answers' => $answers
            ];
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'type' => $this->type,
            'difficulty' => $this->difficulty,
            'image' => $this->image ? Storage::url($this->image->path) : null,
            'duration' => $this->duration,
            'subject' => $this->subject()->get(['id', 'name'])->first(),
            'semester' => $this->semester()->get(['id', 'name'])->first(),
            'unit' => $this->semester->unit->only(['id', 'name']),
            'year' => $this->year,
            'status' => $this->resource->status,
            'student_type' => $this->student_type,
            'questions' => $questions,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
