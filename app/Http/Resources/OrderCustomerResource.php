<?php

namespace App\Http\Resources;

use App\Models\Ancestor;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $email
 * @property mixed $name
 * @property Ancestor|Student $userable
 * @method isStudent()
 */
class OrderCustomerResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $entity = $this->userable;
        return [
            'code' => $this->isStudent() ? $entity->code : null,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $entity->phone,
            'birthday' => $entity->birthday,
        ];
    }
}
