<?php

namespace App\Http\Resources;

use App\Models\KnowledgeTopic;
use App\Models\Question;
use App\Services\QuestionService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property KnowledgeTopic $pivot
 */
class KnowledgeTopicResource extends JsonResource
{
    public function toArray($request): array
    {
        $questionEntity = Question::find($this->pivot->question_id);

        $answers = [];
        foreach ($questionEntity->answers as $answer) {
            $answers[] = [
                'id' => $answer->id,
                'type' => $answer->type,
                'description' => $answer->description,
                'correct' => $answer->correct
            ];
        }

        $question = [
            'id' => $questionEntity->id,
            'type' => $questionEntity->type,
            'description' => QuestionService::replaceLaTex($questionEntity->description),
            'suggestion' => $questionEntity->suggestion,
            'sort_correct' => $questionEntity->sort_correct,
            'answers' => $answers
        ];

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'topic_start' => $this->pivot->topic_start,
            'question' => $question,
            'question_start' => $this->pivot->question_start,
            'question_end' => $this->pivot->question_end,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
