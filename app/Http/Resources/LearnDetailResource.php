<?php

namespace App\Http\Resources;

use App\Models\File;
use App\Models\Knowledge;
use App\Models\Learn;
use App\Models\Semester;
use App\Models\Thematic;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property Learn $resource
 *
 * @property Thematic|Semester $entity
 * @property File $image
 *
 * @method  Collection<Knowledge>|null knowledge
 */
class LearnDetailResource extends JsonResource
{
    public function toArray($request): array
    {
        $learn = $this->resource;
        $thematicData = $this->resource->isThematic() ? $this->resource->entity->only('id', 'name') : null;

        return [
            'id' => $learn->id,
            'name' => $learn->name,
            'code' => $learn->code,
            'description' => $learn->description,
            'type' => $learn->type,
            'price' => $learn->price,
            'year' => $learn->year,
            'status' => $learn->status,
            'image' => $learn->image ? Storage::url($learn->image->path) : null,
            'knowledge_count' => $this->resource->knowledge() ? $this->resource->knowledge()->count() : 0,
            'semester' => $learn->semester->only('id', 'name'),
            'unit' => $learn->unit->only('id', 'name'),
            'subject' => $learn->subject->only('id', 'name'),
            'thematic' => $thematicData,
            'created_at' => $learn->created_at,
            'updated_at' => $learn->updated_at,
        ];
    }
}
