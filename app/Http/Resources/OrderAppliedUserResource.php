<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $email
 * @property mixed $name
 * @property mixed $userable
 * @method isStudent()
 */
class OrderAppliedUserResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $entity = $this->userable;
        return [
            'name' => $this->name,
            'email' => $this->email,
            'code' => $this->isStudent() ? $entity->code : null,
            'phone' => $entity->phone,
            'birthday' => $entity->birthday,
        ];
    }
}
