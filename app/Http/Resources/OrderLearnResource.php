<?php

namespace App\Http\Resources;

use App\Services\LearnService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $email
 * @property mixed $name
 * @property mixed $price
 * @property mixed $type
 * @property mixed $description
 * @property mixed $code
 * @property mixed $id
 * @property mixed $entity
 */
class OrderLearnResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $semester = $this->type == LearnService::TYPE_THEMATIC ? $this->entity->semester->name : $this->entity->name;
        $unit = $this->type == LearnService::TYPE_THEMATIC ? $this->entity->semester->unit->name : $this->entity->unit->name;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type,
            'price' => $this->price,
            'year' => $this->resource->year,
            'status' => $this->resource->status,
            'semester' => $semester,
            'unit' => $unit,
        ];
    }
}
