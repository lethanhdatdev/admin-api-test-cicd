<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;

class AmazonS3VideoSyncCheck
{
    public function handle($request, Closure $next)
    {
        if (config('services.amazon-s3.sync') !== $request->header('x-api-key')) {
            throw new AuthenticationException('Unauthenticated.');
        }

        return $next($request);
    }
}
