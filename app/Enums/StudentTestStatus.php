<?php

namespace App\Enums;

enum StudentTestStatus: string
{
    case START = 'START';
    case FINISH = 'FINISH';

    public static function values(): array
    {
        return array_map(static function ($item) {
            return $item->value;
        }, self::cases());
    }
}
