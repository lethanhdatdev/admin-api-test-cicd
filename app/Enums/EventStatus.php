<?php

namespace App\Enums;

enum EventStatus: string
{
    case END = 'END';
    case HAPPENING = 'HAPPENING';
    case GOING_TO_HAPPEN = 'GOING_TO_HAPPEN';

    public static function values(): array
    {
        return array_map(static function ($item) {
            return $item->value;
        }, self::cases());
    }
}
