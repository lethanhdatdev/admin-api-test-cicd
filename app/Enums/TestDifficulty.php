<?php

namespace App\Enums;

enum TestDifficulty: string
{
    case NORMAL = 'NORMAL';
    case ADVANCED = 'ADVANCED';

    public static function values(): array
    {
        return array_map(static function ($item) {
            return $item->value;
        }, self::cases());
    }
}
