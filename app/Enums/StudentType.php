<?php

namespace App\Enums;

enum StudentType: string
{
    case NORMAL = 'NORMAL';
    case VIP = 'VIP';
}