<?php

namespace App\Enums;

enum LearnType: string
{
    case SEMESTER = 'SEMESTER';
    case THEMATIC = 'THEMATIC';
}
