<?php

namespace App\Enums;

enum TestType: string
{
    case OFFICIAL = 'OFFICIAL';
    case PRACTICE = 'PRACTICE';
    case EVENT = 'EVENT';

    public static function values(): array
    {
        return array_map(static function ($item) {
            return $item->value;
        }, self::cases());
    }
}
