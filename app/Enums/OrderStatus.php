<?php

namespace App\Enums;

enum OrderStatus: string
{
    case PENDING = 'PENDING';
    case SUCCESS = 'SUCCESS';
    case CANCELLED = 'CANCELLED';
}
