<?php

namespace App\Enums;

enum OrderTransactionStatus: string
{
    case PENDING = 'PENDING';
    case FAIL = 'FAIL';
    case SUCCESS = 'SUCCESS';
}
