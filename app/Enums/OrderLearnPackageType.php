<?php

namespace App\Enums;

enum OrderLearnPackageType: string
{
    case THEMATIC = 'THEMATIC';
    case SEMESTER = 'SEMESTER';
}
