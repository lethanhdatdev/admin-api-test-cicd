<?php

namespace App\Providers;

use App\Checks\AmazonS3ServiceChecks;
use Illuminate\Support\ServiceProvider;
use Spatie\Health\Checks\Checks\CacheCheck;
use Spatie\Health\Checks\Checks\DatabaseCheck;
use Spatie\Health\Checks\Checks\DebugModeCheck;
use Spatie\Health\Checks\Checks\EnvironmentCheck;
use Spatie\Health\Checks\Checks\OptimizedAppCheck;
use Spatie\Health\Facades\Health;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Health::checks([
            DebugModeCheck::new(),
            CacheCheck::new()->name('Application Cache'),
            DatabaseCheck::new()->name('Database')->label('PostgreSQL'),
            AmazonS3ServiceChecks::new()->label('Amazon S3'),
            OptimizedAppCheck::new()->name('Optimize App')->label('Optimized app check'),
            EnvironmentCheck::new(),
//            RedisCheck::new(),
        ]);
    }
}
