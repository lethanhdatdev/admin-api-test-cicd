<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $type
 * @property string $description
 * @property string $suggestion
 * @property string $sort_correct
 * @property int $thematic_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Collection<Answer> $answers
 * @property Collection<Test> $tests
 * @property Thematic $thematic
 */
class Question extends Model
{
    protected $fillable = [
        'description',
        'type',
        'thematic_id',
        'suggestion',
        'sort_correct'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function thematic(): BelongsTo
    {
        return $this->belongsTo(Thematic::class);
    }

    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }

    public function tests(): BelongsToMany
    {
        return $this->belongsToMany(Test::class, QuestionTest::class);
    }
}
