<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property string $name
 * @property string $type
 * @property string $mime
 * @property string $path
 */
class File extends Model
{
    protected $fillable = [
        'name',
        'type',
        'mime',
        'path'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function entity(): MorphTo
    {
        return $this->morphTo();
    }
}
