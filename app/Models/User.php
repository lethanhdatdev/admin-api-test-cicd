<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property string $userable_type
 * @property Student|Ancestor $userable
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public function userable(): MorphTo
    {
        return $this->morphTo();
    }

    public function isStudent(): bool
    {
        return $this->userable_type === Student::class;
    }

    public function isAncestor(): bool
    {
        return $this->userable_type === Ancestor::class;
    }

    public function isSuperAdmin(): bool
    {
        return $this->email === 'admin@bittus.vn';
    }

    public function learns(): BelongsToMany
    {
        return $this->belongsToMany(Learn::class, 'user_learn', 'user_id', 'learn_id');
    }
}
