<?php

namespace App\Models;

use App\Enums\Status;
use App\Services\LearnService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $type
 * @property float $price
 * @property string $year
 * @property string|Status $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Thematic|Semester $entity
 * @property File $image
 * @property Collection<Knowledge> $knowledge
 * @property Unit $unit
 * @property Subject $subject
 * @property Thematic $thematic
 * @property Semester $semester
 */
class Learn extends Model
{
    protected $fillable = [
        'code',
        'name',
        'description',
        'type',
        'price',
        'year',
        'status',
        'thematic_id',
        'semester_id',
        'subject_id',
        'unit_id',
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function entity(): MorphTo
    {
        return $this->morphTo();
    }

    public function image(): MorphOne
    {
        return $this->morphOne(File::class, 'entity');
    }

    public function isThematic(): bool
    {
        return $this->type === LearnService::TYPE_THEMATIC;
    }

    public function isSemester(): bool
    {
        return $this->type === LearnService::TYPE_SEMESTER;
    }

    public function knowledge(): Collection|null
    {
        if (null === $this->entity) {
            return null;
        }

        $learnYear = $this->year;
        $learnType = $this->type;

        if ($this->isThematic()) {
            /** @var Thematic $entity */
            return $this->entity
                ->knowledge()
                ->where(static function (Builder $query) use ($learnType) {
                    $query->whereNotNull('learn_type');
                    $query->where('learn_type', '=', $learnType);
                })
                ->where(static function (Builder $query) use ($learnYear) {
                    $query->whereNotNull('year');
                    $query->where('year', '=', $learnYear);
                })
                ->get();
        }

        if ($this->isSemester()) {
            /** @var Semester $entity */
            $entity = $this->entity;

            $collection = new Collection();
            $entity->thematics->map(static function (Thematic $thematic) use ($collection, $learnYear, $learnType) {
                $thematic->knowledge()
                    ->where(static function (Builder $query) use ($learnType) {
                        $query->whereNotNull('learn_type');
                        $query->where('learn_type', '=', $learnType);
                    })
                    ->where(static function (Builder $query) use ($learnYear) {
                        $query->whereNotNull('year');
                        $query->where('year', '=', $learnYear);
                    })
                    ->get()->map(static function (Knowledge $knowledge) use ($collection) {
                        $collection->push($knowledge);
                    });
            });

            return $collection;
        }

        return new Collection();
    }

    public function semester(): BelongsTo
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function knowledgeList(): BelongsToMany
    {
        return $this->belongsToMany(Knowledge::class, 'learn_knowledge', 'learn_id', 'knowledge_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_learn', 'learn_id', 'user_id');
    }
}
