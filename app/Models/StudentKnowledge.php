<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property int $student_id
 * @property int $knowledge_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Student $student
 * @property Knowledge $knowledge
 */
class StudentKnowledge extends Model
{
    protected $table = 'student_knowledge';

    protected $fillable = [
        'student_id',
        'knowledge_id',
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function knowledge(): BelongsTo
    {
        return $this->belongsTo(Knowledge::class);
    }

    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class);
    }

    public function topicAnswers(): BelongsToMany
    {
        return $this->belongsToMany(Topic::class, StudentKnowledgeTopicAnswer::class)
            ->withPivot(['correct', 'answer'])
            ->withTimestamps();
    }
}
