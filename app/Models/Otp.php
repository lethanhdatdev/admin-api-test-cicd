<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $code
 * @property string $sms_id
 * @property string $phone
 * @property string $expired
 */
class Otp extends Model
{
    protected $fillable = [
        'code',
        'sms_id',
        'phone',
        'expired'
    ];

    protected $dateFormat = "Y-m-d H:i:s";
}
