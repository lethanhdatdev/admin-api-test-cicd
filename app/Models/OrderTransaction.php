<?php

namespace App\Models;

use App\Enums\OrderTransactionStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderTransaction extends Model
{
    protected $fillable = [
        'order_id',
        'type',
        'status'
    ];

    protected $casts = [
        'status' => OrderTransactionStatus::class,
    ];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
