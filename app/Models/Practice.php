<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $difficulty
 * @property string $practice_category_id
 * @property int $subject_id
 * @property int $semester_id
 * @property string $year
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection $questions
 * @property File $image
 * @property PracticeCategory $category
 */
class Practice extends Model
{
    protected $fillable = [
        'code',
        'name',
        'difficulty',
        'subject_id',
        'semester_id',
        'practice_category_id',
        'year',
        'student_type'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    public function semester(): BelongsTo
    {
        return $this->belongsTo(Semester::class);
    }

    public function questions(): BelongsToMany
    {
        return $this->belongsToMany(Question::class, QuestionPractice::class);
    }

    public function image(): MorphOne
    {
        return $this->morphOne(File::class, 'entity');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(PracticeCategory::class, 'practice_category_id');
    }
}
