<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property int $unit_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Unit $unit
 * @property Collection<Thematic> $thematics
 * @property Learn $learn
 */
class Semester extends Model
{
    use softDeletes;

    protected $fillable = [
        'name',
        'unit_id'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    }

    public function thematics(): HasMany
    {
        return $this->hasMany(Thematic::class);
    }

    public function learn(): MorphOne
    {
        return $this->morphOne(Learn::class, 'entity');
    }
}
