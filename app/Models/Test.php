<?php

namespace App\Models;

use App\Enums\Status;
use App\Enums\TestDifficulty;
use App\Enums\TestType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $subject_id
 * @property int $semester_id
 * @property TestType $type
 * @property int $duration
 * @property TestDifficulty $difficulty
 * @property string $created_at
 * @property string $updated_at
 * @property string $year
 * @property Status $status
 *
 * @property Subject $subject
 * @property Semester $semester
 * @property Collection $questions
 * @property File $image
 */
class Test extends Model
{
    protected $fillable = [
        'code',
        'name',
        'type',
        'duration',
        'difficulty',
        'subject_id',
        'semester_id',
        'year',
        'student_type'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    protected $casts = [
        'status' => Status::class,
        'difficulty' => TestDifficulty::class,
        'type' => TestType::class
    ];

    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    public function semester(): BelongsTo
    {
        return $this->belongsTo(Semester::class);
    }

    public function questions(): BelongsToMany
    {
        return $this->belongsToMany(Question::class, QuestionTest::class);
    }

    public function image(): MorphOne
    {
        return $this->morphOne(File::class, 'entity');
    }

    public function isEvent(): bool
    {
        return $this->type === TestType::EVENT;
    }

    public function isOfficial(): bool
    {
        return $this->type === TestType::OFFICIAL;
    }

    public function isPractice(): bool
    {
        return $this->type === TestType::PRACTICE;
    }
}
