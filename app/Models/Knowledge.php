<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property string $intro
 * @property string $description
 * @property string $type
 * @property string $year
 * @property string $created_at
 * @property string $updated_at
 * @property int $thematic_id
 *
 * @property Thematic $thematic
 * @property Collection<File> $files
 * @property Collection<Topic> $topics
 */
class Knowledge extends Model
{
    protected $fillable = [
        'name',
        'intro',
        'description',
        'type',
        'thematic_id',
        'year',
        'learn_type',
        'duration'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function thematic(): BelongsTo
    {
        return $this->belongsTo(Thematic::class);
    }

    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'entity');
    }

    public function topics(): BelongsToMany
    {
        return $this->belongsToMany(Topic::class, KnowledgeTopic::class)->withPivot(
            [
                'topic_start',
                'question_id',
                'question_start',
                'question_end'
            ]
        );
    }
}
