<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Answer extends Model
{
    protected $fillable = [
        'type',
        'description',
        'correct',
        'question_id',
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }
}
