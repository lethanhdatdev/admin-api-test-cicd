<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $student_knowledge_id
 * @property int $topic_id
 * @property boolean $correct
 * @property string $answer
 * @property string $created_at
 * @property string $updated_at
 *
 * @property StudentKnowledge $studentKnowledge
 * @property Topic $topic
 */
class StudentKnowledgeTopicAnswer extends Model
{
    protected $fillable = [
        'student_knowledge_id',
        'topic_id',
        'correct',
        'answer'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function studentKnowledge(): BelongsTo
    {
        return $this->belongsTo(StudentKnowledge::class);
    }

    public function topic(): BelongsTo
    {
        return $this->belongsTo(Topic::class);
    }
}
