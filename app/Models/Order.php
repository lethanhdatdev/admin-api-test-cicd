<?php

namespace App\Models;

use App\Enums\OrderStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    protected $fillable = [
        'code',
        'price',
        'status',
        'learn_id',
        'customer_id',
        'applied_user_id',
        'coupon_id'
    ];

    protected $casts = [
        'status' => OrderStatus::class,
    ];

    public function histories(): HasMany
    {
        return $this->hasMany(OrderHistory::class, 'order_id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(OrderTransaction::class, 'order_id');
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function appliedUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'applied_user_id');
    }

    public function learn(): BelongsTo
    {
        return $this->belongsTo(Learn::class, 'learn_id');
    }

    public function transactionType()
    {
        return $this->transactions()->orderBy('id', 'desc')->first()->transaction_type;
    }
}
