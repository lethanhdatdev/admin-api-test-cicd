<?php

namespace App\Models;

use App\Services\PracticeService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property int $student_id
 * @property int $practice_id
 * @property string $status
 * @property int $reset_count
 * @property int $point
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Collection<Question> $questionsWithAnswer
 */
class StudentPractices extends Model
{
    protected $fillable = [
        'student_id',
        'practice_id',
        'status'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function practice(): BelongsTo
    {
        return $this->belongsTo(Practice::class);
    }

    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class);
    }

    public function questionsWithAnswer(): BelongsToMany
    {
        return $this->belongsToMany(Question::class, StudentPracticeAnswers::class)
            ->withPivot(['correct', 'answer', 'duration'])
            ->withTimestamps();
    }

    public function answers(): HasMany
    {
        return $this->hasMany(StudentPracticeAnswers::class);
    }

    public function isFinish(): bool
    {
        return $this->status === PracticeService::FINISH;
    }

    public function isUnFinished(): bool
    {
        return $this->status === PracticeService::UNFINISHED;
    }

    public function isPause(): bool
    {
        return $this->status === PracticeService::PAUSE;
    }
}
