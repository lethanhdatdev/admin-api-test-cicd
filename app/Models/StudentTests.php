<?php

namespace App\Models;

use App\Enums\StudentTestStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property int $student_id
 * @property int $test_id
 * @property StudentTestStatus $status
 * @property string|Carbon $expired
 * @property int $point
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Test $test
 *
 * @property Collection<Question> $questionsWithAnswer
 */
class StudentTests extends Model
{
    protected $fillable = [
        'student_id',
        'test_id',
        'status',
        'point',
        'expired'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    protected $casts = [
        'status' => StudentTestStatus::class,
        'expired' => 'datetime'
    ];

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class);
    }

    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class);
    }

    public function questionsWithAnswer(): BelongsToMany
    {
        return $this->belongsToMany(Question::class, StudentTestAnswers::class)
            ->withPivot(['correct', 'answer', 'duration'])
            ->withTimestamps();
    }

    public function answers(): HasMany
    {
        return $this->hasMany(StudentTestAnswers::class);
    }

    public function isFinish(): bool
    {
        return $this->status === StudentTestStatus::FINISH;
    }
}
