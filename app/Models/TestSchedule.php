<?php

namespace App\Models;

use App\Enums\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $unit_id
 * @property int $subject_id
 * @property int $semester_id
 * @property int $test_id
 * @property string $year
 * @property Status $status
 * @property string $start_time
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Unit $unit
 * @property Subject $subject
 * @property Semester $semester
 * @property Test $test
 */
class TestSchedule extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'unit_id',
        'subject_id',
        'semester_id',
        'test_id',
        'year',
        'start_time'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    protected $casts = [
        'status' => Status::class,
        'start_time' => 'datetime'
    ];

    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    }

    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    public function semester(): BelongsTo
    {
        return $this->belongsTo(Semester::class);
    }

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class);
    }

    public function thematic(): Thematic|null
    {
        if ($this->test?->questions?->isEmpty()) {
            return null;
        }

        $thematics = new Collection();

        if ($this->test) {
            foreach ($this->test?->questions as $question) {
                $thematics->add($question->thematic);
            }
        }

        return $thematics->groupBy('id')
            ->sortBy(static function ($item) {
                return $item->count();
            }, SORT_REGULAR, true)
            ->map(static function ($item) {
                return $item->first();
            })
            ->first();
    }
}
