<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionPractice extends Model
{
    protected $table = 'question_practice';

    protected $fillable = [
        'question_id',
        'practice_id'
    ];
}
