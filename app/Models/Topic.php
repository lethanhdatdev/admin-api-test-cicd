<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @method static Model firstOrCreate(array $attributes = [], array $values = [])
 */
class Topic extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function knowledge(): BelongsToMany
    {
        return $this->belongsToMany(Knowledge::class, KnowledgeTopic::class);
    }

    public function answer(int|null $studentId = null): HasManyThrough|null
    {
        if ($studentId) {
            $student = Student::findOrFail($studentId);
        } else {
            $student = auth()->user()->isStudent() ? auth()->user()->userable : null;
        }

        /** @var Student $student */
        return $student?->knowledgeAnswers()->where('topic_id', '=', $this->id);
    }
}
