<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Collection<Semester> $semesters
 */
class Unit extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    protected $dateFormat = "Y-m-d";

    public function semesters(): HasMany
    {
        return $this->hasMany(Semester::class);
    }
}
