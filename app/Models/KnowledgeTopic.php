<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $knowledge_id
 * @property int $topic_id
 * @property int $topic_start
 * @property int $question_id
 * @property int $question_start
 * @property int $question_end
 */
class KnowledgeTopic extends Model
{
    protected $fillable = [
        'knowledge_id',
        'topic_id',
        'topic_start',
        'question_id',
        'question_start',
        'question_end'
    ];
}
