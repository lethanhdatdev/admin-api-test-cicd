<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Services\PracticeService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $birthday
 * @property string $phone
 * @property string $code
 * @property string $address
 * @property int $unit_id
 * @property int $ancestor_id
 * @property bool $lock
 *
 * @property User $user
 * @property Unit $unit
 * @property Collection<Practice> $practices
 * @property Collection<Knowledge> $knowledge
 */
class Student extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'birthday',
        'phone',
        'code',
        'address',
        'unit_id',
        'ancestor_id',
        'lock'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'birthday' => 'date'
    ];

    public function user(): MorphOne
    {
        return $this->morphOne(User::class, 'userable');
    }

    public function ancestor(): BelongsTo
    {
        return $this->belongsTo(Ancestor::class);
    }

    public function unit(): HasOne
    {
        return $this->hasOne(Unit::class, 'id', 'unit_id');
    }

    public function practices(): BelongsToMany
    {
        return $this->belongsToMany(Practice::class, StudentPractices::class)
            ->withPivot(['status', 'point'])
            ->withTimestamps();
    }

    public function knowledge(): BelongsToMany
    {
        return $this->belongsToMany(Knowledge::class, StudentKnowledge::class)
            ->withTimestamps();
    }

    public function knowledgeAnswers(): HasManyThrough
    {
        return $this->hasManyThrough(StudentKnowledgeTopicAnswer::class, StudentKnowledge::class);
    }

    public function getActiveSessionOfPractice(int $practiceId): null|Practice
    {
        $studentPracticesTable = app(StudentPractices::class)->getTable();

        return $this->practices()
            ->withPivot(['id'])
            ->where('practice_id', '=', $practiceId)
            ->where('status', '!=', PracticeService::FINISH)
            ->latest($studentPracticesTable . '.created_at')
            ->first();
    }

    public function points()
    {
        $practicePoint = $this->practices()->withPivot(['id'])
            ->where('status', '=', PracticeService::FINISH)
            ->get()
            ->groupBy('id')->map(static function (Collection $collection) {
                return $collection->max(static function ($item) {
                    return $item->pivot->point;
                });
            })
            ->sum();

        return $practicePoint;
    }
}
