<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static Model firstOrCreate(array $attributes = [], array $values = [])
 */
class Subject extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $dateFormat = "Y-m-d H:i:s";

    public function thematics(): HasMany
    {
        return $this->hasMany(Thematic::class);
    }
}
