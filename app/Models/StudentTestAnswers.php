<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $student_tests_id
 * @property int $question_id
 * @property boolean $correct
 * @property string $answer
 * @property int $duration
 * @property string $created_at
 * @property string $updated_at
 */
class StudentTestAnswers extends Model
{
    protected $fillable = [
        'student_tests_id',
        'question_id',
        'correct',
        'answer',
        'duration'
    ];

    protected $dateFormat = "Y-m-d H:i:s";
}
