<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $file
 * @property string $thumb
 * @property string $extra
 * @property float $duration
 * @property string $created_at
 * @property string $updated_at
 */
class SyncVideo extends Model
{
    protected $fillable = [
        'name',
        'file',
        'thumb',
        'extra',
        'duration'
    ];

    protected $casts = [
        'extra' => 'object'
    ];

    protected $dateFormat = "Y-m-d H:i:s";
}
