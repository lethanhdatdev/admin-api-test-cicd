<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $student_practices_id
 * @property int $question_id
 * @property int $duration
 * @property boolean $correct
 * @property string $answer
 * @property string $created_at
 * @property string $updated_at
 */
class StudentPracticeAnswers extends Model
{
    protected $fillable = [
        'student_practices_id',
        'question_id',
        'correct',
        'answer',
        'duration'
    ];

    protected $dateFormat = "Y-m-d H:i:s";
}
