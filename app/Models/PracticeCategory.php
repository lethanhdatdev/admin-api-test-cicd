<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class PracticeCategory extends Model
{
    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    protected $dateFormat = 'Y-m-d H:i:s';
}
