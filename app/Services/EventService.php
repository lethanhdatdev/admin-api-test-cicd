<?php

namespace App\Services;

use App\Models\Event;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EventService
{

    public function list($paginateData): LengthAwarePaginator
    {
        if (!in_array($paginateData['sort'], ['id', 'unit_name', 'subject_name'])) {
            $paginateData['sort'] = 'id';
        }
        return Event::with('test', 'unit', 'subject', 'file')
            ->join('units', 'events.unit_id', '=', 'units.id')
            ->join('subjects', 'events.subject_id', '=', 'subjects.id')
            ->where(function (Builder $query) use ($paginateData) {
                if (isset($paginateData['search'])) {
                    $search = $paginateData['search'];
                    $query->where("events.name", "like", "%$search%");
                }
            })
            ->where(function (Builder $query) use ($paginateData) {
                $filters = ['status', 'subject_id', 'unit_id'];
                foreach ($filters as $filter) {
                    if (isset($paginateData[$filter])) {
                        $query->where($filter, '=', $paginateData[$filter]);
                    }
                }
            })
            ->select([
                'events.id',
                'events.unit_id',
                'events.subject_id',
                'events.test_id',
                'events.name',
                'events.event_time',
                'events.short_description',
                'events.content',
                'events.status',
                'units.name as unit_name',
                'subjects.name as subject_name'
            ])
            ->orderBy($paginateData['sort'], $paginateData['direction'])
            ->paginate($paginateData['size']);
    }
}
