<?php

namespace App\Services;

class PracticeService
{
    public const FINISH = 'FINISH';
    public const UNFINISHED = 'UNFINISHED';
    public const PAUSE = 'PAUSE';

    public const TYPE_DIFFICULT_NORMAL = 'NORMAL';
    public const TYPE_DIFFICULT_ADVANCED = 'ADVANCED';

    public static function calculatePoint(string $difficulty, int $count = 1): int
    {
        if ($difficulty === self::TYPE_DIFFICULT_ADVANCED) {
            return $count * 3;
        }

        return $count;
    }
}
