<?php

namespace App\Services;

use App\Http\Requests\StudentCreateRequest;
use App\Models\Ancestor;
use Carbon\Carbon;

class StudentService
{
    public static function generateCode(StudentCreateRequest $request): string
    {
        $parts = [
            (new Carbon())->format('Ymd'),
            (new Carbon($request->get('birthday')))->format('Ymd')
        ];
        /** @var Ancestor $ancestor */
        $ancestor = Ancestor::findOrFail($request->get('ancestor_id'));
        $parts[] = str_pad($ancestor->students()->count() + 1, 5, '0', STR_PAD_LEFT);

        return implode('_', $parts);
    }
}
