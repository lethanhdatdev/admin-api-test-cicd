<?php

namespace App\Services;

use App\Models\Learn;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class LearnService
{
    public const TYPE_THEMATIC = 'THEMATIC';
    public const TYPE_SEMESTER = 'SEMESTER';

    public function list($paginateData): LengthAwarePaginator
    {
        if (!in_array($paginateData['sort'], ['name', 'created_at', 'price'])) {
            $paginateData['sort'] = 'created_at';
        }
        return Learn::with('subject', 'knowledgeList', 'unit', 'semester')
            ->where(function (Builder $query) use ($paginateData) {
                if (isset($paginateData['search'])) {
                    $search = $paginateData['search'];
                    $query->where("name", "like", "%$search%");
                    $query->orWhere("code", "like", "%$search%");
                    $query->orWhere("description", "like", "%$search%");
                }
            })
            ->where(function (Builder $query) use ($paginateData) {
                if (isset($paginateData['created_from'])) {
                    $from = $paginateData['created_from'];
                    $query->where("learns.created_at", '>=', "$from 00:00:00");
                }
                if (isset($paginateData['created_to'])) {
                    $to = $paginateData['created_to'];
                    $query->where("learns.created_at", '<=', "$to 23:59:59");
                }
                $filters = ['type', 'year', 'status', 'subject_id', 'unit_id'];
                foreach ($filters as $filter) {
                    if (isset($paginateData[$filter])) {
                        $query->where($filter, '=', $paginateData[$filter]);
                    }
                }
            })
            ->orderBy($paginateData['sort'], $paginateData['direction'])
            ->paginate($paginateData['size']);
    }
}
