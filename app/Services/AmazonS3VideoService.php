<?php

namespace App\Services;

use App\Models\SyncVideo;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class AmazonS3VideoService
{
    public function list($paginateData): LengthAwarePaginator
    {
        if (!in_array($paginateData['sort'], ['created_at', 'name'])) {
            $paginateData['sort'] = 'created_at';
        }
        return SyncVideo::where(function ($query) use ($paginateData) {
            if (isset($paginateData['search'])) {
                $search = $paginateData['search'];
                $query->where("name", "like", "%$search%");
                $query->orWhere("created_at", "like", "%$search%");
            }
        })
            ->orderBy($paginateData['sort'], $paginateData['direction'])
            ->paginate($paginateData['size']);
    }
}
