<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OrderService
{
    public function updateOrder(Order $order, $data): Order
    {
        DB::beginTransaction();
        $order->update($data);
        $order->histories()->create($data);
        $order->transactions()->create(Arr::except($data, 'status') + [
                'status' => $data['status'] == 'SUCCESS' ? 'SUCCESS' : 'FAIL',
                'type' => 'BANK_TRANSFER'
            ]);
        if ($data['status'] == 'SUCCESS') {
            $order->appliedUser->learns()->attach($order->learn_id);
        }
        DB::commit();
        return $order;
    }

    public function list($paginateData): LengthAwarePaginator
    {
        if (!in_array($paginateData['sort'], ['created_at', 'code', 'price', 'status', 'customer_email', 'applied_user_email'])) {
            $paginateData['sort'] = 'created_at';
        }
        return Order::with('learn', 'appliedUser', 'customer')
            ->where(function (Builder $query) use ($paginateData) {
                if (isset($paginateData['status'])) {
                    $status = $paginateData['status'];
                    $query->where("orders.status", '=', $status);
                }
                if (isset($paginateData['type'])) {
                    $type = $paginateData['type'];
                    $query->whereHas('learn', function (Builder $query) use ($type) {
                        $query->where('learns.type', '=', $type);
                    });
                }
                if (isset($paginateData['created_from'])) {
                    $from = $paginateData['created_from'];
                    $query->where("created_at", '>=', "$from 00:00:00");
                }
                if (isset($paginateData['created_to'])) {
                    $to = $paginateData['created_to'];
                    $query->where("created_at", '<=', "$to 23:59:59");
                }
                if (isset($paginateData['search'])) {
                    $search = $paginateData['search'];
                    $query->where("orders.code", "like", "%$search%");
                    $query->orWhere("orders.price", "like", "%$search%");
                    $query->orWhereHas('learn', function (Builder $query) use ($search) {
                        $query->where('learns.name', 'like', "%$search%");
                    });
                    $query->orWhereHas('customer', function ($query) use ($search) {
                        $query->where('users.name', 'like', "%$search%")
                            ->orWhere('users.email', 'like', "%$search%");
                    });
                    $query->orWhereHas('appliedUser', function (Builder $query) use ($search) {
                        $query->where('users.name', 'like', "%$search%")
                            ->orWhere('users.email', 'like', "%$search%");
                    });
                }

            })
            ->orderBy($paginateData['sort'], $paginateData['direction'])
            ->paginate($paginateData['size']);
    }
}
