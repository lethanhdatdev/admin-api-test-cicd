<?php

namespace App\Services;

use App\Models\Ancestor;
use App\Models\Otp;
use App\Models\Student;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;

class OTPService
{
    public const OTP_TYPE_CSKH = 2;

    /**
     * @throws Exception
     */
    public static function startOTP(string $phoneNumber): bool
    {
        // Check Phone number
        $ancestor = Ancestor::where('phone', $phoneNumber)->first();

        if (null !== $ancestor) {
            throw new Exception('Phone number: ' . $phoneNumber . ' is existing');
        }


        $student = Student::where('phone', $phoneNumber)->first();

        if (null !== $student) {
            throw new Exception('Phone number: ' . $phoneNumber . ' is existing');
        }

        /** @var Otp|null $otp */
        $otp = Otp::where('phone', $phoneNumber)->first();

        $otpTimeout = (int)config('services.esms.otp_timeout');
        $otpCode = '';
        $now = new Carbon();

        if ($otp) {
            $expired = new Carbon($otp->expired);

            if ($now >= $expired) {
                $otpCode = str_pad(random_int(1, 999999), 6, '0');
                $expired = $now->addMinutes($otpTimeout);
                $otp->update(['code' => $otpCode, 'expired' => $expired->format('Y-m-d H:i:s')]);
            } else {
                $otpCode = $otp->code;
            }
        } else {
            $expired = $now->addMinutes($otpTimeout);
            $otpCode = str_pad(random_int(1, 999999), 6, '0');

            $otp = Otp::create([
                'code' => $otpCode,
                'phone' => $phoneNumber,
                'expired' => $expired->format('Y-m-d H:i:s')
            ]);
        }

        $content = config('services.esms.otp_message');
        $content = str_replace(['{OTP}', '{timeout}'], [$otpCode, $otpTimeout], $content);

        $data = [
            'ApiKey' => config('services.esms.key'),
            'SecretKey' => config('services.esms.secret'),
            'Content' => $content,
            'Phone' => $phoneNumber,
            'Brandname' => config('services.esms.brand'),
            'SmsType' => static::OTP_TYPE_CSKH,
            'IsUnicode' => 0,
            'SandBox' => config('services.esms.sandbox'),
            'RequestId' => null,
            'CallbackUrl' => null,
            'SendDate' => null
        ];

        $response = Http::withHeaders([
            'Content-Type' => 'application/json'
        ])->post('http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_post_json', $data);

        $response = $response->json();

        if ($response['CodeResult'] !== '100') {
            return false;
        }

        $otp->updateQuietly(['sms_id' => $response['SMSID']]);

        return true;
    }

    public static function otpCheck(string $phoneNumber, string $code): bool
    {
        /** @var Otp|null $otp */
        $otp = Otp::where(['phone' => $phoneNumber, 'code' => $code])->first();

        if (null === $otp) {
            return false;
        }

        $expired = new Carbon($otp->expired);
        $now = new Carbon();

        $result = $now < $expired;

        $otp->delete();

        return $result;
    }
}
