<?php

namespace App\Services;

class QuestionService
{
    public const TYPE_FILL_IN_BLANK = 'FILL_IN_BLANK';
    public const TYPE_CHOICE = 'CHOICE';
    public const TYPE_SORT = 'SORT';

    public static function replaceLaTex(string $input): string
    {
        $regex = '/\\\\(\S.*?)(\\.| |\\n|$)/';
        return preg_replace($regex, '$$\\\$1$$$2', $input);
    }
}
