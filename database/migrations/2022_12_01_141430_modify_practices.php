<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('practices', static function (Blueprint $table) {
            $table->string('image_url', 255)->nullable();
            $table->bigInteger('practice_category_id')->nullable();
            $table->foreign('practice_category_id')
                ->references('id')->on('practice_categories')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('practices', static function (Blueprint $table) {
            $table->dropForeign('practices_practice_category_id_foreign');
            $table->dropColumn(['image_url', 'practice_category_id']);
        });
    }
};
