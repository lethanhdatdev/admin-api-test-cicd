<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\StudentType;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('practices', static function (Blueprint $table) {
            $table->enum('student_type', ['NORMAL', 'VIP'])->nullable()->default('NORMAL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practices', static function (Blueprint $table) {
            $table->dropColumn('student_type');
        });
    }
};
