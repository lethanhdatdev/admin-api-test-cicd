<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('student_knowledge', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('student_id')->unsigned()->nullable(false);
            $table->bigInteger('knowledge_id')->unsigned()->nullable(false);
            $table->foreign('student_id')
                ->references('id')
                ->on('students')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('knowledge_id')
                ->references('id')
                ->on('knowledge')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->unique(['student_id', 'knowledge_id']);
            $table->comment('Học viên tham học kiến thức');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('student_knowledge');
    }
};
