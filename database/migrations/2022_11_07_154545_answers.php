<?php

use App\Services\QuestionService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', static function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->boolean('correct');
            $table->enum('type', [QuestionService::TYPE_FILL_IN_BLANK, QuestionService::TYPE_CHOICE, QuestionService::TYPE_SORT]);
            $table->bigInteger('question_id');
            $table->foreign('question_id')
                ->references('id')->on('questions')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
};
