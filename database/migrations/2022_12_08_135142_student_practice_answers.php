<?php

use App\Services\PracticeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('student_practice_answers', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('student_practices_id')->unsigned()->nullable(false);
            $table->bigInteger('question_id')->unsigned()->nullable(false);
            $table->boolean('correct')->nullable(false);
            $table->text('answer')->nullable(false);

            $table->foreign('student_practices_id')
                ->references('id')
                ->on('student_practices')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->unique(['student_practices_id', 'question_id']);

            $table->comment('Học viên trả lời câu hỏi trong bài thực hành');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('student_practice_answers');
    }
};
