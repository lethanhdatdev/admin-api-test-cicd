<?php

use App\Services\PracticeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_practice', static function (Blueprint $table) {
            $table->integer('duration')->nullable();
            $table->enum('status', [PracticeService::FINISH, PracticeService::UNFINISHED])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumns('user_practice', ['duration', 'status'])) {
            Schema::table('user_practice', static function (Blueprint $table) {
                $table->dropColumn(['duration', 'status']);
            });
        }
    }
};
