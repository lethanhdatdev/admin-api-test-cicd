<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->text('description');
            $table->bigInteger('thematic_id');
            $table->foreign('thematic_id')
                ->references('id')->on('thematics')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->timestamps();
            $table->comment('Bài học');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
};
