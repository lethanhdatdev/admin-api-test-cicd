<?php

use App\Services\QuestionService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', static function (Blueprint $table) {
            $table->id();
            $table->string('type', 50);
            $table->string('mime', 50);
            $table->string('path', 255);
            $table->string('name', 255);
            $table->bigInteger('entity_id')->nullable();
            $table->string('entity_type')->nullable();
            $table->timestamps();
        });

        Schema::table('knowledge', static function (Blueprint $table) {
            $table->dropColumn('picture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');

        Schema::table('knowledge', static function (Blueprint $table) {
            $table->string('picture', 255)->nullable();
        });
    }
};
