<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('student_practices', static function (Blueprint $table) {
            $table->dropColumn('duration');
        });

        Schema::table('student_practice_answers', static function (Blueprint $table) {
            $table->integer('duration')->unsigned()->nullable(false)->default(0)->after('answer');
        });
    }

    public function down(): void
    {
        Schema::table('student_practices', static function (Blueprint $table) {
            $table->integer('duration')->unsigned()->nullable(false)->default(0);
        });

        Schema::table('student_practice_answers', static function (Blueprint $table) {
            $table->dropColumn('duration');
        });
    }
};
