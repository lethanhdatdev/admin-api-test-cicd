<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('user_students', static function (Blueprint $table) {
            $table->id();
            $table->string('code', 255);
            $table->date('birthday');
            $table->string('address', 255);
            $table->string('phone', 15);
            $table->bigInteger('parent_id');
            $table->foreign('parent_id')
                ->references('id')->on('user_parents')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->bigInteger('unit_id');
            $table->foreign('unit_id')
                ->references('id')->on('units')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->comment('Học sinh / sinh viên');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_students');
    }
};
