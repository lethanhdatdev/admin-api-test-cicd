<?php

use App\Services\KnowledgeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('knowledge', static function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('intro', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('picture', 255)->nullable();
            $table->enum('type', [KnowledgeService::TYPE_NORMAL, KnowledgeService::TYPE_ADVANCED]);
            $table->bigInteger('lesson_id');
            $table->foreign('lesson_id')
                ->references('id')->on('lessons')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->timestamps();
            $table->comment('Kiến thức');
        });
    }

    public function down()
    {
        Schema::dropIfExists('knowledge');
    }
};
