<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('user_students', static function (Blueprint $table) {
            $table->boolean('lock')->nullable(false)->default(false);
            $table->float('point')->nullable(false)->default(0);
        });
    }

    public function down()
    {
        Schema::table('user_students', function (Blueprint $table) {
            $table->dropColumn(['lock', 'point']);
        });
    }
};
