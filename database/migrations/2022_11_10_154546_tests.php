<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', static function (Blueprint $table) {
            $table->id();
            $table->string('code', 100)->unique();
            $table->string('name', 255);
            $table->bigInteger('subject_id');
            $table->foreign('subject_id')
                ->references('id')->on('subjects')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->bigInteger('semester_id');
            $table->foreign('semester_id')
                ->references('id')->on('semesters')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->enum('type', ['EVENT', 'PRACTICE', 'OFFICIAL']);
            $table->integer('duration');
            $table->enum('difficulty', ['NORMAL', 'ADVANCED']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
};
