<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_unit', static function (Blueprint $table) {
            $table->bigInteger('subject_id')->unsigned();
            $table->bigInteger('unit_id')->unsigned();
            $table->foreign('subject_id')
                ->references('id')->on('subjects')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('unit_id')
                ->references('id')->on('units')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_unit');
    }
};
