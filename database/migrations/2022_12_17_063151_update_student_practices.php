<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('student_practices', static function (Blueprint $table) {
            $table->dropUnique('student_practices_student_id_practice_id_unique');
        });
    }

    public function down(): void
    {
        Schema::table('student_practices', static function (Blueprint $table) {
            $table->unique(['student_id', 'practice_id']);
        });
    }
};
