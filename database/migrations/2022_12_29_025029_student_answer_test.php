<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_test_answers', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('student_tests_id')->unsigned()->nullable(false);
            $table->bigInteger('question_id')->unsigned()->nullable(false);
            $table->boolean('correct')->nullable();
            $table->text('answer')->nullable();
            $table->integer('duration')->nullable();

            $table->foreign('student_tests_id')
                ->references('id')
                ->on('student_tests')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->unique(['student_tests_id', 'question_id']);

            $table->comment('Học viên trả lời câu hỏi trong bài thi');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_test_answers');
    }
};
