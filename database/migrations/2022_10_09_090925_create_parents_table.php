<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('users', static function (Blueprint $table) {
            $table->bigInteger('userable_id')->nullable();
            $table->string('userable_type')->nullable();

            $table->dropColumn(['user_code', 'user_affiliate_code', 'birthday', 'phone', 'point', 'address']);
        });

        Schema::create('user_parents', static function (Blueprint $table) {
            $table->id();
            $table->string('affiliate_code', 255)->nullable();
            $table->date('birthday');
            $table->string('address', 255);
            $table->string('phone', 15)->nullable();
            $table->comment('Phụ huynh');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('users', static function (Blueprint $table) {
            $table->string('user_code', 30)->nullable();
            $table->string('user_affiliate_code', 255)->nullable();
            $table->date('birthday')->nullable();
            $table->string('phone', 15)->nullable();
            $table->float('point')->nullable();
            $table->string('address', 255)->nullable();

            $table->dropColumn(['userable_id', 'userable_type']);
        });

        Schema::dropIfExists('user_parents');
    }
};
