<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('learns', function (Blueprint $table) {
            $table->unsignedBigInteger('thematic_id')->nullable()->default(null);
            $table->unsignedBigInteger('semester_id')->nullable()->default(null);
        });
    }
};
