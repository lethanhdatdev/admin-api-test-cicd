<?php

use App\Services\QuestionService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', static function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->enum('type', [QuestionService::TYPE_FILL_IN_BLANK, QuestionService::TYPE_CHOICE, QuestionService::TYPE_SORT]);
            $table->bigInteger('thematic_id');
            $table->foreign('thematic_id')
                ->references('id')->on('thematics')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->string('suggestion');
            $table->string('sort_correct')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
};
