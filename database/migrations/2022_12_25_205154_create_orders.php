<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code', 255);
            $table->unsignedFloat('price');
            $table->string('status', 255);
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('applied_user_id');
            $table->unsignedBigInteger('learn_id');
            $table->unsignedBigInteger('coupon_id')->nullable()->default(null);
            $table->foreign('customer_id')
                ->references('id')
                ->on('users')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('applied_user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('learn_id')
                ->references('id')
                ->on('learns')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
