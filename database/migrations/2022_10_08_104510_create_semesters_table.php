<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('semesters', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->bigInteger('unit_id');
            $table->foreign('unit_id')
                ->references('id')->on('units')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->comment('Học kỳ');
            $table->timestamps();
            $table->unique(['name', 'unit_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('semesters');
    }
};
