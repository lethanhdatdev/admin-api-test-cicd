<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('sync_videos', static function (Blueprint $table) {
            $table->float('duration')->unsigned()->default(0.0)->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('sync_videos', static function (Blueprint $table) {
            $table->dropColumn('duration');
        });
    }
};
