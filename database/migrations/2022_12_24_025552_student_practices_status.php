<?php

use App\Services\PracticeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(static function () {
            DB::statement('ALTER TABLE student_practices DROP CONSTRAINT student_practices_status_check;');
            DB::statement(
                'ALTER TABLE student_practices ADD CONSTRAINT student_practices_status_check'
                . ' CHECK (status::TEXT = ANY (ARRAY['
                . '\'' . PracticeService::PAUSE . '\'::CHARACTER VARYING,'
                . '\'' . PracticeService::FINISH . '\'::CHARACTER VARYING,'
                . '\'' . PracticeService::UNFINISHED . '\'::CHARACTER VARYING]::TEXT[]))');
        });
    }

    public function down(): void
    {
        DB::transaction(static function () {
            DB::statement('ALTER TABLE student_practices DROP CONSTRAINT student_practices_status_check;');
            DB::statement(
                'ALTER TABLE student_practices ADD CONSTRAINT student_practices_status_check'
                . ' CHECK (status::TEXT = ANY (ARRAY['
                . '\'' . PracticeService::FINISH . '\'::CHARACTER VARYING,'
                . '\'' . PracticeService::UNFINISHED . '\'::CHARACTER VARYING]::TEXT[]))');
        });
    }
};
