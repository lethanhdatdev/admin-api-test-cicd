<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('tests', static function (Blueprint $table) {
            $table->string('year', 9)->nullable()->after('difficulty');
        });

        Schema::table('practices', static function (Blueprint $table) {
            $table->string('year', 9)->nullable()->after('difficulty');
        });

        Schema::table('knowledge', static function (Blueprint $table) {
            $table->string('year', 9)->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('tests', static function (Blueprint $table) {
            $table->dropColumn('year');
        });
        Schema::table('practices', static function (Blueprint $table) {
            $table->dropColumn('year');
        });
        Schema::table('knowledge', static function (Blueprint $table) {
            $table->dropColumn('year');
        });
    }
};
