<?php

use App\Enums\StudentTestStatus;
use App\Services\PracticeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('student_tests', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('student_id')->unsigned()->nullable(false);
            $table->bigInteger('test_id')->unsigned()->nullable(false);
            $table->enum('status', ['START', 'FINISH'])
                ->nullable(false)
                ->default(PracticeService::UNFINISHED);
            $table->timestamp('expired')->nullable(false);

            $table->foreign('student_id')
                ->references('id')
                ->on('students')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('test_id')
                ->references('id')
                ->on('tests')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->unique(['student_id', 'test_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('student_tests');
    }
};
