<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', static function (Blueprint $table) {
            $table->dropForeign('user_students_parent_id_foreign');
            $table->renameColumn('parent_id', 'ancestor_id');
            $table->foreign('ancestor_id')
                ->references('id')->on('ancestors')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', static function (Blueprint $table) {
            $table->dropForeign('students_ancestor_id_foreign');
            $table->renameColumn('ancestor_id', 'parent_id');
            $table->foreign('parent_id')
                ->references('id')->on('ancestors')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });
    }
};
