<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('student_practice_answers', static function (Blueprint $table) {
            $table->boolean('correct')->nullable()->change();
            $table->text('answer')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table('student_practice_answers', static function (Blueprint $table) {
            $table->dropColumn('correct')->nullable(false);
            $table->dropColumn('answer')->nullable(false);
        });
    }
};
