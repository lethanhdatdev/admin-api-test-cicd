<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('question_knowledge', static function (Blueprint $table) {
            $table->integer('start_time')->unsigned()->nullable();
            $table->integer('end_time')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if (Schema::hasTable('question_knowledge')
            && Schema::hasColumns('question_knowledge', ['start_time', 'end_time'])) {
            Schema::table('question_knowledge', static function (Blueprint $table) {
                $table->dropColumn('start_time');
                $table->dropColumn('end_time');
            });
        }
    }
};
