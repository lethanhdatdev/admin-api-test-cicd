<?php

use App\Enums\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('learns', static function (Blueprint $table) {
            $table->enum('status', [Status::ACTIVE->value, Status::LOCK->value])->nullable()->default(Status::ACTIVE->value);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('learns', static function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
};
