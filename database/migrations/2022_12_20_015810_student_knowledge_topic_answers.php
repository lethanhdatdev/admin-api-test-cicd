<?php

use App\Services\PracticeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('student_knowledge_topic_answers', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('student_knowledge_id')->unsigned()->nullable(false);
            $table->bigInteger('topic_id')->unsigned()->nullable(false);
            $table->boolean('correct')->nullable(false);
            $table->text('answer')->nullable(false);

            $table->foreign('student_knowledge_id')
                ->references('id')
                ->on('student_knowledge')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('topic_id')
                ->references('id')
                ->on('topics')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->unique(['student_knowledge_id', 'topic_id']);

            $table->comment('Học viên trả lời câu hỏi trong chủ điểm - kiến thức');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('student_knowledge_topic_answers');
    }
};
