<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thematics', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->text('description');
            $table->bigInteger('subject_id');
            $table->foreign('subject_id')
                ->references('id')->on('subjects')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->bigInteger('semester_id');
            $table->foreign('semester_id')
                ->references('id')->on('semesters')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->timestamps();
            $table->comment('Chuyên đề');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thematics');
    }
};
