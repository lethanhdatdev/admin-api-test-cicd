<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('test_id');
            $table->text('name');
            $table->dateTime('event_time');
            $table->text('short_description')->nullable()->default(null);
            $table->longText('content')->nullable()->default(null);
            $table->text('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }
};
