<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $lessons = DB::table('lessons')->get(['id', 'thematic_id'])->toArray();

        Schema::table('knowledge', static function (Blueprint $table) {
            $table->bigInteger('thematic_id')->after('type')->nullable();
        });

        foreach ($lessons as $lesson) {
            DB::table('knowledge')
                ->where('lesson_id', '=', $lesson->id)
                ->update(['thematic_id' => $lesson->thematic_id]);
        }

        Schema::table('knowledge', static function (Blueprint $table) {
            $table->dropForeign('knowledge_lesson_id_foreign');
            $table->dropColumn('lesson_id');
            $table->foreign('thematic_id')
                ->references('id')->on('thematics')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });

        Schema::dropIfExists('lessons');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::create('lessons', static function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->text('description');
            $table->bigInteger('thematic_id');
            $table->foreign('thematic_id')
                ->references('id')->on('thematics')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->timestamps();
            $table->comment('Bài học');
        });

        Schema::table('knowledge', static function (Blueprint $table) {
            $table->dropForeign('knowledge_thematic_id_foreign');
            $table->dropColumn('thematic_id');
            $table->bigInteger('lesson_id')->nullable();
            $table->foreign('lesson_id')
                ->references('id')->on('lessons')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
        });
    }
};
