<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('sync_videos', static function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable(false)->unique();
            $table->string('file', 255)->nullable(false)->unique();
            $table->string('thumb')->nullable(false);
            $table->json('extra')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('sync_videos');
    }
};
