<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_tests', static function (Blueprint $table) {
            $table->bigInteger('question_id')->unsigned();
            $table->bigInteger('test_id')->unsigned();
            $table->foreign('question_id')
                ->references('id')->on('questions')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('test_id')
                ->references('id')->on('tests')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_tests');
    }
};
