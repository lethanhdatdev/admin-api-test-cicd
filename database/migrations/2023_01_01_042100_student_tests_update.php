<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('student_tests', static function (Blueprint $table) {
            $table->dropUnique('student_tests_student_id_test_id_unique');
            $table->integer('point')->unsigned()->default(0);
        });
    }

    public function down(): void
    {
        Schema::table('student_tests', static function (Blueprint $table) {
            $table->unique(['student_id', 'test_id']);
            $table->dropColumn('point');
        });
    }
};
