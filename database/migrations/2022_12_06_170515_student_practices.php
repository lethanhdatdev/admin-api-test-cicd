<?php

use App\Services\PracticeService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('student_practices', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('student_id')->unsigned()->nullable(false);
            $table->bigInteger('practice_id')->unsigned()->nullable(false);
            $table->integer('duration')->unsigned()->nullable(false)->default(0);
            $table->enum('status', [PracticeService::FINISH, PracticeService::UNFINISHED])
                ->nullable(false)
                ->default(PracticeService::UNFINISHED);

            $table->foreign('student_id')
                ->references('id')
                ->on('students')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('practice_id')
                ->references('id')
                ->on('practices')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->unique(['student_id', 'practice_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('student_practices');
    }
};
