<?php

use App\Services\LearnService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learns', static function (Blueprint $table) {
            $table->id();
            $table->string('code', 50)->nullable(false)->unique();
            $table->string('name', 255)->nullable(false);
            $table->text('description')->nullable();
            $table->enum('type', [LearnService::TYPE_SEMESTER, LearnService::TYPE_THEMATIC])
                ->nullable(false);
            $table->float('price')->nullable(false)->default(0.0);
            $table->bigInteger('entity_id')->nullable();
            $table->string('entity_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('learns');
        Schema::enableForeignKeyConstraints();
    }
};
