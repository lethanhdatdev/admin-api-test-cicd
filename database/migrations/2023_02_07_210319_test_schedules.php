<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('test_schedules', static function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable(false);
            $table->text('description')->nullable();
            $table->bigInteger('unit_id')->unsigned()->nullable(false);
            $table->bigInteger('subject_id')->unsigned()->nullable(false);
            $table->bigInteger('semester_id')->unsigned()->nullable(false);
            $table->bigInteger('test_id')->unsigned()->nullable(false);
            $table->string('year', 9)->nullable(false);
            $table->enum('status', ['ACTIVE', 'LOCK'])->nullable()->default('ACTIVE');
            $table->timestamp('start_time')->nullable(false);

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('unit_id')
                ->references('id')
                ->on('units')
                ->cascadeOnUpdate()
                ->restrictOnDelete();

            $table->foreign('subject_id')
                ->references('id')
                ->on('subjects')
                ->cascadeOnUpdate()
                ->restrictOnDelete();

            $table->foreign('semester_id')
                ->references('id')
                ->on('semesters')
                ->cascadeOnUpdate()
                ->restrictOnDelete();

            $table->foreign('test_id')
                ->references('id')
                ->on('tests')
                ->cascadeOnUpdate()
                ->restrictOnDelete();

            $table->comment('Lịch thi');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('test_schedules');
    }
};
