<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('user_parents', static function (Blueprint $table) {
            $table->rename('ancestors');
        });
    }

    public function down()
    {
        Schema::table('ancestors', function (Blueprint $table) {
            $table->rename('user_parents');
        });
    }
};
