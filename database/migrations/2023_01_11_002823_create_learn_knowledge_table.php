<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('learn_knowledge', function (Blueprint $table) {
            $table->bigInteger('learn_id')->unsigned();
            $table->bigInteger('knowledge_id')->unsigned();
            $table->foreign('learn_id')
                ->references('id')->on('learns')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('knowledge_id')
                ->references('id')->on('knowledge')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('learn_knowledge');
    }
};
