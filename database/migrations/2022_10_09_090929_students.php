<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('user_students', static function (Blueprint $table) {
            $table->rename('students');
        });
    }

    public function down()
    {
        Schema::table('students', static function (Blueprint $table) {
            $table->rename('user_students');
        });
    }
};
