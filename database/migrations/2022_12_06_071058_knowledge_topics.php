<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('knowledge_topics', static function (Blueprint $table) {
            $table->bigInteger('knowledge_id')->unsigned();
            $table->foreign('knowledge_id')
                ->references('id')->on('knowledge')
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table->bigInteger('topic_id')->unsigned();
            $table->foreign('topic_id')
                ->references('id')->on('topics')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->integer('topic_start')->unsigned()->nullable();
            $table->bigInteger('question_id')->unsigned();
            $table->foreign('question_id')
                ->references('id')->on('questions')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->integer('question_start')->unsigned()->nullable();
            $table->integer('question_end')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('knowledge_topics');
    }
};
