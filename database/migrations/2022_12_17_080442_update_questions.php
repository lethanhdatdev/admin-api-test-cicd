<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('questions', static function (Blueprint $table) {
            $table->text('sort_correct')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table('questions', static function (Blueprint $table) {
            $table->string('sort_correct')->nullable()->change();
        });
    }
};
