<?php

namespace Database\Seeders;

use App\Models\Ancestor;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        $adminUser = User::where('email', 'admin@bittus.vn')->first();

        if (!$adminUser) {
            /** @var User $adminUser */
            User::factory()->create([
                'name' => 'Admin User',
                'email' => 'admin@bittus.vn',
                'password' => Hash::make('bittus-admin@2022'),
            ]);
        }

        /**
         * Create Ancestor
         */
        /** @var User $parentUser */
        $parentUser = User::where('email', 'phu.huynh@bittus.vn')->first();

        if (!$parentUser) {
            /** @var User $parentUser */
            $parentUser = User::factory()->create([
                'name' => 'Phụ huynh',
                'email' => 'phu.huynh@bittus.vn',
                'password' => Hash::make('TestUser@9999')
            ]);
        }

        /** @var Ancestor $userInfo */
        $userInfo = Ancestor::where('phone', '0999999990')->first();
        if (!$userInfo) {
            $userInfo = Ancestor::create([
                'phone' => '0999999990',
                'birthday' => '1990-01-01',
                'address' => '123 abc, tp. HCM'
            ]);
        }

        $userInfo->user()->save($parentUser);
    }
}
