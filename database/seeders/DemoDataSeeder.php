<?php

namespace Database\Seeders;

use App\Enums\TestDifficulty;
use App\Enums\TestType;
use App\Models\Ancestor;
use App\Models\File;
use App\Models\Knowledge;
use App\Models\Practice;
use App\Models\PracticeCategory;
use App\Models\Student;
use App\Models\Topic;
use App\Models\User;
use App\Repository\AnswerRepository;
use App\Repository\KnowledgeRepository;
use App\Repository\PracticeRepository;
use App\Repository\QuestionRepository;
use App\Repository\SemesterRepository;
use App\Repository\SubjectRepository;
use App\Repository\TestRepository;
use App\Repository\ThematicRepository;
use App\Repository\TopicRepository;
use App\Repository\UnitRepository;
use App\Services\KnowledgeService;
use App\Services\QuestionService;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class DemoDataSeeder extends Seeder
{
    public function __construct(
        private SubjectRepository $subjectRepo,
        private UnitRepository $unitRepo,
        private SemesterRepository $semesterRepo,
        private ThematicRepository $thematicRepo,
        private QuestionRepository $questionRepository,
        private AnswerRepository $answerRepository,
        private TestRepository $testRepository,
        private PracticeRepository $practiceRepository,
        private KnowledgeRepository $knowledgeRepository,
        private TopicRepository $topicRepository
    ) {
    }

    public function run(): void
    {
        $toan = $this->subjectRepo->firstOrCreate(['name' => 'Toán']);
        $ly = $this->subjectRepo->firstOrCreate(['name' => 'Lý']);
        $hoa = $this->subjectRepo->firstOrCreate(['name' => 'Hóa']);

        $lop7 = $this->unitRepo->firstOrCreate(['name' => 'Lớp 7']);
        $lop8 = $this->unitRepo->firstOrCreate(['name' => 'Lớp 8']);
        $lop9 = $this->unitRepo->firstOrCreate(['name' => 'Lớp 9']);

        /**
         * Student create
         */
        $this->generateStudent($lop7->id);

        $hocky1 = $this->semesterRepo->firstOrCreate(['name' => 'Học kỳ 1', 'unit_id' => $lop7->id]);
        $hocky2 = $this->semesterRepo->firstOrCreate(['name' => 'Học kỳ 2', 'unit_id' => $lop7->id]);
        $this->semesterRepo->firstOrCreate(['name' => 'Học kỳ 1', 'unit_id' => $lop8->id]);
        $this->semesterRepo->firstOrCreate(['name' => 'Học kỳ 2', 'unit_id' => $lop9->id]);

        $thematic1 = $this->thematicRepo->firstOrCreate([
            'name' => 'Chuyên đề 1',
            'description' => 'Chuyên đề 1',
            'subject_id' => $toan->id,
            'semester_id' => $hocky1->id,
        ]);
        $thematic2 = $this->thematicRepo->firstOrCreate([
            'name' => 'Chuyên đề 2',
            'description' => 'Chuyên đề 2',
            'subject_id' => $ly->id,
            'semester_id' => $hocky1->id,
        ]);
        $thematic3 = $this->thematicRepo->firstOrCreate([
            'name' => 'Chuyên đề 3',
            'description' => 'Chuyên đề 3',
            'subject_id' => $hoa->id,
            'semester_id' => $hocky2->id,
        ]);

        $this->generateKnowledge($thematic1->id);

        /**
         * Create test
         */
        $test = $this->testRepository->firstOrCreate([
            'code' => 'TEST_DEMO_1',
            'name' => 'Đề thi demo',
            'subject_id' => $toan->id,
            'semester_id' => $hocky1->id,
            'type' => TestType::OFFICIAL,
            'duration' => 1800,
            'difficulty' => TestDifficulty::NORMAL
        ]);
        $test->save();
        $test->questions()->sync($this->generateQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);
        $test->questions()->sync($this->genFillQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);
        $test->questions()->sync($this->genChoiceQuestion($thematic1->id, 5, 'TEST_DEMO_1'), false);

        /**
         * Create advanced test
         */
        $testAdvanced = $this->testRepository->firstOrNew([
            'code' => 'TEST_DEMO_2',
            'name' => 'Đề thi nâng cao demo',
            'subject_id' => $hoa->id,
            'semester_id' => $hocky2->id,
            'type' => TestType::PRACTICE,
            'duration' => 1800,
            'difficulty' => TestDifficulty::ADVANCED
        ]);
        $testAdvanced->save();
        $testAdvanced->questions()->sync($this->generateQuestion($thematic3->id, 10, 'TEST_DEMO_2'), false);
        $testAdvanced->questions()->sync($this->genFillQuestion($thematic3->id, 10, 'TEST_DEMO_2'), false);
        $testAdvanced->questions()->sync($this->genChoiceQuestion($thematic3->id, 10, 'TEST_DEMO_2'), false);

        $test3 = $this->testRepository->firstOrCreate([
            'code' => 'TEST_DEMO_3',
            'name' => 'Đề thi demo 3',
            'subject_id' => $ly->id,
            'semester_id' => $hocky2->id,
            'type' => TestType::OFFICIAL,
            'duration' => 3600,
            'difficulty' => TestDifficulty::ADVANCED
        ]);
        $test3->save();
        $test3->questions()->sync($this->generateQuestion($thematic3->id, 5, 'TEST_DEMO_3'), false);
        $test3->questions()->sync($this->genFillQuestion($thematic3->id, 5, 'TEST_DEMO_3'), false);
        $test3->questions()->sync($this->genChoiceQuestion($thematic3->id, 5, 'TEST_DEMO_3'), false);

        $practiceCategory = PracticeCategory::firstOrCreate([
            'name' => 'Chương trình SEL',
            'description' => 'Bài thực hành chương trình SEL'
        ]);
        $practiceCategory2 = PracticeCategory::firstOrCreate([
            'name' => 'Hướng nghiệpChương trình SEL',
            'description' => 'Bài thực hành hướng nghiệp'
        ]);
        PracticeCategory::firstOrCreate([
            'name' => 'Trắc nghiệm tâm lý',
            'description' => 'Bài thực hành trắc nghiệm tâm lý'
        ]);

        /**
         * Create practice
         */
        $practice = Practice::where('code', '=', 'PRAC_DEMO_1')->first();

        if (null === $practice) {
            $practice = $this->practiceRepository->create([
                'code' => 'PRAC_DEMO_1',
                'name' => 'Bài thực hành demo',
                'subject_id' => $toan->id,
                'semester_id' => $hocky1->id,
                'difficulty' => TestDifficulty::NORMAL,
                'practice_category_id' => $practiceCategory->id
            ]);
        }
        $practice->questions()->sync($this->generateQuestion($thematic1->id, 5, 'PRAC_DEMO_1'), false);
        $practice->questions()->sync($this->genFillQuestion($thematic1->id, 5, 'PRAC_DEMO_1'), false);
        $practice->questions()->sync($this->genChoiceQuestion($thematic1->id, 5, 'PRAC_DEMO_1'), false);

        /**
         * Create advanced practice
         */
        $practiceAdvanced = Practice::where('code', '=', 'PRAC_DEMO_2')->first();
        if (null === $practiceAdvanced) {
            $practiceAdvanced = $this->practiceRepository->create([
                'code' => 'PRAC_DEMO_2',
                'name' => 'Bài thực hành nâng cao demo',
                'subject_id' => $toan->id,
                'semester_id' => $hocky1->id,
                'difficulty' => TestDifficulty::ADVANCED,
                'practice_category_id' => $practiceCategory2->id
            ]);
        }

        $practiceAdvanced->questions()->sync($this->generateQuestion($thematic1->id, 5, 'PRAC_DEMO_1'), false);
        $practiceAdvanced->questions()->sync($this->genFillQuestion($thematic1->id, 5, 'PRAC_DEMO_1'), false);
        $practiceAdvanced->questions()->sync($this->genChoiceQuestion($thematic1->id, 5, 'PRAC_DEMO_1'), false);
    }

    private function generateKnowledge(int $thematicId): void
    {
        /** @var Knowledge $knowledge * */
        $knowledge = $this->knowledgeRepository->firstOrCreate([
            'name' => 'Kiến thức',
            'intro' => 'Kiến thức',
            'description' => 'Kiến thức',
            'type' => KnowledgeService::TYPE_NORMAL,
            'thematic_id' => $thematicId
        ]);

        if (app()->environment() !== 'testing') {
            $key = 'knowledge/' . md5('demo') . '.png';
            if (!File::where(['name' => 'demo.png', 'path' => $key])->exists()) {
                Storage::disk('s3')->put($key, file_get_contents(__DIR__ . '/files/demo.png'), 'public');
                /** @var File $file */
                $file = File::create([
                    'name' => 'demo.png',
                    'mime' => 'image/png',
                    'type' => 'image',
                    'path' => $key
                ]);

                $file->entity()->associate($knowledge);
                $file->save();
            }

            $videoKey = 'knowledge/' . md5('demo') . '.mp4';
            if (!File::where(['name' => 'demo.mp4', 'path' => $videoKey])->exists()) {
                Storage::disk('s3')->put($videoKey, file_get_contents(__DIR__ . '/files/demo.mp4'), 'public');
                /** @var File $file */
                $file = File::create([
                    'name' => 'demo.png',
                    'mime' => 'video/mp4',
                    'type' => 'video',
                    'path' => $videoKey
                ]);

                $file->entity()->associate($knowledge);
                $file->save();
            }
        }

        /** @var Topic $topic */
        $topic = $this->topicRepository->firstOrCreate(['name' => 'Topic 1', 'description' => 'Topic 1 description']);
        /** @var Topic $topic2 */
        $topic2 = $this->topicRepository->firstOrCreate(['name' => 'Topic 2', 'description' => 'Topic 2 description']);

        $ids = $this->generateQuestion($thematicId, 1, 'KNOWLEDGE_DEMO_1');
        foreach ($ids as $id) {
            $knowledge->topics()->syncWithPivotValues(
                [$topic->id],
                [
                    'topic_start' => 0,
                    'question_id' => $id,
                    'question_start' => 60,
                    'question_end' => 90,
                    'created_at' => (new Carbon()),
                    'updated_at' => (new Carbon())
                ],
                false
            );
        }

        $ids = $this->genChoiceQuestion($thematicId, 1, 'KNOWLEDGE_DEMO_1');
        foreach ($ids as $id) {
            $knowledge->topics()->syncWithPivotValues(
                [$topic2->id],
                [
                    'topic_start' => 91,
                    'question_id' => $id,
                    'question_start' => 120,
                    'question_end' => 150,
                    'created_at' => (new Carbon()),
                    'updated_at' => (new Carbon())
                ],
                false
            );
        }
    }

    private function generateStudent(int $unitId): void
    {
        /** @var User $studentUser */
        $studentUser = User::where('email', 'hoc.sinh.1@bittus.vn')->first();
        if (!$studentUser) {
            /** @var User $studentUser */
            $studentUser = User::factory()->create([
                'name' => 'Học sinh',
                'email' => 'hoc.sinh.1@bittus.vn',
                'password' => Hash::make('TestUser@9999')
            ]);
        }
        /** @var Student $studentEntity */
        $studentEntity = Student::where('phone', '0999999991')->first();
        if (!$studentEntity) {
            $ancestor = Ancestor::where('phone', '0999999990')->first();

            $parts = [
                (new Carbon())->format('Ymd'),
                (new Carbon('1990-01-01'))->format('Ymd')
            ];

            $parts[] = str_pad($ancestor->students()->count() + 1, 5, '0', STR_PAD_LEFT);
            $studentEntity = Student::create([
                'phone' => '0999999991',
                'birthday' => '1990-01-01',
                'address' => '123 abc, tp. HCM',
                'unit_id' => $unitId,
                'code' => implode('_', $parts),
                'ancestor_id' => $ancestor->id
            ]);
        }
        $studentEntity->user()->save($studentUser);
    }

    private function generateQuestion(int $thematicId, int $count = 1, string $prefix = ''): array
    {
        $ids = [];

        for ($i = 1; $i <= $count; $i++) {
            $question = $this->questionRepository->firstOrCreate([
                'description' => 'Câu hỏi sắp xếp demo ' . $prefix . ' #' . $i,
                'type' => QuestionService::TYPE_SORT,
                'thematic_id' => $thematicId,
                'suggestion' => 'Suggestion if answer is incorrect',
                'sort_correct' => 'Số 1,Số 2,Số 3,Số 4',
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 1',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 2',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 3',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Số 4',
                'type' => QuestionService::TYPE_SORT,
                'correct' => true,
                'question_id' => $question->id
            ]);

            $ids[] = $question->id;
        }

        return $ids;
    }

    private function genChoiceQuestion(int $thematicId, int $count = 1, string $prefix = ''): array
    {
        $ids = [];

        for ($i = 1; $i <= $count; $i++) {
            $question = $this->questionRepository->firstOrCreate([
                'description' => 'Câu hỏi trắc nghiệm ' . $prefix . ' #' . $i,
                'type' => QuestionService::TYPE_CHOICE,
                'thematic_id' => $thematicId,
                'suggestion' => 'Suggestion if answer is incorrect',
                'sort_correct' => '',
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 1',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => false,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 2',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => false,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 3',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'Lựa chọn 4',
                'type' => QuestionService::TYPE_CHOICE,
                'correct' => false,
                'question_id' => $question->id
            ]);

            $ids[] = $question->id;
        }

        return $ids;
    }

    private function genFillQuestion(int $thematicId, int $count = 1, string $prefix = ''): array
    {
        $ids = [];

        for ($i = 1; $i <= $count; $i++) {
            $question = $this->questionRepository->firstOrCreate([
                'description' => 'The quick ... fox jumps over the ... dog ' . $prefix . ' #' . $i,
                'type' => QuestionService::TYPE_FILL_IN_BLANK,
                'thematic_id' => $thematicId,
                'suggestion' => 'Suggestion if answer is incorrect',
                'sort_correct' => '',
            ]);

            $ids[] = $question->id;

            $this->answerRepository->firstOrCreate([
                'description' => 'brown',
                'type' => QuestionService::TYPE_FILL_IN_BLANK,
                'correct' => true,
                'question_id' => $question->id
            ]);
            $this->answerRepository->firstOrCreate([
                'description' => 'lazy',
                'type' => QuestionService::TYPE_FILL_IN_BLANK,
                'correct' => true,
                'question_id' => $question->id
            ]);
        }

        return $ids;
    }
}
